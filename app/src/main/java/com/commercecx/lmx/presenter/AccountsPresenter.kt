package com.commercecx.lmx.presenter

import android.os.Bundle
import com.commercecx.lmx.base.BasePresenter
import com.commercecx.lmx.base.IBaseViewTranslator
import com.commercecx.lmx.interactors.*
import com.commercecx.lmx.model.Account
import com.commercecx.lmx.model.Product
import java.lang.ref.WeakReference

/**
 * Created by Sagar Das on 8/25/20.
 */
class AccountsPresenter (
    val view: AccountsViewTranslator
) : BasePresenter<AccountsViewTranslator>(WeakReference(view)) {



    fun onViewCreated() {


        view()?.getInstance(this)



        GetAccountsInteractor().execute() {


                    it.data?.let { data ->

                        var accountList = data.toMutableList()

                        view()?.displayAccounts(accountList)

                    }
                }

        }




    override fun onCreate(savedInstanceState: Bundle?, extras: Bundle?) {
        super.onCreate(savedInstanceState, extras)
        //isCatalog = extras?.getBoolean(IS_CATALOG_ARG) ?: false
    }

    fun searchAccounts(query: String?){
        val searchQuery = "%$query%"
        GetAccountsByNameInteractor().execute(AccountNameFilter(searchQuery!!)) {
            //view()?.hideLoader()
            it.data?.let { data ->
                view()?.displayAccounts(data)
            }
        }
    }




}

interface AccountsViewTranslator : IBaseViewTranslator {

    fun displayAccounts(data: List<Account>)
    fun getInstance(accountsPresenter: AccountsPresenter)

}
