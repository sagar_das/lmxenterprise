package com.commercecx.lmx.model.database

import com.commercecx.lmx.model.UserInfo
import com.commercecx.lmx.model.dao.UserDBDAo


import com.salesforce.androidsdk.smartstore.store.QuerySpec

class DBUserHelper : BaseHelper() {

    companion object {
        private const val TABLE_NAME = "USER"
    }

    override fun getTableName() = TABLE_NAME

    override fun getIndexSpecs() = UserDBDAo.getIndexSpec()

    fun insertUserData(accountInfo: UserInfo) {
        dropTable()
        getSmartStore().registerSoup(getTableName(), getIndexSpecs())
        getSmartStore().create(getTableName(), UserDBDAo(accountInfo))

    }

    fun getUserData(): UserInfo =
        UserInfo.Mapper.fromDB(
            getSmartStore().query(QuerySpec.buildAllQuerySpec(TABLE_NAME, UserDBDAo.EMAIL, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX).getJSONObject(0)
        )


    override fun getAllQuerySpec(): QuerySpec {
        return QuerySpec.buildAllQuerySpec(TABLE_NAME, UserDBDAo.EMAIL, QuerySpec.Order.ascending, PAGE_SIZE)
    }
}
