package com.commercecx.lmx.view

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import com.commercecx.lmx.R
import com.commercecx.lmx.util.Logger


/**
 * Created by Sagar Das on 8/19/20.
 */
class JDECustomEditText : AppCompatEditText {
    constructor(context: Context) : super(context) {
        initialize(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(context, attrs)
    }

    private fun initialize(context: Context, attrs: AttributeSet?) {
        try {
            var value = 0
            if (attrs != null) {
                val a =
                    context.theme.obtainStyledAttributes(attrs, R.styleable.JDECustomEditText, 0, 0)
                // Gets you the 'value' number - 0 or 666 in your example
                if (a.hasValue(R.styleable.JDECustomEditText_jdeFontStyle)) {
                    value = a.getInt(R.styleable.JDECustomEditText_jdeFontStyle, 0)
                }
                a.recycle()
            }
            var typeface = Typeface.createFromAsset(context.assets, FONT_ROBOTO_REGULAR)
            when (JDECustomButton.FontStyle.fromId(value)) {
                JDECustomButton.FontStyle.Bold -> typeface = Typeface.createFromAsset(context.assets, FONT_ROBOTO_BOLD)
                JDECustomButton.FontStyle.Medium -> typeface = Typeface.createFromAsset(context.assets, FONT_ROBOTO_MEDIUM)
                JDECustomButton.FontStyle.SemiBold -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_ROBOTO_SEMIBOLD)
                JDECustomButton.FontStyle.MediumItalic -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_ROBOTO_MEDIUM_ITALIC)
                JDECustomButton.FontStyle.Open_San_Regular -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_OPEN_SAN_REGULAR)
                JDECustomButton.FontStyle.Open_San_SemiBold -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_OPEN_SAN_SEMIBOLD)
                JDECustomButton.FontStyle.Open_San_Bold -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_OPEN_SAN_BOLD)
                JDECustomButton.FontStyle.Open_San_Italic -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_OPEN_SAN_ITALIC)
            }
            setTypeface(typeface)
        } catch (e: Exception) {
            Logger.error(TAG, e)
        }
    }

    companion object {
        private const val FONT_ROBOTO_MEDIUM = "fonts/Roboto-Medium.ttf"
        private const val FONT_ROBOTO_REGULAR = "fonts/Roboto-Regular.ttf"
        private const val FONT_ROBOTO_SEMIBOLD = "fonts/Roboto-Black.ttf"
        private const val FONT_ROBOTO_BOLD = "fonts/Roboto-Bold.ttf"
        private const val FONT_ROBOTO_MEDIUM_ITALIC = "fonts/Roboto-MediumItalic.ttf"
        private const val FONT_OPEN_SAN_REGULAR = "fonts/OpenSans-Regular.ttf"
        private const val FONT_OPEN_SAN_SEMIBOLD = "fonts/OpenSans-Semibold.ttf"
        private const val FONT_OPEN_SAN_BOLD = "fonts/OpenSans-Bold.ttf"
        private const val FONT_OPEN_SAN_ITALIC = "fonts/OpenSans-Italic.ttf"
        private val TAG = JDECustomEditText::class.java.simpleName
    }
}
