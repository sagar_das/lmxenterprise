package com.commercecx.lmx.application

import android.app.Application
import com.commercecx.lmx.activity.LoginActivity
import com.commercecx.lmx.model.OrderSubtype
import com.commercecx.lmx.model.Product
import com.commercecx.lmx.model.Route
import com.commercecx.lmx.model.UserInfo
import com.salesforce.androidsdk.rest.RestClient
import com.salesforce.androidsdk.smartstore.app.SmartStoreSDKManager
import com.salesforce.androidsdk.smartstore.store.SmartStore
import io.realm.Realm
import io.realm.RealmConfiguration


/**
 * Created by Sagar Das on 8/19/20.
 */
class BaseApplication : Application() {
    //region Variable declaration
    //endregion

    val smartStore: SmartStore by lazy {
        SmartStoreSDKManager.getInstance().getGlobalSmartStore("CX")
    }

    companion object {
        lateinit var instance: BaseApplication
        private const val FEATURE_APP_USES_KOTLIN = "KT"
    }


    var restClient: RestClient? = null

    var orderList = mutableListOf<Product>()
    var accountId :String = ""
    var myRoute: Route? = null

    lateinit var userInfo: UserInfo
    var fromVan = false
    var orderSubtype: OrderSubtype? = null
    var deliveryDate: Long = 0
    override fun onCreate() {
        super.onCreate()

        instance = this

        // The default Realm file is "default.realm" in Context.getFilesDir();
        // we'll change it to "RVR.realm"
        Realm.init(this)
        val config: RealmConfiguration = RealmConfiguration.Builder().name("RVR.realm")
            .schemaVersion(0)
            .deleteRealmIfMigrationNeeded().build()
        Realm.setDefaultConfiguration(config)

        //SalesforceSDKManager.initNative(this, HomeActivity::class.java)
        SmartStoreSDKManager.initNative(this, LoginActivity::class.java)
        SmartStoreSDKManager.getInstance().registerUsedAppFeature(FEATURE_APP_USES_KOTLIN)
        SmartStoreSDKManager.getInstance().isBrowserLoginEnabled = true

    } //region Access Tokens
    //endregion
}
