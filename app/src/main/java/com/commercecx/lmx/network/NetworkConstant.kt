package com.commercecx.lmx.network

/**
 * Created by Sagar Das on 8/19/20.
 */
object NetworkConstant {
    const val NOTES_POST_URI =
        "https://68sejjo60f.execute-api.us-east-2.amazonaws.com/Testing/notes"
    const val ROUTES_GET_URI = "https://fjs1izsaec.execute-api.us-east-2.amazonaws.com/Test/notes"
    const val SALESFORCE_PRODUCT_PATH = "/services/apexrest/productRest/0051U000007hVjhQAE"
    const val SALESFORCE_ORDER_PATH = "/services/apexrest/productRest"
}
