package com.commercecx.lmx.util

/**
 * Created by Sagar Das on 8/19/20.
 */

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import java.io.Serializable


class NotePhoto : Serializable {
    var path: String? = null
    var activity: Activity? = null
    var bitmapDrawable: BitmapDrawable? = null
    var bitmap: Bitmap? = null
    var isDoc = false

    constructor(path: String?, activity: Activity?) {
        this.path = path
        this.activity = activity
        bitmapDrawable = NoteAttachUtils.getScaledDrawable(activity!!, path)
        bitmap = NoteAttachUtils.getScaledBitmap(activity, path)
    }

    constructor(activity: Activity?, imageBytes: ByteArray?) {
        bitmapDrawable = NoteAttachUtils.getBitmapFromRealm(activity!!, imageBytes!!)
    }

    constructor(bitmapDrawable: BitmapDrawable?) {
        this.bitmapDrawable = bitmapDrawable
    }

    constructor(activity: Activity?, bitmap: Bitmap?) {
        bitmapDrawable = NoteAttachUtils.getBitmapFromGallery(activity!!, bitmap)
    }

    constructor(isDoc: Boolean) {
        this.isDoc = isDoc
    }
}
