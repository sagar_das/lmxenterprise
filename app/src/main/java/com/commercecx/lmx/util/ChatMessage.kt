package com.commercecx.lmx.util

/**
 * Created by Sagar Das on 8/19/20.
 */
/* POJO for chat bot messages*/
class ChatMessage(message: String, mine: Boolean, image: Boolean) {
    private var isImage: Boolean
    private var isMine: Boolean
    private var content: String
    fun getContent(): String {
        return content
    }

    fun setContent(content: String) {
        this.content = content
    }

    fun isMine(): Boolean {
        return isMine
    }

    fun setIsMine(isMine: Boolean) {
        this.isMine = isMine
    }

    fun isImage(): Boolean {
        return isImage
    }

    fun setIsImage(isImage: Boolean) {
        this.isImage = isImage
    }

    init {
        content = message
        isMine = mine
        isImage = image
    }
}