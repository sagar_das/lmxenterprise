package com.commercecx.lmx.util

/**
 * Created by Sagar Das on 8/19/20.
 */

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.util.Base64
import android.util.Log
import android.widget.ImageView
import java.io.*

object NoteAttachUtils {
    private val TAG = NoteAttachUtils::class.java.simpleName
    fun getScaledDrawable(
        activity: Activity,
        path: String?
    ): BitmapDrawable {
        val display = activity.windowManager.defaultDisplay

        // Display.getWidth() and Display.getHeight() are deprecated
        // It would be best to scale the image so that it fits the ImageView
        // perfectly, but the size of the view in which the image will be
        // displayed is often not available, such as onCreateView, where
        // you cannot get the size of the ImageView.
        // Instead, scale the image to the default display for the device
        val destWidth = display.width.toFloat()
        val destHeight = display.height.toFloat()

        // Read in the dimensions of the image on disk
        var options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, options)
        val srcWidth = options.outWidth.toFloat()
        val srcHeight = options.outHeight.toFloat()
        var inSampleSize = 1
        if (srcHeight > destHeight || srcWidth > destWidth) {
            inSampleSize = if (srcWidth > srcHeight) {
                Math.round(srcHeight / destHeight)
            } else {
                Math.round(srcWidth / destWidth)
            }
        }
        options = BitmapFactory.Options()
        options.inSampleSize = inSampleSize
        val bitmap = BitmapFactory.decodeFile(path, options)
        return BitmapDrawable(activity.resources, bitmap)
    }

    fun getScaledBitmap(
        activity: Activity,
        path: String?
    ): Bitmap {
        val display = activity.windowManager.defaultDisplay

        // Display.getWidth() and Display.getHeight() are deprecated
        // It would be best to scale the image so that it fits the ImageView
        // perfectly, but the size of the view in which the image will be
        // displayed is often not available, such as onCreateView, where
        // you cannot get the size of the ImageView.
        // Instead, scale the image to the default display for the device
        val destWidth = display.width.toFloat()
        val destHeight = display.height.toFloat()

        // Read in the dimensions of the image on disk
        var options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, options)
        val srcWidth = options.outWidth.toFloat()
        val srcHeight = options.outHeight.toFloat()
        var inSampleSize = 1
        if (srcHeight > destHeight || srcWidth > destWidth) {
            inSampleSize = if (srcWidth > srcHeight) {
                Math.round(srcHeight / destHeight)
            } else {
                Math.round(srcWidth / destWidth)
            }
        }
        options = BitmapFactory.Options()
        options.inSampleSize = inSampleSize
        return BitmapFactory.decodeFile(path, options)
    }

    fun getBitmapFromRealm(activity: Activity, imageBytes: ByteArray): BitmapDrawable {
        return BitmapDrawable(
            activity.resources,
            BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        )
    }

    fun getBitmapFromGallery(activity: Activity, bitmap: Bitmap?): BitmapDrawable {
        return BitmapDrawable(activity.resources, bitmap)
    }

    fun cleanImageView(imageView: ImageView) {
        if (imageView.drawable is BitmapDrawable) {
            // Clean up the view's image for the sake of memory
            val bitmapDrawable = imageView.drawable as BitmapDrawable

            // Frees up the native storage for the bitmap
            // If you don't call this the memory will not be freed up at
            // garbage collection time, but in a finalizer, which might cause
            // you to run out of memory.
            bitmapDrawable.bitmap.recycle()
            imageView.setImageDrawable(null)
        }
    }

    @Throws(IOException::class)
    fun getBytes(inputStream: InputStream): ByteArray {
        val byteBuffer = ByteArrayOutputStream()
        val bufferSize = 1024
        val buffer = ByteArray(bufferSize)
        var len = 0
        while (inputStream.read(buffer).also { len = it } != -1) {
            byteBuffer.write(buffer, 0, len)
        }
        return byteBuffer.toByteArray()
    }

    fun convertToFile(fileBytes: ByteArray?, fileType: String?): File {
        val file = File("sample.pdf")
        try {
            FileOutputStream(file).use { fos -> fos.write(fileBytes) }
        } catch (e: Exception) {
            Log.e(TAG, "convertToFile: ", e)
        }
        return file
    }

    fun convertBytesToBitmap(data: String): Bitmap{
        val imageBytes = Base64.decode(data, Base64.DEFAULT)
        //val imageBitmap = BitmapFactory.decodeStream(stream)

        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
    }
}
