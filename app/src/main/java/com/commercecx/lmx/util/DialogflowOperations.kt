package com.commercecx.lmx.util

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.content.Context
import android.text.format.DateFormat
import com.commercecx.lmx.model.Note
import com.commercecx.lmx.network.NetworkOperations
import io.realm.Realm
import io.realm.Realm.Transaction.OnSuccess
import java.util.*


class DialogflowOperations(private val context: Context) {
    fun processResult(intent_name: String?, values: HashMap<String?, String?>) {
        when (intent_name) {
            "NOTES-MAIN-save - yes" -> saveNote(values["content"], values["title"])
        }
    }

    private fun saveNote(contents: String?, title: String?) {
        var contents = contents
        var title = title
        contents = contents!!.replace("\"".toRegex(), "")
        title = title!!.replace("\"".toRegex(), "")
        val realm = Realm.getDefaultInstance()
        val notes: List<Note> = realm.where(Note::class.java).findAll()
        val note = Note()
        note.setId(saltString)
        note.setIndex(notes.size)
        note.setName(if (title == "") "No Title" else title)
        note.setRemark(contents)
        note.setAttachment(false)
        note.setActive(false)
        note.setPendingOperation("insert")
        note.setServerCommitted(false)
        note.setUpdatedTSMP(Date().time)
        val df = DateFormat()
        note.setDate(DateFormat.format("MM/dd/yy", Date()).toString())
        realm.executeTransactionAsync(
            Realm.Transaction { realm -> realm.insertOrUpdate(note) },
            OnSuccess {
                if (NetworkUtil(context).isConnected) {
                    NetworkOperations().sendNoteToServer(note, note.getId())
                }
            })
    }

    companion object {
        // length of the random string.
        private val saltString: String
            private get() {
                val SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
                val salt = StringBuilder()
                val rnd = Random()
                while (salt.length < 18) { // length of the random string.
                    val index = (rnd.nextFloat() * SALTCHARS.length).toInt()
                    salt.append(SALTCHARS[index])
                }
                return salt.toString()
            }
    }
}
