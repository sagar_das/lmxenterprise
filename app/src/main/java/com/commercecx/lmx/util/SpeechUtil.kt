package com.commercecx.lmx.util

import android.app.Activity
import android.content.Context
import android.speech.tts.TextToSpeech
import android.speech.tts.TextToSpeech.OnInitListener
import android.speech.tts.UtteranceProgressListener
import android.util.Log
import android.widget.Toast
import java.util.*

/**
 * Created by Sagar Das on 8/19/20.
 */


class SpeechUtil(context: Context, activity: Activity) :
    OnInitListener {
    private val context: Context
    private var tts: TextToSpeech? = null
    private var text: String? = null
    private val activity: Activity? = null

    interface speakingCompleteListener {
        fun onSpeakingComplete()
    }

    var mListener: speakingCompleteListener
    fun initiateSpeech(text: String?) {
        this.text = text
        tts = TextToSpeech(context, this)
        //   tts.setOnUtteranceCompletedListener(this);
        tts!!.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
            override fun onStart(s: String) {}
            override fun onDone(s: String) {
                mListener.onSpeakingComplete()
            }

            override fun onError(s: String) {}
        })
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            val result = tts!!.setLanguage(Locale.US)
            if (result == TextToSpeech.LANG_MISSING_DATA
                || result == TextToSpeech.LANG_NOT_SUPPORTED
            ) {
                Log.e("TTS", "This Language is not supported")
            } else {
                //  Toast.makeText(context, "Initilization Success!", Toast.LENGTH_SHORT).show();
                speakOut(text)
            }
        } else {
            Log.e("TTS", "Initilization Failed!")
            Toast.makeText(context, "Initilization Failed!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun speakOut(text: String?) {
        val mySpeech = HashMap<String, String>()
        mySpeech[TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID] = "FINISHED SAYING"
        tts!!.speak(text, TextToSpeech.QUEUE_FLUSH, mySpeech)
    }

    init {
        this.context = context
        mListener = activity as speakingCompleteListener
    }
}
