package com.commercecx.lmx.util

/**
 * Created by Sagar Das on 8/19/20.
 */


import android.content.Context
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream

object FileUtil {
    private val TAG = FileUtil::class.java.simpleName
    private fun readJSONFromAsset(context: Context, filename: String): String? {
        var json: String? = null
        try {
            val ipStream: InputStream = context.assets.open(filename)
            val size = ipStream.available()
            val buffer = ByteArray(size)
            ipStream.read(buffer)
            ipStream.close()

            json = buffer.toString(Charsets.UTF_8)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    fun getCatalogJsonObject(context: Context): JSONObject? {
        val response = readJSONFromAsset(context, "json/catalog.json")
        return try {
            JSONObject(response)
        } catch (e: JSONException) {
            e.printStackTrace()
            null
        }
    }

    fun getRoutesJsonObject(context: Context): JSONObject? {
        val response = readJSONFromAsset(context, "json/routes.json")
        return try {
            JSONObject(response)
        } catch (e: JSONException) {
            e.printStackTrace()
            null
        }
    }

    fun getTruckJsonObject(context: Context): JSONObject? {
        val response = readJSONFromAsset(context, "json/truck.json")
        return try {
            JSONObject(response)
        } catch (e: JSONException) {
            e.printStackTrace()
            null
        }
    }

    fun getNoteJsonObject(context: Context): JSONObject? {
        val response = readJSONFromAsset(context, "json/notes.json")
        return try {
            JSONObject(response)
        } catch (e: JSONException) {
            e.printStackTrace()
            null
        }
    }
}
