package com.commercecx.lmx.util

import android.content.Context
import android.net.Uri
import android.widget.Toast
import com.opencsv.CSVReader
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStreamReader
import java.nio.charset.StandardCharsets
import java.util.*

/**
 * Created by Sagar Das on 8/19/20.
 */


object CSVUtil {
    //private static String fileName = "src/main/resources/numbers.csv";
    fun readFromCSV(uri: Uri?, context: Context): List<*> {
        val products_list: MutableList<Array<String>> = ArrayList()
        try {
            val fis = FileInputStream(
                context.contentResolver.openFileDescriptor(
                    uri!!, "r"
                )!!.fileDescriptor
            )
            val isr = InputStreamReader(fis, StandardCharsets.UTF_8)
            val reader = CSVReader(isr)
            var nextLine: Array<String>
            var count = 0
            while (reader.readNext().also { nextLine = it } != null) {
                if (count == 0) {
                    count++
                    continue
                }
                products_list.add(nextLine)
                //Toast.makeText(context, nextLine.toString(), Toast.LENGTH_SHORT).show();
//                    for (String s : nextLine) {
//                        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
//                    }
            }
        } catch (ex: FileNotFoundException) {
            Toast.makeText(context, "File not found", Toast.LENGTH_SHORT).show()
        } catch (ex: IOException) {
            Toast.makeText(context, "Error in processing file", Toast.LENGTH_SHORT).show()
        }
        return products_list
    }
}
