package com.commercecx.lmx.interactors

import com.commercecx.lmx.network.NetworkConstant
import com.commercecx.lmx.util.Logger
import com.salesforce.androidsdk.rest.RestClient
import com.salesforce.androidsdk.rest.RestRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject

/**
 * Created by Sagar Das on 8/24/20.
 */
class OrderInteractor(private val client: RestClient) {

    var sourceOrderId: String? = null


    suspend fun sendOrder() {
        return GlobalScope.async(Dispatchers.IO) {
            // make network call
            try {


                val orderDataString = "{ " +
                        "\"AccountInfo\": { " +
                        " \"AccountName\" : \"Test Account\", " +
                        " \"AccountID\" : \"0011U00001Q8lXJQAZ\" " +
                        " }, " +
                        "\"Orders\" : [{ " +
                        "\"SourceOrderNumber\": " + "\"" + sourceOrderId + "\", " +
                        "\"PriceBook2Id\": \"01s1U00000AQqxiQAD\", " +
                        "\"OrderDescription\": \"test order desc\", " +
                        "\"ContractId\": \"8001U000000IUGKQA4\", " +
                        "\"BillingStreet\" : \"Wilhelminaplein\", " +
                        "\"BillingState\" : \"Friesland\", " +
                        "\"BillingCity\" : \"Friesland\", " +
                        "\"BillingPostalCode\" : \"8911 BS\", " +
                        "\"BillingCountry\" : \"NETHERLANDS\", " +
                        "\"ShippingStreet\" : \"Wilhelminaplein\", " +
                        "\"ShippingState\" : \"Friesland\", " +
                        "\"ShippingCity\" : \"Friesland\", " +
                        "\"ShippingPostalCode\" : \"8911 BS\", " +
                        "\"ShippingCountry\" : \"NETHERLANDS\", " +
                        "\"StartDate\" : \"2020-08-25T11:46:00.000Z\" " +
                        " }], " +
                        "\"Products\" : [{ " +
                        "\"ProductFamily\" : \"Test\", " +
                        "\"ProductName\" : \"DB Black\", " +
                        "\"ProductImage\" : \"FileDownload?file=null\", " +
                        "\"ProductID\" : \"01t1U000006NYftQAG\", " +
                        "\"ProductDescription\" : \"Desc\", " +
                        "\"PriceBookEntryId\": \"01u1U00000FGFSHQA5\", " +
                        "\"UnitPrice\": \"12.00\", " +
                        "\"IsActive\" : true, " +
                        "\"Quantity\" : \"10\" " +
                        "}]" +
                        "}"

//            val syncRequest = RestRequest(
//                RestRequest.RestMethod.POST,
//                "/services/apexrest/RSOOrder/CreateOrder",
//                RequestBody.create(MediaType.parse("application/json"), params.toString())
//            )

                val syncRequest = RestRequest(
                    RestRequest.RestMethod.POST,
                    NetworkConstant.SALESFORCE_ORDER_PATH,
                    RequestBody.create(MediaType.parse("application/json"), orderDataString)
                )


                //JDELog.d("UDE", params.toString())


                val syncResponse = client.sendSync(syncRequest)

                if (syncResponse.isSuccess) {
//                with(JDEDBProductHelper(store)) {

                    //TODO: We''e supposed to store the orders not send them to backendz
                    Logger.info("Done")
                } else {
                    throw IllegalStateException("Response not success: " + syncResponse.statusCode)
                }

            } catch (e: Exception) {
                Logger.error(e.message)
            }
        }.await()
    }

    fun syncCall(sourceOrderId: String?) {

        this.sourceOrderId = sourceOrderId

        GlobalScope.launch(Dispatchers.Main) {
            sendOrder()
        }

    }
}