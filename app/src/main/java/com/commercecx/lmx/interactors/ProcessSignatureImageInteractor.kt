//package com.commercecx.lmx.interactors
//
//import android.content.Context
//import android.graphics.Bitmap
//import com.mobgen.jdeapp.ui.home.routes.orders.SignatureViewTranslator
//
//
//data class ProcessSignatureParams(val view: SignatureViewTranslator?)
//
//class ProcessSignatureImageInteractor : JDEBaseInteractor<ProcessSignatureParams, Result<Void>> {
//
//    companion object {
//        const val SIGNATURE_FILE_NAME = "signature.jpeg"
//        const val SIGNATURE_FILE_QUALITY = 100
//    }
//
//    override fun syncCall(params: ProcessSignatureParams?): Result<Void> {
//        return try {
//            val bitmap = params?.view?.getSignatureBitmap()
//
//            // Store the image
//            val fileOutputStream = params?.view?.context()?.openFileOutput(SIGNATURE_FILE_NAME, Context.MODE_PRIVATE)
//            bitmap?.compress(Bitmap.CompressFormat.JPEG, SIGNATURE_FILE_QUALITY, fileOutputStream)
//            fileOutputStream?.close()
//
//            bitmap?.recycle()
//
//            bitmap?.let {
//                return Result()
//            } ?: Result(null, Exception())
//        } catch (ex: Exception) {
//            //JDELog.e("ProcessSignatureImageInteractor exception: {${ex.message}}")
//            Result(null, ex)
//        }
//    }
//}