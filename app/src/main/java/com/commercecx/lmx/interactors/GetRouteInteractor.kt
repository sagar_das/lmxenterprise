package com.commercecx.lmx.interactors

import com.commercecx.lmx.model.Route

import com.commercecx.lmx.model.database.DBRoutesHelper



class GetRouteInteractor : BaseInteractor<String, Result<Route>> {

    override fun syncCall(params: String?): Result<Route> {
        if (params == null) {
            throw IllegalArgumentException()
        }

        return try {
            Result(DBRoutesHelper().getRouteById(params))


        } catch (ex: Exception) {
            //JDELog.e("GetAllRoutesInteractor exception: {${ex.message}}")
            Result(null, ex)
        }
    }
}