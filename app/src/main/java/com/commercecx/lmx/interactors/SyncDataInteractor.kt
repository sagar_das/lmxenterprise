package com.commercecx.lmx.interactors

import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.util.Base64
import com.commercecx.lmx.application.BaseApplication
import com.commercecx.lmx.model.AccountInfo
import com.commercecx.lmx.model.ProductInfo
import com.commercecx.lmx.model.database.*
import com.commercecx.lmx.network.NetworkConstant
import com.commercecx.lmx.util.Logger

import com.salesforce.androidsdk.rest.RestClient
import com.salesforce.androidsdk.rest.RestRequest
import com.salesforce.androidsdk.smartstore.store.SmartStore
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.util.*


//TODO: Errors


class SyncDataInteractor(
    private val client: RestClient,
    private val store: SmartStore,
    private val prefs: SharedPreferences
) : BaseInteractor<Unit, Result<Unit>> {

    companion object {
        const val LAST_CALL_PREF = "last_call_pref"
        const val NO_VALUE_PREF = -1L
    }

    override fun syncCall(params: Unit?): Result<Unit> {
        try {

            DBOrdersHelper().deleteOldOrders()
            DBRoutesHelper().deletePreviousRoutes(Date())

            val param = JSONObject()
            param.put("EmpEmail", client.clientInfo.email)
            param.put("InitialLoad", isInitialLoad(prefs))

//            val syncRequest = RestRequest(
//                RestRequest.RestMethod.POST,
//                "/services/apexrest/RSOOrder/SyncData",
//                RequestBody.create(MediaType.parse("application/json; charset=utf-8"), param.toString())
//            )

            val syncRequest = RestRequest(
                RestRequest.RestMethod.GET,
                NetworkConstant.SALESFORCE_PRODUCT_PATH
            )


            val syncResponse = client.sendSync(syncRequest)
            if (syncResponse.isSuccess) {

                val response = syncResponse.asJSONArray()
                var  productJsonArray = JSONArray()
                var  accountJsonArray = JSONArray()

                for (i in 0 until response.length()) {
                    val item = response.getJSONObject(i)
                    val attribute = item["attributes"] as JSONObject
                    if(attribute.get("type").toString().equals("Product2",true)
                        && item.get("Name").toString().contains("DB ",true)){
                        productJsonArray.put(item)
                    }
                    else if(attribute.get("type").toString().equals("Account",true)){
                            accountJsonArray.put(item)
                        }




                }

                val prodInfo = ProductInfo.Mapper.from(
                    productJsonArray
                )

                val accountInfo = AccountInfo.Mapper.from(
                    accountJsonArray
                )



//                val routesInfo = RoutesInfo.Mapper.from(
//                    syncResponse.asJSONObject(),
//                    client.clientInfo.displayName,
//                    client.clientInfo.email
//                )



                prodInfo.products.asSequence().forEachIndexed { index, product ->
                    //JDELog.e("Proccessing image $index of ${routesInfo.products.size} ")
                    if (!product.productimage.contains("null")) {

                        if (!product.productimage.isBlank()) {

                            try {

                                //val stream = imageResponse.asInputStream()
                                val imgArr = product.productimage.split(",")

                                val imageBytes = Base64.decode(imgArr[1],Base64.DEFAULT)
                                //val imageBitmap = BitmapFactory.decodeStream(stream)

                                val imageBitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)

                                val imageFile = File(Environment.getExternalStorageDirectory().absolutePath + "/" + product.productid)

                                val fileOutputStream = FileOutputStream(imageFile)

                                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream)
                                fileOutputStream.close()
                                imageBitmap.recycle()

//                                JDELog.d("Success " + imageFile.absolutePath)

                            } catch (e: Exception) {
                              Logger.error("Error proccessing image" + e.message)
                            }
                        }
                    } else {
//                        JDELog.e("No image for products")
                    }
                }

                if (isInitialLoad(prefs)) {
//                    JDELog.w("Initial load, inserting products")
                    DBProductHelper().insertProducts(prodInfo.products)
                    DBAccountHelper().insertAccounts(accountInfo.accounts)
                } else {
//                    JDELog.w("Not initial load, updating products")
                    DBProductHelper().updateProducts(prodInfo.products)
                    DBAccountHelper().updateAccounts(accountInfo.accounts)
                }
                storeSyncDate(prefs)


//                JDEDBContractedProductHelper().insertProducts(prodInfo.contractedProduct)
//                JDEDBAccountHelper().insertUserData(routesInfo.userInfo)



//                routesInfo.routes.forEach{
//
//                    if (JDEDBRoutesHelper().getRouteById(it.caseId) != null) {
//                        it.updateVisited(JDEDBRoutesHelper().getRouteById(it.caseId)!!.timeVisited)
//                    }
//
//                }
//
//                JDEDBRoutesHelper().insertRoutes(routesInfo.routes)

//                BaseApplication.instance.userInfo = routesInfo.userInfo
                return Result(Unit)
            } else {
                return Result(null, Exception(syncResponse.statusCode.toString()))
            }

        } catch (e: Exception) {
            BaseApplication.instance.userInfo = DBUserHelper().getUserData()
            return Result(null, e)
        }
    }


    private fun isInitialLoad(prefs: SharedPreferences) = prefs.getLong(LAST_CALL_PREF, NO_VALUE_PREF) == NO_VALUE_PREF

    private fun storeSyncDate(prefs: SharedPreferences) = prefs.edit().putLong(LAST_CALL_PREF, Date().time).apply()

}







