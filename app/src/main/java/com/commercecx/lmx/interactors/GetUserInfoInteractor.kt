package com.commercecx.lmx.interactors

import com.commercecx.lmx.model.UserInfo
import com.commercecx.lmx.model.database.DBUserHelper


class GetUserInfoInteractor : BaseInteractor<Unit, Result<UserInfo>> {

    override fun syncCall(params: Unit?): Result<UserInfo> {
        return try {
            Result(DBUserHelper().getUserData())
        } catch (ex: Exception) {
            //JDELog.e("GetUserInfoInteractor exception: ${ex.message}")
            Result(null, ex)
        }
    }
}