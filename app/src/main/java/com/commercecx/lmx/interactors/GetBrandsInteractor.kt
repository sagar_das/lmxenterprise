package com.commercecx.lmx.interactors


import com.commercecx.lmx.model.Category
import com.commercecx.lmx.model.database.DBProductHelper

import com.salesforce.androidsdk.smartstore.store.SmartStore

data class BrandFilter(val category: Category, val subCategory: Category)

class GetBrandsInteractor(private val smartStore: SmartStore) :
    BaseInteractor<BrandFilter, Result<List<Category>>> {

    override fun syncCall(params: BrandFilter?): Result<List<Category>> {
        if (params == null) {
            throw IllegalArgumentException()
        }
        return try {
            Result(DBProductHelper().getBrands(params.category.name, params.subCategory.name))
        } catch (ex: Exception) {
            //JDELog.e("GetBrandsInteractor exception: {${ex.message}}")
            Result(null, ex)

        }

    }
}
