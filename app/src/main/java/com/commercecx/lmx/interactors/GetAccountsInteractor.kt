package com.commercecx.lmx.interactors

import com.commercecx.lmx.model.Account
import com.commercecx.lmx.model.PendingOrder
import com.commercecx.lmx.model.Product
import com.commercecx.lmx.model.database.DBAccountHelper
import com.commercecx.lmx.model.database.DBContractedProductHelper
import com.commercecx.lmx.model.database.DBProductHelper

/**
 * Created by Sagar Das on 8/25/20.
 */

data class AccountNameFilter(val accountName: String)

class GetAccountsInteractor : BaseInteractor<Unit, Result<List<Account>>> {

    override fun syncCall(params: Unit?): Result<List<Account>> {

        return try {
            var result = mutableListOf<Account>()




                result.addAll(DBAccountHelper().getAllAccounts())
                result = result.distinctBy { it.id }.toMutableList()


            result.sortBy { it.name }

            Result(result)
        } catch (ex: Exception) {
            //   JDELog.e("GetAllProductsInteractor exception: {${ex.message}}")
            Result(null, ex)
        }
    }
}

class GetAccountsByNameInteractor : BaseInteractor<AccountNameFilter, Result<List<Account>>> {

    override fun syncCall(params: AccountNameFilter?): Result<List<Account>> {

        if (params == null) {
            throw IllegalArgumentException()
        }

        return try {
            var result = mutableListOf<Account>()
            result.addAll(DBAccountHelper().getAccountByName(params.accountName))
            result = result.distinctBy { it.id }.toMutableList()
            result.sortBy { it.name }

            Result(result)
        } catch (ex: Exception) {
            //JDELog.e("GetAllProductsInteractor exception: ${ex.message}")
            Result(null, ex)
        }
    }
}