package com.commercecx.lmx.interactors

import com.commercecx.lmx.model.Category

import com.commercecx.lmx.model.database.DBProductHelper


class GetSubCategoriesInteractor : BaseInteractor<Category, Result<List<Category>>> {

    override fun syncCall(params: Category?): Result<List<Category>> {
        if (params == null) {
            throw IllegalArgumentException()
        }
        return try {
            Result(DBProductHelper().getSubCategories(params.name))

        } catch (ex: Exception) {
            //JDELog.e("GetSubCategoriesInteractor exception: {${ex.message}}")
            Result(null, ex)

        }
    }
}
