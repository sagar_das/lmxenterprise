package com.commercecx.lmx.interactors


import com.commercecx.lmx.model.PendingOrder
import com.commercecx.lmx.model.database.DBOrdersHelper


class GetAllOrdersInteractor : BaseInteractor<Unit, Result<List<PendingOrder>>> {

    override fun syncCall(params: Unit?): Result<List<PendingOrder>> {
        return try {
            var allOrders = DBOrdersHelper().getAllOrders().sortedByDescending { it.index }
            if(allOrders.size>3){
                allOrders = allOrders.subList(0,3)
            }
            Result(allOrders)
        } catch (e: Exception) {
            Result(null, e)
        }
    }
}