package com.commercecx.lmx.interactors

import com.commercecx.lmx.model.Route
import com.commercecx.lmx.model.database.DBRoutesHelper


class GetAllRoutesInteractor : BaseInteractor<Unit, Result<List<Route>>> {

    override fun syncCall(params: Unit?): Result<List<Route>> {

        val routes = mutableListOf<Route>()

        return try {
            routes.addAll(DBRoutesHelper().getAllRoutes().sortedWith(compareBy({ it.isVisited() }, { it.time })))
            Result(routes.toList())

        } catch (ex: Exception) {
            (Result(null, ex))
        }
    }
}