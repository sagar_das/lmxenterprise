package com.commercecx.lmx.interactors

import com.commercecx.lmx.application.BaseApplication
import com.commercecx.lmx.model.Product
import com.commercecx.lmx.model.database.DBContractedProductHelper


class GetConsignmentProducts : BaseInteractor<Unit, Result<List<Product>>> {

    override fun syncCall(params: Unit?): Result<List<Product>> {
        return try {

            val result: MutableList<Product> = DBContractedProductHelper().getAllConsignmentProducts().toMutableList()
            result.sortBy { it.productname }
            var orderList = result.toMutableList()
            if (!BaseApplication.instance.accountId.isEmpty()) {
                orderList.clear()
                orderList = result.filterTo(orderList, { product ->
                    product.accountID.equals(BaseApplication.instance.accountId) && product.consignment.equals(true)
                })
            }
            Result(orderList)
        } catch (ex: Exception) {
            //JDELog.e("GetConsignmentProducts exception: ${ex.message}")
            Result(null, ex)
        }
    }
}