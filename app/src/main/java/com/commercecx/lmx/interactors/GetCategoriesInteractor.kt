package com.commercecx.lmx.interactors



import com.commercecx.lmx.R
import com.commercecx.lmx.model.Category
import com.commercecx.lmx.model.database.DBProductHelper


class GetCategoriesInteractor() : BaseInteractor<Unit?, Result<List<Category>>> {

    enum class CategoryType(val text: String, val imageResourceId: Int) {
        COFFEE("COFFEE", R.drawable.placeholder_image), TEA("TEA", R.drawable.placeholder_image), MORE("& MORE", R.drawable.moreinfo_arrow)
    }

    override fun syncCall(params: Unit?): Result<List<Category>> {
        return try {
            val sortedCategories = mutableListOf<Category>()

            val unsortedCategories = DBProductHelper().getCategories()

            unsortedCategories.forEach {
                if (it.name.toUpperCase() == CategoryType.COFFEE.text) {
                    sortedCategories.add(it.apply {
                        imageResourceId = CategoryType.COFFEE.imageResourceId
                    })
                    return@forEach
                }
            }

            unsortedCategories.forEach {
                if (it.name.toUpperCase() == CategoryType.TEA.text) {
                    sortedCategories.add(it.apply {
                        imageResourceId = CategoryType.TEA.imageResourceId
                    })
                    return@forEach
                }
            }

            unsortedCategories.forEach {
                if (it.name.toUpperCase() == CategoryType.MORE.text) {
                    sortedCategories.add(it.apply {
                        imageResourceId = CategoryType.MORE.imageResourceId
                    })
                    return@forEach
                }
            }

            unsortedCategories.forEach {
                if (it.name.toUpperCase() != CategoryType.COFFEE.text && it.name.toUpperCase() != CategoryType.TEA.text && it.name.toUpperCase() != CategoryType.MORE.text) {
                    sortedCategories.add(it)
                }
            }

            Result(sortedCategories)

        } catch (ex: Exception) {
            //JDELog.e("GetCategoriesInteractor exception: {${ex.message}}")
            Result(null, ex)
        }

    }
}
