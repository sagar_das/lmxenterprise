package com.commercecx.lmx.adapter

/**
 * Created by Sagar Das on 8/19/20.
 */

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.commercecx.lmx.R
import com.commercecx.lmx.activity.CatalogActivity
import com.commercecx.lmx.activity.MainNavActivity
import com.commercecx.lmx.activity.ReviewCartActivity
import com.commercecx.lmx.model.CartOld
import com.commercecx.lmx.model.OrderOld
import com.commercecx.lmx.model.Product
import com.commercecx.lmx.model.ProductOld
import com.commercecx.lmx.util.Logger
import com.commercecx.lmx.util.NoteAttachUtils
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import io.realm.Realm
import io.realm.RealmList

class CatalogAdapter(
    private val activity: Activity,
    name: String?,
    cart_id: Int,
    bundle: Bundle?,
    products: MutableList<Product>?,
    private val context: Context
) :
    RecyclerView.Adapter<CatalogAdapter.ViewHolder?>() {
    private val gson: Gson? = null
    var productsOld: MutableList<ProductOld>
    var products: MutableList<Product>? = null
    private val dialog: BottomSheetDialog? = null
    private var realm: Realm
    private var cart: CartOld? = null
    private var cart_id: Int
    private var bundle: Bundle? = null
    var product: Product? = null

    interface IProductClickListener {
        fun onProductClick(product: Product?)
    }

    var productClickListener: IProductClickListener? = null



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(activity).inflate(R.layout.li_catalog_item, parent, false)
        )
    }

    override fun onBindViewHolder(@NonNull holder: ViewHolder, position: Int) {
        val productOld = ProductOld()
        product = products!![position]
        productOld.setName(product!!.productname)
        productOld.setId(position.toString())
        productOld.setItem_price("12")
        productOld.setItem_tax("0")
        holder.nameTV.text = product!!.productname
        //holder.weightTV.setText(product.getItem_pack());

        //Updating price
//        if(holder.qtyET.getText().toString().equals("1")||holder.qtyET.getText().toString().equals("0")){
//            holder.tvPrice.setText("$"+product.getItem_price());
//        }
//        else {
//            Double price = Double.parseDouble(product.getItem_price()) *
//                    Double.parseDouble(holder.qtyET.getText().toString());
//
//            holder.tvPrice.setText("$" + String.valueOf(Math.round(price * 100.0)/100.0));
//        }
        holder.tvPrice.text = "$" + "12"
        if (product!!.productimage != null && !product!!.productimage!!.isEmpty()) {
            val res = activity.resources
//            val resourceId = res.getIdentifier(
//                product.getImage(), "drawable", activity.packageName
//            )

            val imgArr = product!!.productimage.split(",")

            val imageBitmap = NoteAttachUtils.convertBytesToBitmap(imgArr[1])


            holder.imageView.setImageBitmap(imageBitmap)
            //holder.imageView.setImageResource(resourceId)
        } else {
            val res = activity.resources
            val resourceId = res.getIdentifier(
                "product", "drawable", activity.packageName
            )
            holder.imageView.setImageResource(resourceId)
        }
        if (cart != null && cart!!.getOldProducts() != null && isProductExists(
                cart,
                productOld
            ) != null
        ) {
            holder.removeBtn.visibility = View.VISIBLE
            holder.addBtn.visibility = View.GONE
        } else {
            holder.removeBtn.visibility = View.GONE
            holder.addBtn.visibility = View.VISIBLE
        }
        if (activity.javaClass.simpleName.equals(
                MainNavActivity::class.java.getSimpleName(),
                ignoreCase = true
            )
        ) {
            holder.llQty.visibility = View.GONE
            holder.addBtn.visibility = View.GONE
            holder.removeBtn.visibility = View.GONE
            holder.qtyET.visibility = View.GONE
            holder.tvQTY.visibility = View.GONE
            //holder.ibEdit.visibility = View.VISIBLE
        }
        holder.ibEdit.setOnClickListener { v ->
            val dialogBuilder = AlertDialog.Builder(v.context)
            val completeDialog =
                LayoutInflater.from(context).inflate(R.layout.edit_product_dialog, null)
            dialogBuilder.setView(completeDialog)
            val dialog = dialogBuilder.create()
            val btnYes =
                completeDialog.findViewById<View>(R.id.btnYes) as Button
            val btnNo =
                completeDialog.findViewById<View>(R.id.btnNo) as Button
            val etProductName =
                completeDialog.findViewById<View>(R.id.etProductName) as EditText
            val etProductPrice =
                completeDialog.findViewById<View>(R.id.etProductPrice) as EditText
            val etProductTax =
                completeDialog.findViewById<View>(R.id.etProductTax) as EditText
            etProductName.setText(productOld.getName())
            etProductPrice.setText(productOld.getItem_price())
            etProductTax.setText(productOld.getItem_tax())
            btnYes.setOnClickListener {
                productOld.setName(etProductName.text.toString())
                productOld.setItem_price(etProductPrice.text.toString())
                productOld.setItem_tax(etProductTax.text.toString())
                realm.executeTransactionAsync { realm -> // realm.insertOrUpdate(route);
                    realm.copyToRealmOrUpdate(productOld)
                }
                notifyItemChanged(position)
                notifyDataSetChanged()
                dialog.dismiss()
            }
            btnNo.setOnClickListener { dialog.dismiss() }
            dialog.show()
        }
        holder.addBtn.setOnClickListener {
            Logger.debug(holder.qtyET.text.toString())
            if (!holder.qtyET.text.toString().isEmpty() && !holder.qtyET.text.toString()
                    .equals("0", ignoreCase = true) && !holder.qtyET.text.toString()
                    .equals("", ignoreCase = true)
            ) {
                holder.removeBtn.visibility = View.VISIBLE
                holder.addBtn.visibility = View.GONE
                if (cart != null) {
                    val dbProduct = isProductExists(cart, productOld)
                    if (dbProduct != null && !dbProduct.getId()!!.isEmpty()) {
                        //update the existing product
                        realm.beginTransaction() //open the database
                        dbProduct.setItem_qty(holder.qtyET.text.toString())
                        cart!!.getOldProducts()!!.add(productOld)
                        realm.insertOrUpdate(cart)
                        realm.commitTransaction()
                    } else {
                        realm.beginTransaction() //open the database
                        productOld.setItem_qty(holder.qtyET.text.toString())
                        if (cart!!.getOldProducts() != null) {
                            cart!!.getOldProducts()!!.add(productOld)
                        } else {
                            val productsOld = RealmList<ProductOld>()
                            productsOld.add(productOld)
                            cart!!.setOldProducts(productsOld)



                        }
                        realm.insertOrUpdate(cart)
                        realm.commitTransaction()
                    }
                    if (position != 0) {
                        val fristProduct = productsOld[0]
                        val currProduct = productsOld[position]

                        productsOld.removeAt(position)
                        productsOld.add(0, currProduct)
                        notifyItemMoved(position, 0)
                        (activity as CatalogActivity).scrollToBeginning()
                        notifyDataSetChanged()
                    }
                    (activity as CatalogActivity).updateCount(cart)
                    (activity as CatalogActivity).updateMessage(
                        holder.qtyET.text.toString(),
                        "products added"
                    )
                }
            } else {
                Toast.makeText(
                    activity,
                    "Please enter no of products to be added to cart",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        holder.removeBtn.setOnClickListener {
            holder.removeBtn.visibility = View.GONE
            holder.addBtn.visibility = View.VISIBLE
            if (cart != null && cart!!.getOldProducts() != null) {
                val dbProduct = isProductExists(cart, productOld)
                realm.beginTransaction() //open the database
                cart!!.getOldProducts()!!.remove(dbProduct)
                if (cart!!.getPreviousProducts() != null) {

                    cart!!.getPreviousProducts()!!
                        .remove(dbProduct)
                    realm.commitTransaction()
                }


            }
            realm.beginTransaction() //open the database
            realm.insertOrUpdate(cart)
            realm.commitTransaction()
            (activity as CatalogActivity).updateCount(cart)
            (activity as CatalogActivity).updateMessage(
                holder.qtyET.text.toString(),
                "items removed"
            )
        }

        holder.llProductItem.setOnClickListener {

            productClickListener!!.onProductClick(product)
        }
        holder.qtyET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (!editable.toString().isEmpty()) {
                    val dbProduct = isProductExists(cart, productOld)

                    //Updating price
                    if (editable.toString() == "1" || editable.toString() == "0") {
                        holder.tvPrice.text = "$" + productOld.getItem_price()
                    } else {
                        val price =
                            productOld.getItem_price()!!.toDouble() * editable.toString().toDouble()
                        holder.tvPrice.text = "$" + (Math.round(price * 100.0) / 100.0).toString()
                    }
                    if (dbProduct != null) {
                        productOld.setItem_qty(editable.toString())
                        realm.beginTransaction()
                        cart!!.getOldProducts()!!.remove(dbProduct)
                        cart!!.getOldProducts()!!.add(productOld)
                        realm.commitTransaction()
                        (activity as CatalogActivity).updateCount(cart)
                    }
                }
            }
        })
        if (activity.javaClass.simpleName.equals(
                CatalogActivity::class.java.getSimpleName(),
                ignoreCase = true
            )
        ) {
            activity.findViewById<View>(R.id.reviewCartBtn).setOnClickListener {
                if (cart!!.getOldProducts() != null && cart!!.getOldProducts()!!.size > 0) {
                    Logger.debug("onClickReviewCartBtn...")
                    val intent = Intent(activity, ReviewCartActivity::class.java)
                    intent.putExtra("cart_id", cart!!.getId())
                    intent.putExtra("ROUTE_BUNDLE", bundle)
                    activity.startActivity(intent)
                } else {
                    Toast.makeText(activity, "No Products in the Cart", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun getItemCount(): Int {
        //return productsOld.size
        return products!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nameTV: TextView
        var tvPrice: TextView
        var tvQTY: TextView
        var weightTV: TextView
        var imageView: ImageView
        var addBtn: ImageButton
        var removeBtn: ImageButton
        var ibEdit: ImageButton
        var qtyET: EditText
        var llProductItem: LinearLayout
        var llQty: LinearLayout

        init {
            llProductItem = itemView.findViewById<View> (R.id.llProductItem) as LinearLayout
            llQty = itemView.findViewById<View> (R.id.llQTY) as LinearLayout
            tvQTY = itemView.findViewById<View>(R.id.tvQTY) as TextView
            tvPrice = itemView.findViewById<View>(R.id.tvPrice) as TextView
            weightTV = itemView.findViewById<View>(R.id.item_pack) as TextView
            nameTV = itemView.findViewById<View>(R.id.tvName) as TextView
            imageView = itemView.findViewById<View>(R.id.profileImageIV) as ImageView
            addBtn = itemView.findViewById<View>(R.id.addBtn) as ImageButton
            removeBtn = itemView.findViewById<View>(R.id.removeBtn) as ImageButton
            ibEdit = itemView.findViewById<View>(R.id.ibEdit) as ImageButton
            qtyET = itemView.findViewById<View>(R.id.qtyET) as EditText
        }
    }

    private fun isProductExists(cart: CartOld?, currentProduct: ProductOld): ProductOld? {
        if (cart!!.getOldProducts() != null) {
            for (product in cart.getOldProducts()!!) {
                if (product.getId().equals(currentProduct.getId(), true)) {
                    return product
                }
            }
        }
        if (cart.getPreviousProducts() != null) {
            for (product in cart.getPreviousProducts()!!) {
                if (product.getId().equals(currentProduct.getId(), true)) {
                    return product
                }
            }
        }
        return null
    }

    fun notifyChanges() {
        val cart1 = realm.where(CartOld::class.java).equalTo("id", cart!!.getId()).findFirst()
        cart = cart1
        notifyDataSetChanged()
        (activity as CatalogActivity).updateCount(cart)
    }

    fun changeProductListData(products: MutableList<Product>?){
        this.products = products
        notifyDataSetChanged()

    }

    init {
        realm = Realm.getDefaultInstance()
        this.bundle = bundle
        this.products = products
        productClickListener = activity as IProductClickListener?

        val eproducts = Realm.getDefaultInstance().where(ProductOld::class.java).findAll()
        productsOld = Realm.getDefaultInstance().copyFromRealm(eproducts)
        this.cart_id = cart_id
        if (cart_id == -1) {
            val num = realm.where(OrderOld::class.java).max("id")
            val nextID: Int
            nextID = if (num == null) {
                1
            } else {
                num.toInt() + 1
            }
            cart = CartOld()
            cart!!.setId(nextID)
            cart!!.setName(name)
        } else {
            if (activity.javaClass.simpleName === CatalogActivity::class.java.getSimpleName()) {
                cart = realm.where(CartOld::class.java).equalTo("id", cart_id).findFirst()
                (activity as CatalogActivity).updateCount(cart)
            }
        }
    }
}
