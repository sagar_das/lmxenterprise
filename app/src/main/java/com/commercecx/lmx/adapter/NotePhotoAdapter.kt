package com.commercecx.lmx.adapter

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.app.Activity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.commercecx.lmx.R
import com.commercecx.lmx.util.NotePhoto


class NotePhotoAdapter(notePhotosList: List<NotePhoto>, activity: Activity) :
    RecyclerView.Adapter<NotePhotoAdapter.PhotoHolder?>() {
    private var notePhotosList: List<NotePhoto>
    private val activity: Activity

    interface NotePhotoAdapterClickListener {
        fun onPhotoClick(photo: NotePhoto?, position: Int)
        fun onPhotoDelete()
    }

    private val mClickListener: NotePhotoAdapterClickListener
    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): PhotoHolder {
        val v: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.notes_attached_images, parent, false)
        return PhotoHolder(v)
    }

    override fun onBindViewHolder(@NonNull holder: PhotoHolder, position: Int) {
        val notePhoto: NotePhoto = notePhotosList[position]
        holder.imageView.setImageDrawable(notePhoto.bitmapDrawable)
        holder.imageView.setOnClickListener { mClickListener.onPhotoClick(notePhoto, position) }
        holder.btnDelete.setOnClickListener { mClickListener.onPhotoDelete() }
    }

    inner class PhotoHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: ImageView
        var btnDelete: ImageButton

        init {
            imageView = itemView.findViewById<View>(R.id.ivAttachedImage) as ImageView
            btnDelete = itemView.findViewById<View>(R.id.btnDelete) as ImageButton
        }
    }

    override fun getItemCount(): Int {
        return notePhotosList.size
    }

    fun swapDataSet(notePhotosList: List<NotePhoto>) {
        this.notePhotosList = notePhotosList
        notifyDataSetChanged()
    }

    init {
        this.notePhotosList = notePhotosList
        this.activity = activity
        mClickListener = activity as NotePhotoAdapterClickListener
    }
}
