package com.commercecx.lmx.adapter;

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.print.PrintAttributes;
import android.print.pdf.PrintedPdfDocument;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.commercecx.lmx.R;
import com.commercecx.lmx.activity.ReOrderActivity;
import com.commercecx.lmx.model.OrderOld;
import com.commercecx.lmx.model.ProductOld;
import com.commercecx.lmx.model.RouteOld;
import com.commercecx.lmx.util.NotePhoto;
import com.commercecx.lmx.view.JDEExpandableRecyclerView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

import static android.content.Context.PRINT_SERVICE;


public class NewOrderAdapter extends JDEExpandableRecyclerView.Adapter<NewOrderAdapter.ChildViewHolder, JDEExpandableRecyclerView.SimpleGroupViewHolder, String, String> {


    private Context context;
    private ArrayList<OrderOld> orders;
    SharedPreferences preferences;
    RouteOld route;

    public NewOrderAdapter(Context context, ArrayList<OrderOld> orders, RouteOld route) {
        this.context = context;
        this.orders = orders;
        this.route = route;
    }


    @Override
    public int getGroupItemCount() {
        return orders.size() - 1;
    }

    @Override
    public int getChildItemCount(int group) {
        //Logger.debug("" + group);
        OrderOld order = orders.get(group);
        List<ProductOld> products = order.getProducts();
        return products.size();
    }

    @Override
    public String getGroupItem(int position) {
        //Logger.debug("" + position);
        OrderOld order = orders.get(position);
        return order.getName();
    }

    @Override
    public String getChildItem(int group, int position)
    {
        RealmList<ProductOld> products = orders.get(group).getProducts();
        ProductOld product = products.get(position);
        return product.getName();
    }

    @Override
    protected JDEExpandableRecyclerView.SimpleGroupViewHolder onCreateGroupViewHolder(ViewGroup parent) {
        return new JDEExpandableRecyclerView.SimpleGroupViewHolder(parent.getContext());
    }

    @Override
    protected ChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_drawer, parent, false);
        return new ChildViewHolder(view);
    }

    @Override
    public int getChildItemViewType(int group, int position) {
        return 1;
    }

    @Override
    public void onBindGroupViewHolder(JDEExpandableRecyclerView.SimpleGroupViewHolder holder, int group) {
        super.onBindGroupViewHolder(holder, group);
        final JDEExpandableRecyclerView.SimpleGroupViewHolder usableHolder = holder;
        final OrderOld order = orders.get(group);
        holder.setText(getGroupItem(group));
        holder.getTextView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ReOrderActivity.class);
                intent.putExtra("order_id",order.getId());
                context.startActivity(intent);
            }
        });

        if(group <= orders.size() - 2){
            holder.getShareButton().setColorFilter(ContextCompat.getColor(context, R.color.light_blue101));
        }
        else{
            holder.getShareButton().setColorFilter(ContextCompat.getColor(context, R.color.orange101));
        }

        holder.getShareButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // region Generating content for PDF

                preferences = PreferenceManager.getDefaultSharedPreferences(context);



                PrintAttributes printAttrs = new PrintAttributes.Builder().
                setColorMode(PrintAttributes.COLOR_MODE_COLOR).
                setMediaSize(PrintAttributes.MediaSize.NA_LETTER).
                setResolution(new PrintAttributes.Resolution("zooey", PRINT_SERVICE, 300, 300)).
                setMinMargins(PrintAttributes.Margins.NO_MARGINS).
                build();

                PdfDocument document = new PrintedPdfDocument(v.getContext(),printAttrs);
                PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(598, 842, 1).create();
                PdfDocument.Page page = document.startPage(pageInfo);



                Canvas canvas = page.getCanvas();
                final float textSize = 14f;
                Paint paint = new Paint();
                paint.setTextSize(textSize);
                int x = 40;
                int y = 40;
                String companyLogoPath = preferences.getString("prefCompanyLogo","no_pic");
                if(!companyLogoPath.equalsIgnoreCase("no_pic"))
                {
                    NotePhoto photo = new NotePhoto(companyLogoPath,(Activity)context);
                    Bitmap image = photo.getBitmap();
                    Bitmap resizedImage = Bitmap.createScaledBitmap(image, 100, 100, false);
                    canvas.drawBitmap(resizedImage,440,10,paint);
                }
                else{

                    Bitmap image = BitmapFactory.decodeResource(context.getResources(), R.drawable.ccx_logo);
                    Bitmap resizedImage = Bitmap.createScaledBitmap(image, 100, 60, false);
                    canvas.drawBitmap(resizedImage,440,10,paint);

                }
                canvas.drawText(preferences.getString("prefCompanyName", ""),x,y,paint);
                //canvas.drawText("Order Details: ",x,y+20,paint);
                canvas.drawText("Representative: "+preferences.getString("prefFirstName", "Guest")+" "+preferences.getString("prefLastName", ""),x,y+20,paint);
                //canvas.drawText("Order name : "+order.getName()+"    Date : "+order.getDate(),x,y+40,paint);
                canvas.drawText("Customer details : ",x,y+40,paint);

                canvas.drawText(route.getCustomerName(),x,y+60,paint);
                canvas.drawText(route.getAddress(),x,y+80,paint);
                canvas.drawText("Contact: "+route.getCustomerPhoneNo(),x,y+100,paint);
                canvas.drawText("Date: "+order.getDate(),x+400,y+80,paint);
                canvas.drawText("Order: "+order.getName(),x+400,y+100,paint);
                canvas.drawText("#",x,y+130,paint);
                canvas.drawText("Product",x+70,y+130,paint);
                canvas.drawText("Qty",x+300,y+130,paint);
                canvas.drawText("Price",x+400,y+130,paint);
                canvas.drawLine(40.0f,150.0f,550.0f,150.0f,paint);
                canvas.drawLine(40.0f,175.0f,550.0f,175.0f,paint);
                y = y+140;
                int index = 1;
                for (ProductOld product : order.getProducts()) {
                canvas.drawText(Integer.toString(index),x,y+10,paint);
                canvas.drawText(product.getName(),x+70,y+10,paint);
                canvas.drawText(product.getItem_qty(),x+300,y+10,paint);
                canvas.drawText(product.getItem_price(),x+400,y+10,paint);
                y+=20;
                index+=1;
            }
                canvas.drawLine(x,y+20,550.0f,y+20,paint);
                canvas.drawText("Subtotal :",x+300,y+40,paint);
                canvas.drawText("$"+order.getSub_total(),x+400,y+40,paint);
                canvas.drawText("Tax :",x+300,y+60,paint);
                canvas.drawText("$"+order.getTax(),x+400,y+60,paint);
                canvas.drawText("Total :",x+300,y+80,paint);
                canvas.drawText("$"+order.getTotal(),x+400,y+80,paint);




                //content.draw(page.getCanvas());


                document.finishPage(page);

                Uri contentUri;

                try {
                    File pdfDirPath = new File(v.getContext().getFilesDir(), "pdfs");
                    pdfDirPath.mkdirs();
                    File pdfFile = new File(pdfDirPath, "lmx-order-"+order.getName()+".pdf");
                    contentUri = FileProvider.getUriForFile(v.getContext(), "com.commercecx.lmx.fileprovider", pdfFile);
                    OutputStream outputStream = new FileOutputStream(pdfFile);
                    document.writeTo(outputStream);
                    document.close();
                    outputStream.close();
                } catch (IOException e) {
                    throw new RuntimeException("Error generating file", e);
                }

                // endregion

                String contentToBeShared = order.getName()+"\n"+order.getTotal();
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                //sendIntent.putExtra(Intent.EXTRA_TEXT, contentToBeShared);
                //sendIntent.setType("text/plain");
                sendIntent.setType("application/pdf");
                sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                    "Invoice for Order "+order.getName());
                sendIntent.putExtra(Intent.EXTRA_STREAM,contentUri);

                Intent shareIntent = Intent.createChooser(sendIntent, null);

               // v.getContext().startActivity(sendIntent);
            }
        });
    }

    @Override
    public void onBindChildViewHolder(ChildViewHolder holder, int group, final int position) {
        super.onBindChildViewHolder(holder, group, position);
        holder.tvItemName.setText(getChildItem(group, position));
        RealmList<ProductOld> products = orders.get(group).getProducts();
        ProductOld product = products.get(position);
        holder.item_qty.setText("QTY : " + product.getItem_qty());




    }

    public class ChildViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvItemName;
        private final TextView item_qty;
        //private final ImageView ivOrderStatus;

        public ChildViewHolder(View itemView) {
            super(itemView);
            tvItemName = (TextView) itemView.findViewById(R.id.item_name);
            item_qty = (TextView) itemView.findViewById(R.id.item_qty);
            //ivOrderStatus = (ImageView) itemView.findViewById(R.id.ivShare);
        }
    }

}
