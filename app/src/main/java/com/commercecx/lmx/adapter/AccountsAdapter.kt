package com.commercecx.lmx.adapter

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.commercecx.lmx.R
import com.commercecx.lmx.model.Account
import com.commercecx.lmx.model.Product

/**
 * Created by Sagar Das on 8/25/20.
 */

class AccountsAdapter(
    private val activity: Activity,
    accounts: MutableList<Account>?,
    private val context: Context
) : RecyclerView.Adapter<AccountsAdapter.ViewHolder?>() {

    private var accounts: MutableList<Account>? = null
    var account: Account? = null

    interface IAccountClickListener {
        fun onAccountClick(account: Account?)
    }

    var accountClickListener: IAccountClickListener? = null

    init {
        this.accounts = accounts
        accountClickListener = activity as IAccountClickListener?
    }



    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName: TextView
        var tvDesc: TextView
        var llAccountItem: LinearLayout


        init {
            llAccountItem = itemView.findViewById<View>(R.id.llAccountItem) as LinearLayout
            tvName = itemView.findViewById<View>(R.id.tvName) as TextView
            tvDesc = itemView.findViewById<View>(R.id.tvDescription) as TextView

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(activity).inflate(R.layout.li_account_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        account = accounts!![position]
        holder.tvName.text = account!!.name
        holder.tvDesc.text = account!!.description

        holder.llAccountItem.setOnClickListener {
            account = accounts!![position]
            accountClickListener!!.onAccountClick(account)
        }

    }

    override fun getItemCount(): Int {
        return accounts!!.size
    }
}