package com.commercecx.lmx.adapter

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.content.Intent

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.commercecx.lmx.R
import com.commercecx.lmx.activity.CatalogActivity
import com.commercecx.lmx.activity.ReOrderActivity
import com.commercecx.lmx.activity.ReviewCartActivity
import com.commercecx.lmx.model.CartOld
import com.commercecx.lmx.model.OrderOld
import com.commercecx.lmx.model.ProductOld
import com.commercecx.lmx.util.Logger
import com.commercecx.lmx.view.JDECustomTextView
import com.commercecx.lmx.view.JDESwipeLayout
import io.realm.Realm


class ReOrderAdapter(context: ReOrderActivity, order: OrderOld) :
    RecyclerView.Adapter<ReOrderAdapter.ViewHolder?>() {
    //    private final int COUNT = 9;
    private val itemsOffset: IntArray
    private val context: ReOrderActivity
    private val order: OrderOld
    private val realm: Realm
    private val cart: CartOld
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View =
            LayoutInflater.from(parent.context).inflate(R.layout.list_re_order, parent, false)
        val viewHolder = ViewHolder(itemView)
        val onClick = View.OnClickListener { viewHolder.swipeLayout.animateReset() }
        if (viewHolder.rightView != null) {
            viewHolder.rightView.isClickable = true
            viewHolder.rightView.setOnClickListener(onClick)
        }
        viewHolder.swipeLayout.setOnSwipeListener(object : JDESwipeLayout.OnSwipeListener {
            override fun onBeginSwipe(swipeLayout: JDESwipeLayout?, moveToRight: Boolean) {}
            override fun onSwipeClampReached(swipeLayout: JDESwipeLayout, moveToRight: Boolean) {
                swipeLayout.animateReset()
            }

            override fun onLeftStickyEdge(swipeLayout: JDESwipeLayout?, moveToRight: Boolean) {}
            override fun onRightStickyEdge(swipeLayout: JDESwipeLayout?, moveToRight: Boolean) {}
        })
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.textViewPos.setText("#" + (position + 1));
//        holder.swipeLayout.setOffset(itemsOffset[position]);
        val product: ProductOld = order.getProducts()!!.get(position)!!
        holder.nameTV.setText(product.getName())
        holder.weightTV.setText(product.getItem_pack())
        holder.qtyET.setText(product.getItem_qty())
        holder.removeBtn.setOnClickListener {
            realm.beginTransaction() //open the database
            cart.getPreviousProducts()!!.remove(product)
            realm.commitTransaction()
            notifyDataSetChanged()
        }
        holder.qtyET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (!editable.toString().isEmpty()) {
                    val dbProduct: ProductOld? = isProductExists(cart, product)
                    if (dbProduct != null) {
                        realm.beginTransaction() //open the database
                        dbProduct.setItem_qty(editable.toString())
                        cart.getPreviousProducts()!!.remove(dbProduct)
                        cart.getPreviousProducts()!!.add(dbProduct)
                        realm.commitTransaction()
                        (context as ReOrderActivity).updateCount(cart)
                    }
                }
            }
        })
        context.findViewById<JDECustomTextView>(R.id.reviewCartBtn).setOnClickListener(View.OnClickListener {
            if (cart.getOldProducts() != null && cart.getOldProducts()!!
                    .size > 0 || cart.getPreviousProducts() != null && cart.getPreviousProducts()!!
                    .size > 0
            ) {
                Logger.debug("onClickReviewCartBtn...")
                val intent = Intent(context, ReviewCartActivity::class.java)
                intent.putExtra("cart_id", cart.getId())
                context.startActivity(intent)
            } else {
                Toast.makeText(context, "No Products in the Cart", Toast.LENGTH_LONG).show()
            }
        })
        context.findViewById<LinearLayout>(R.id.add_order_ll).setOnClickListener(View.OnClickListener {
            Logger.debug("onClickReviewCartBtn...")
            val intent = Intent(context, CatalogActivity::class.java)
            intent.putExtra("cart_id", cart.getId())
            context.startActivity(intent)
        })
    }

    override fun onViewDetachedFromWindow(holder: ViewHolder) {
        if (holder.getAdapterPosition() !== RecyclerView.NO_POSITION) {
            itemsOffset[holder.getAdapterPosition()] = holder.swipeLayout.getOffset()
        }
    }

    fun onViewRecycled(holder: ViewHolder?) {
        super.onViewRecycled(holder!!)
    }



    override fun getItemCount(): Int {
        return order.getProducts()!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val swipeLayout: JDESwipeLayout
        val rightView: View?
        var nameTV: TextView
        var weightTV: TextView
        var removeBtn: TextView
        var qtyET: EditText

        init {
            swipeLayout = itemView.findViewById<View>(R.id.swipe_layout) as JDESwipeLayout
            rightView = itemView.findViewById(R.id.right_view)
            weightTV = itemView.findViewById<View>(R.id.item_pack) as TextView
            nameTV = itemView.findViewById<View>(R.id.tvName) as TextView
            removeBtn = itemView.findViewById<View>(R.id.removeBtn) as TextView
            qtyET = itemView.findViewById<View>(R.id.qtyET) as EditText
        }
    }

    private fun isProductExists(cart: CartOld, currentProduct: ProductOld): ProductOld? {
        if (cart.getPreviousProducts() != null) {
            for (product in cart.getPreviousProducts()!!) {
                if (product.getId().equals(currentProduct.getId(), true)) {
                    return product
                }
            }
        }
        return null
    }

    init {
        this.context = context
        this.order = order
        itemsOffset = IntArray(order.getProducts()!!.size)
        realm = Realm.getDefaultInstance()
        val num: Number = realm.where(OrderOld::class.java).max("id")!!
        val nextID: Int
        nextID = if (num == null) {
            1
        } else {
            num.toInt() + 1
        }
        cart = CartOld()
        cart.setId(nextID)
        cart.setName(order.getName())
        cart.setPreviousProducts(order.getProducts())
        realm.executeTransaction { realm ->
            realm.insertOrUpdate(cart)
            (context as ReOrderActivity).updateCount(cart)
        }
    }
}

