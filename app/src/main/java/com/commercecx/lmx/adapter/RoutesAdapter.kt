package com.commercecx.lmx.adapter

/**
 * Created by Sagar Das on 8/19/20.
 */

import android.app.AlertDialog

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.NonNull
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.commercecx.lmx.R
import com.commercecx.lmx.activity.AddRouteActivity
import com.commercecx.lmx.activity.NewOrderActivity
import com.commercecx.lmx.model.OrderOld
import com.commercecx.lmx.model.RecentActivityDash
import com.commercecx.lmx.model.RouteOld
import com.commercecx.lmx.view.JDECircleImageView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.realm.Realm
import org.osmdroid.util.GeoPoint
import java.io.IOException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class RoutesAdapter(
    fragment: Fragment,
    private val context: Context,
    private val layoutInflater: LayoutInflater,
    routes: MutableList<RouteOld>
) :
    RecyclerView.Adapter<RoutesAdapter.ViewHolder?>() {
    private val gson: Gson
    private var routes: MutableList<RouteOld>? = null
    private val completedRoutes: MutableList<RouteOld>
    private val dialog: BottomSheetDialog? = null
    private var resourceId = 0
    private var resourceWhiteId = 0
    private var firstCompletedItem: Boolean
    private val realm: Realm
    private var completedIndex: Int
    private var isCompletedIndex = false
    private var orderList: List<OrderOld?>? = null

    interface IRoutesCompletedListener {
        fun onRouteCompleted()
    }

    var mRoutes: IRoutesCompletedListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.li_customers, parent, false)
        )
    }

    override fun onBindViewHolder(@NonNull holder: ViewHolder, position: Int) {
        val route = routes!![position]

        //Setting up image
        if (route.getImage() != null && !route.getImage()!!.isEmpty()) {
            val res = context.resources
            resourceId = res.getIdentifier(
                route.getImage(), "drawable", context.packageName
            )
        } else {
            val res = context.resources
            resourceId = res.getIdentifier(
                "ic_directions_car", "drawable", context.packageName
            )
            resourceWhiteId = res.getIdentifier(
                "ic_directions_car_white", "drawable", context.packageName
            )
        }
        holder.bookmarkIV.setOnClickListener {
            val intent = Intent(context, AddRouteActivity::class.java)
            val b = Bundle()
            b.putSerializable("ROUTE_DATA", route)
            intent.putExtra("ROUTE_BUNDLE", b)
            context.startActivity(intent)
        }


        //Route not completed
        if (route.isCompleted() === false) {
            holder.cvLarge.setVisibility(View.VISIBLE)
            holder.cvSmall.setVisibility(View.GONE)
            holder.nameTV.text = route.getName()
            holder.addressTV.text = route.getAddress()
            holder.dateTV.text = route.getDate()
            holder.circleImageView.setImageResource(resourceId)
            if (position == 0) {
                holder.circleImageView.setImageResource(resourceWhiteId)
                holder.bookmarkIV.setBackgroundColor(context.resources.getColor(R.color.orange101))
                holder.nameTV.setTextColor(context.resources.getColor(android.R.color.white))
                holder.addressTV.setTextColor(context.resources.getColor(android.R.color.white))
                holder.dateTV.setTextColor(context.resources.getColor(android.R.color.white))
                holder.tvNavigate.setTextColor(context.resources.getColor(android.R.color.white))
                holder.tvNewOrder.setTextColor(context.resources.getColor(android.R.color.white))
                holder.ibEditRoute.setColorFilter(Color.argb(255, 255, 255, 255))
                holder.ibDeleteRoute.setColorFilter(Color.argb(255, 255, 255, 255))
                holder.ibNotify.setColorFilter(Color.argb(255, 255, 255, 255))
                holder.ibNavigate.setColorFilter(Color.argb(255, 255, 255, 255))
                holder.ibNewOrder.setColorFilter(Color.argb(255, 255, 255, 255))
                holder.vOne.setBackgroundColor(context.resources.getColor(android.R.color.white))
                holder.vTwo.setBackgroundColor(context.resources.getColor(android.R.color.white))
                holder.llOptions.visibility = View.VISIBLE
                holder.tvHeader.visibility = View.VISIBLE
                holder.tvHeader.text = "Ongoing"
                orderList = realm.where(OrderOld::class.java).equalTo("route_id", route.getId()).findAll()

                if (orderList!!.size > 0) {
                    val order = orderList!![orderList!!.size - 1]
                    holder.tvOrderName.text = order!!.getName()
                    holder.tvOrderDesc.text = "This order has " + Integer.toString(
                        order.getProducts()!!.size
                    ) + " products. "
                } else {
                    holder.tvOrderName.text = "No orders found"
                    holder.tvOrderDesc.text = ""
                }
            }
            if (position == 1) {
                holder.tvHeader.visibility = View.VISIBLE
                holder.tvHeader.text = "Up Next"
            }
            if (route.isActive()) {
                holder.ivDelegated.visibility = View.VISIBLE
            }
        } else {
            holder.cvLarge.setVisibility(View.GONE)
            holder.cvSmall.setVisibility(View.VISIBLE)
            holder.nameTVDone.text = route.getName()
            holder.addressTVDone.text = route.getAddress()
            holder.dateTVDone.text = route.getDate()
            holder.circleImageViewDone.setImageResource(resourceId)
            if (position == completedIndex) {
                holder.tvHeader.visibility = View.VISIBLE
                holder.tvHeader.text = "Completed"
                firstCompletedItem = true
            }
        }
        holder.newOrder.setOnClickListener {
            val intent = Intent(context, NewOrderActivity::class.java)
            val b = Bundle()
            b.putSerializable("ROUTE_DATA", route)
            intent.putExtra("ROUTE_BUNDLE", b)
            context.startActivity(intent)
        }
        holder.startNav.setOnClickListener(View.OnClickListener { view -> //                Intent intent = new Intent(context, NavigationActivity.class);
            //                Bundle b = new Bundle();
            //                b.putSerializable("ROUTE_DATA",route);
            //                intent.putExtra("ROUTE_BUNDLE",b);
            //                context.startActivity(intent);
            val dialogBuilder = AlertDialog.Builder(view.context)
            val completeDialog =
                LayoutInflater.from(context).inflate(R.layout.map_selection_dialog, null)
            dialogBuilder.setView(completeDialog)
            val dialog = dialogBuilder.create()
            val btnGoogleMaps = completeDialog.findViewById<View>(R.id.btnGoogleMaps) as Button
            val btnWaze = completeDialog.findViewById<View>(R.id.btnWaze) as Button
            btnGoogleMaps.setOnClickListener(View.OnClickListener {
                if (!isGoogleMapsInstalled) {
                    Toast.makeText(context, "Please install Google maps", Toast.LENGTH_SHORT).show()
                    dialog.dismiss()
                    return@OnClickListener
                }
                dialog.dismiss()
                val gmmIntentUri = Uri.parse("google.navigation:q=" + route.getAddress())
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")
                context.startActivity(mapIntent)
            })
            btnWaze.setOnClickListener(View.OnClickListener {
                if (!isWazeInstalled) {
                    Toast.makeText(context, "Please install Waze", Toast.LENGTH_SHORT).show()
                    dialog.dismiss()
                    return@OnClickListener
                }
                dialog.dismiss()
                val geoPoint = getLocationFromAddress(route.getAddress())
                val uri =
                    "waze://?ll=" + geoPoint!!.latitude + "," + geoPoint.longitude + "&navigate=yes"
                val gmmIntentUri = Uri.parse(uri)
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.waze")
                context.startActivity(mapIntent)
            })
            dialog.show()
        })
        holder.ibEditRoute.setOnClickListener { v ->
            val dialogBuilder = AlertDialog.Builder(v.context)
            val completeDialog =
                LayoutInflater.from(context).inflate(R.layout.edit_route_dialog, null)
            dialogBuilder.setView(completeDialog)
            val dialog = dialogBuilder.create()
            val btnYes =
                completeDialog.findViewById<View>(R.id.btnYes) as Button
            val btnNo =
                completeDialog.findViewById<View>(R.id.btnNo) as Button
            val etRouteName =
                completeDialog.findViewById<View>(R.id.etRouteName) as EditText
            val etRouteAddress =
                completeDialog.findViewById<View>(R.id.etRouteAddress) as EditText
            etRouteName.setText(route.getName())
            etRouteAddress.setText(route.getAddress())
            btnYes.setOnClickListener {
                route.setName(etRouteName.text.toString())
                route.setAddress(etRouteAddress.text.toString())
                realm.executeTransactionAsync { realm -> // realm.insertOrUpdate(route);
                    realm.copyToRealmOrUpdate(route)
                }
                notifyItemChanged(position)
                notifyDataSetChanged()
                dialog.dismiss()
            }
            btnNo.setOnClickListener { dialog.dismiss() }
            dialog.show()
        }
        holder.ibDeleteRoute.setOnClickListener { v ->
            val dialogBuilder = AlertDialog.Builder(v.context)
            val discardDialog =
                LayoutInflater.from(context).inflate(R.layout.delete_route_dialog, null)
            dialogBuilder.setView(discardDialog)
            val dialog = dialogBuilder.create()
            val discard =
                discardDialog.findViewById<View>(R.id.btnDiscard) as Button
            val cancel =
                discardDialog.findViewById<View>(R.id.btnCancel) as Button
            discard.setOnClickListener {
                val d = Date()
                val dateFormat: DateFormat = SimpleDateFormat("MM-dd-yyyy")
                val strDate = dateFormat.format(d)
                val updatedTSmp = d.time
                val recentActivity = RecentActivityDash()
                val raResults = realm.where(
                    RecentActivityDash::class.java
                ).findAll()
                val raId = if (raResults.size > 0) (raResults[0])!!.getId() + 1 else 1
                recentActivity.setId(raId)
                recentActivity.setActivity_name("Deleted a Route")
                recentActivity.setActivity_date(strDate)
                realm.executeTransaction { realm ->
                    val result = realm.where(RouteOld::class.java).equalTo("id", route.getId())
                        .findAll()
                    result.deleteAllFromRealm()
                    realm.insertOrUpdate(recentActivity)
                }
                routes!!.removeAt(position)
                notifyItemChanged(position)
                //                        DashboardAdapter.this.notify();
                notifyDataSetChanged()
                dialog.dismiss()
            }
            cancel.setOnClickListener { dialog.dismiss() }
            dialog.show()
        }
        holder.ibNotify.setOnClickListener { //Show confirmation dialog
            val dialogBuilder = AlertDialog.Builder(
                context
            )
            val completeDialog = layoutInflater.inflate(R.layout.complete_dialog, null)
            dialogBuilder.setView(completeDialog)
            val dialog = dialogBuilder.create()
            val btnYes =
                completeDialog.findViewById<View>(R.id.btnYes) as Button
            val btnNo =
                completeDialog.findViewById<View>(R.id.btnNo) as Button
            btnYes.setOnClickListener { //Re-shuffling the items
                val route = routes!![position]
                realm.executeTransaction { realm ->
                    route.setCompleted(true)
                    realm.copyToRealmOrUpdate(route)
                }
                routes!!.removeAt(position)
                routes!!.add(route)
                firstCompletedItem = false
                completedIndex -= 1
                notifyItemMoved(position, routes!!.size - 1)
                // notifyDataSetChanged();
                mRoutes.onRouteCompleted()
                dialog.dismiss()
            }
            btnNo.setOnClickListener { dialog.dismiss() }
            dialog.show()
        }
    }



    override fun getItemCount(): Int {
        return routes!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nameTV: TextView
        var nameTVDone: TextView
        var dateTV: TextView
        var dateTVDone: TextView
        var addressTV: TextView
        var addressTVDone: TextView
        var tvNavigate: TextView
        var tvNewOrder: TextView
        var tvHeader: TextView
        var tvHeaderDate: TextView
        var tvOrderName: TextView
        var tvOrderDesc: TextView
        var bookmarkIV: LinearLayout
        var newOrder: LinearLayout
        var startNav: LinearLayout
        var llOptions: LinearLayout
        var ibEditRoute: ImageButton
        var ibDeleteRoute: ImageButton
        var ibNotify: ImageButton
        var ibNavigate: ImageButton
        var ibNewOrder: ImageButton
        var ivDelegated: ImageView
        var cvLarge: CardView
        var cvSmall: CardView
        var vOne: View
        var vTwo: View
        var circleImageView: JDECircleImageView
        var circleImageViewDone: JDECircleImageView

        init {
            addressTV = itemView.findViewById<View>(R.id.tvAddress) as TextView
            addressTVDone = itemView.findViewById<View>(R.id.tvAddressDone) as TextView
            dateTV = itemView.findViewById<View>(R.id.tvDate) as TextView
            dateTVDone = itemView.findViewById<View>(R.id.tvDateDone) as TextView
            nameTV = itemView.findViewById<View>(R.id.tvName) as TextView
            nameTVDone = itemView.findViewById<View>(R.id.tvNameDone) as TextView
            tvHeader = itemView.findViewById<View>(R.id.tvHeader) as TextView
            tvHeaderDate = itemView.findViewById<View>(R.id.tvHeaderDate) as TextView
            tvOrderName = itemView.findViewById<View>(R.id.tvOrderName) as TextView
            tvOrderDesc = itemView.findViewById<View>(R.id.tvOrderDesc) as TextView
            bookmarkIV = itemView.findViewById<View>(R.id.bookmark) as LinearLayout
            llOptions = itemView.findViewById<View>(R.id.llOptions) as LinearLayout
            newOrder = itemView.findViewById<View>(R.id.new_order) as LinearLayout
            startNav = itemView.findViewById<View>(R.id.start_nav) as LinearLayout
            tvNavigate = itemView.findViewById<View>(R.id.tvNavigate) as TextView
            tvNewOrder = itemView.findViewById<View>(R.id.tvNewOrder) as TextView
            ivDelegated = itemView.findViewById<View>(R.id.ivDelegated) as ImageView
            //  this.smallDivider = (View) itemView.findViewById(R.id.small_divider);
            //  this.bigDivider = (View) itemView.findViewById(R.id.big_divider);
            circleImageView = itemView.findViewById<View>(R.id.profileImageIV) as JDECircleImageView
            circleImageViewDone =
                itemView.findViewById<View>(R.id.profileImageIVDone) as JDECircleImageView
            ibEditRoute = itemView.findViewById<View>(R.id.ibEditRoute) as ImageButton
            ibDeleteRoute = itemView.findViewById<View>(R.id.ibDeleteRoute) as ImageButton
            ibNotify = itemView.findViewById<View>(R.id.ibNotification) as ImageButton
            ibNavigate = itemView.findViewById<View>(R.id.ibNavigate) as ImageButton
            ibNewOrder = itemView.findViewById<View>(R.id.ibNewOrder) as ImageButton
            vOne = itemView.findViewById(R.id.vOne) as View
            vTwo = itemView.findViewById(R.id.vTwo) as View
            cvLarge = itemView.findViewById<View>(R.id.cvLarge) as CardView
            cvSmall = itemView.findViewById<View>(R.id.cvSmall) as CardView

            //  this.routeBtn = (TextView) itemView.findViewById(R.id.routeBtn);
        }
    }

    val isGoogleMapsInstalled: Boolean
        get() = try {
            val info = context.packageManager.getApplicationInfo("com.google.android.apps.maps", 0)
            if (!info.enabled) false else true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    val isWazeInstalled: Boolean
        get() {
            return try {
                val info = context.packageManager.getApplicationInfo("com.waze", 0)
                if (!info.enabled) false else true
            } catch (e: PackageManager.NameNotFoundException) {
                false
            }
        }

    fun getLocationFromAddress(strAddress: String?): GeoPoint? {
        val coder = Geocoder(context)
        val address: List<Address>?
        var p1: GeoPoint? = null
        return try {
            address = coder.getFromLocationName(strAddress, 5)
            if (address == null || address.size == 0) {
                return null
            }
            val location = address[0]
            location.latitude
            location.longitude
            p1 = GeoPoint(
                location.latitude,
                location.longitude
            )
            p1
        } catch (e: IOException) {
            null
        }
    }

    init {
        realm = Realm.getDefaultInstance()
        val gsonBuilder = GsonBuilder()
        gson = gsonBuilder.create()
        firstCompletedItem = false
        mRoutes = fragment as IRoutesCompletedListener
        completedIndex = routes.size
        completedRoutes = ArrayList()
        if (routes.size > 0) {
            for (route in routes) {
                if (route.isCompleted()) {
                    completedRoutes.add(route)
                }
            }
            for (route in completedRoutes) {
                if (!isCompletedIndex) {
                    completedIndex = route.getIndex()
                    isCompletedIndex = true
                }
                routes.remove(route)
            }
            routes.addAll(completedRoutes)
            this.routes = routes
        }
    }
}
