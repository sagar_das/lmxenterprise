package com.commercecx.lmx.adapter

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.content.Context

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.commercecx.lmx.R
import com.commercecx.lmx.activity.ReviewCartActivity
import com.commercecx.lmx.model.CartOld
import com.commercecx.lmx.model.ProductOld
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection
import io.realm.Realm


class ReviewCartAdapter(private val context: Context, val topic: Int, cart: CartOld) :
    StatelessSection(R.layout.li_header_review_cart, R.layout.li_item_review_cart) {
    private val cart: CartOld
    private val realm: Realm
    override fun getContentItemsTotal(): Int {
        return if (topic == NEW_ORDER) {
            cart.getOldProducts()!!.size
        } else {
            cart.getPreviousProducts()!!.size
        }
    }

    override fun getItemViewHolder(view: View): RecyclerView.ViewHolder {
        return ItemViewHolder(view)
    }

    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemHolder = holder as ItemViewHolder
        var product: ProductOld? = null
        if (topic == NEW_ORDER) {
            product = cart.getOldProducts()!!.get(position)
            itemHolder.productImg.visibility = View.VISIBLE
            itemHolder.imageRl.visibility = View.VISIBLE
            val params = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
            )
            params.addRule(RelativeLayout.RIGHT_OF, R.id.imageRl)
            itemHolder.catlogLL.layoutParams = params
        } else {
            product = cart.getPreviousProducts()!!.get(position)
            itemHolder.productImg.visibility = View.GONE
            itemHolder.imageRl.visibility = View.GONE
            val params = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
            )
            params.addRule(RelativeLayout.RIGHT_OF, 0)
            itemHolder.catlogLL.layoutParams = params
        }
        val product1: ProductOld? = product
        itemHolder.productName.setText(product!!.getName())
        itemHolder.productPack.setText(product!!.getItem_pack())
        itemHolder.productQuantity.setText(product!!.getItem_qty())
        itemHolder.productPrice.setText(product!!.getItem_price())
        if (product!!.getImage() != null && !product!!.getImage()!!.isEmpty()) {
            val res = context.resources
            val resourceId = res.getIdentifier(
                product!!.getImage(), "drawable", context.packageName
            )
            (holder as ItemViewHolder).productImg.setImageResource(resourceId)
        } else {
            val res = context.resources
            val resourceId = res.getIdentifier(
                "user_icon", "drawable", context.packageName
            )
            (holder as ItemViewHolder).productImg.setImageResource(resourceId)
        }
        itemHolder.deleteBtn.setOnClickListener {
            realm.beginTransaction() //open the database
            cart.getOldProducts()!!.remove(product1)
            realm.commitTransaction()
            (context as ReviewCartActivity).setHomeFeedAdapter()
            (context as ReviewCartActivity).calculateTotalAmount()
        }
        itemHolder.productQuantity.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (!editable.toString().isEmpty()) {
                    val dbProduct: ProductOld? = isProductExists(cart, product1)
                    realm.beginTransaction() //open the database
                    dbProduct!!.setItem_qty(editable.toString())
                    cart.getOldProducts()!!.remove(dbProduct)
                    cart.getOldProducts()!!.add(dbProduct)
                    realm.insertOrUpdate(cart)
                    realm.commitTransaction()
                    (context as ReviewCartActivity).calculateTotalAmount()
                }
            }
        })
    }

    override fun getHeaderViewHolder(view: View): RecyclerView.ViewHolder {
        return HeaderViewHolder(view)
    }

    override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder) {
        val headerHolder = holder as HeaderViewHolder
        if (topic == NEW_ORDER) {
            headerHolder.cartCountTV.setText(
                cart.getOldProducts()!!.size.toString() + " products added"
            )
        } else {
            headerHolder.cartCountTV.setText(
                cart.getPreviousProducts()!!.size.toString() + " products added"
            )
        }
    }

    private fun isProductExists(cart: CartOld, currentProduct: ProductOld?): ProductOld? {
        if (cart.getOldProducts() != null) {
            for (product in cart.getOldProducts()!!) {
                if (product.getId().equals(currentProduct!!.getId(), true)) {
                    return product
                }
            }
        }
        if (cart.getPreviousProducts() != null) {
            for (product in cart.getPreviousProducts()!!) {
                if (product.getId().equals(currentProduct!!.getId(), true)) {
                    return product
                }
            }
        }
        return null
    }

    companion object {
        const val NEW_ORDER = 0
        const val OLD_ORDER = 1
    }

    init {
        this.cart = cart
        realm = Realm.getDefaultInstance()
    }
}

internal class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val cartCountTV: TextView

    init {
        cartCountTV = view.findViewById(R.id.cartCountTV)
    }
}

internal class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val rootView: View
    val productImg: ImageView
    val productName: TextView
    val productQuantity: EditText
    val productPack: TextView
    val productPrice: TextView
    val deleteBtn: TextView
    val imageRl: RelativeLayout
    val catlogLL: LinearLayout

    init {
        rootView = view.findViewById(R.id.itemView) as View
        productImg = view.findViewById<View>(R.id.profileImageIV) as ImageView
        productName = view.findViewById<View>(R.id.tvName) as TextView
        productQuantity = view.findViewById<View>(R.id.qtyET) as EditText
        productPack = view.findViewById<View>(R.id.item_pack) as TextView
        productPrice = view.findViewById<View>(R.id.item_price) as TextView
        deleteBtn = view.findViewById<View>(R.id.removeBtn) as TextView
        imageRl = view.findViewById<View>(R.id.imageRl) as RelativeLayout
        catlogLL = view.findViewById<View>(R.id.catlogLL) as LinearLayout
    }
}