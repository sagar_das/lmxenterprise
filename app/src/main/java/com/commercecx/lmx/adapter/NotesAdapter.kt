package com.commercecx.lmx.adapter

/**
 * Created by Sagar Das on 8/19/20.
 */

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.commercecx.lmx.R
import com.commercecx.lmx.activity.AddNoteActivity
import com.commercecx.lmx.activity.NoteDetailActivity
import com.commercecx.lmx.constant.Constant
import com.commercecx.lmx.model.Note
import com.commercecx.lmx.util.FileUtil
import com.commercecx.lmx.view.JDECustomTextView
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.realm.Realm
import io.realm.RealmList
import org.json.JSONArray
import org.json.JSONException
import java.util.*

internal class NotesAdapter     //        Realm realm = Realm.getDefaultInstance();
//        notes = realm.where(Note.class).findAll();
//
//        if (notes.isEmpty()) {
//            saveDataFromJson();
//        } else {
//            Activity activity = (Activity)context;
//            JDECustomTextView notesCountTV = activity.findViewById(R.id.notes_count);
//            if (notesCountTV != null)
//                notesCountTV.setText(notes.size() + " Notes");
//        }
    (private val context: Context, private var notes: List<Note>) :
    RecyclerView.Adapter<NotesAdapter.ViewHolder?>() {
    private var gson: Gson? = null
    private fun saveDataFromJson() {
        val gsonBuilder = GsonBuilder()
        gson = gsonBuilder.create()
        var jsonArray: JSONArray? = null
        try {
            jsonArray = FileUtil.getNoteJsonObject(context)!!.getJSONArray("data")
        } catch (e: JSONException) {
            e.printStackTrace()
        }
//        if (jsonArray!!.length() > 0) {
//            notes = Arrays.asList(
//                *gson!!.fromJson(
//                    jsonArray.toString(),
//                    Array<Note>::class.java
//                )
//            )
//        }
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction { realm ->
                val _notesList = RealmList<Note>()
                _notesList.addAll(notes)
                realm.insertOrUpdate(_notesList) // <-- insert unmanaged to Realm
            }
        }
        val activity = context as Activity
        (activity.findViewById<View>(R.id.notes_count) as JDECustomTextView).text =
            notes.size.toString() + " Notes"
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.li_notes, parent, false))
    }

    override fun onBindViewHolder(@NonNull holder: ViewHolder, position: Int) {
        val note = notes[position]
        holder.nameTV.text = note.getName()
        holder.remarkTV.text = note.getRemark()
        holder.dateTV.text = note.getDate()
        if (note.isActive()) {
            holder.bookmarkIV.visibility = View.VISIBLE
        } else {
            holder.bookmarkIV.visibility = View.GONE
        }
        if (note.isAttachment()) {
            holder.attachmentIV.visibility = View.VISIBLE
        } else {
            holder.attachmentIV.visibility = View.GONE
        }
        holder.view.setOnClickListener {
            val intent = Intent(context, AddNoteActivity::class.java)
            intent.putExtra(Constant.BundleKeys.NOTE_KEY, note.getId())
            context.startActivity(intent)
        }
        holder.attachmentIV.setOnClickListener {
            if (note.isAttachment()) {
                val intent = Intent(context, NoteDetailActivity::class.java)
                intent.putExtra(Constant.BundleKeys.NOTE_KEY, note.getId())
                context.startActivity(intent)
            } else {
                Toast.makeText(context, "No Attachements to View", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun getItemCount(): Int {
        return notes.size
    }

    internal class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nameTV: TextView
        var dateTV: TextView
        var remarkTV: TextView
        var bookmarkIV: ImageView
        var attachmentIV: ImageView
        var view: LinearLayout

        init {
            remarkTV = itemView.findViewById<View>(R.id.tvRemark) as TextView
            dateTV = itemView.findViewById<View>(R.id.tvDate) as TextView
            nameTV = itemView.findViewById<View>(R.id.tvNote) as TextView
            bookmarkIV = itemView.findViewById<View>(R.id.bookmark) as ImageView
            attachmentIV = itemView.findViewById<View>(R.id.attachment) as ImageView
            view = itemView.findViewById(R.id.view)
        }
    }


}
