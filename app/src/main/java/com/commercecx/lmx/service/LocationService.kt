package com.commercecx.lmx.service

import android.Manifest
import android.app.Service
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Binder
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat


class LocationService : Service(), LocationListener {
    var isGPSEnabled = false
    var isNetworkEnabled = false
    var canGetLocation = false
    private val mBinder: IBinder = LocationBinder()

    interface ILocationService {
        fun locationUpdated()
    }

    private var mServiceCallback: ILocationService? = null
    var location: Location? = null
    var latitude = 0.0
    var longitude = 0.0
    private var locationManager: LocationManager? = null

    //endregion
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        return super.onStartCommand(intent, flags, startId)
    }

    fun fetchLocation(): Location? {
        try {

            //Initializing location manager
            locationManager = getSystemService(LOCATION_SERVICE) as LocationManager

            //Fetching GPS status
            isGPSEnabled = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)

            //Fetching network status
            isNetworkEnabled = locationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            if (!isGPSEnabled && !isNetworkEnabled) {
                enableGPSAlert()
                Toast.makeText(this, "Services not enabled", Toast.LENGTH_SHORT).show()
            } else {
                //region Fetching location using GPS
                if (isGPSEnabled) {
                    if (ContextCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        )
                        === PackageManager.PERMISSION_GRANTED
                    ) {
                        locationManager!!.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(), this
                        )
                        Log.i(TAG, "GPS enabled")
                    }
                    if (locationManager != null) {
                        location =
                            locationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                    }
                    latitude = if (location != null) location!!.latitude else 0.0
                    longitude = if (location != null) location!!.longitude else 0.0
                } else if (isNetworkEnabled) {
                    if (ContextCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        )
                        === PackageManager.PERMISSION_GRANTED
                    ) {
                        locationManager!!.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(), this
                        )
                        Log.i(TAG, "GPS enabled")
                    }
                    if (locationManager != null) {
                        location =
                            locationManager!!.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                    }
                    latitude = if (location != null) location!!.latitude else 0.0
                    longitude = if (location != null) location!!.longitude else 0.0
                }
                //endregion
            }
        } catch (e: Exception) {
            Log.e(TAG, "Error in fetching location ", e)
        }
        mServiceCallback!!.locationUpdated()
        return location
    }

    fun stopGPS() {
        if (locationManager != null) {
            locationManager!!.removeUpdates(this@LocationService)
        }
    }

    fun setCallbacks(callbacks: ILocationService?) {
        mServiceCallback = callbacks
    }

    override fun onBind(intent: Intent): IBinder? {
        return mBinder
    }

    override fun onLocationChanged(location: Location) {}
    override fun onStatusChanged(s: String, i: Int, bundle: Bundle) {}
    override fun onProviderEnabled(s: String) {}
    override fun onProviderDisabled(s: String) {}
    fun enableGPSAlert() {
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(this)

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings")

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?")

        // On pressing the Settings button.
        alertDialog.setPositiveButton("Settings",
            DialogInterface.OnClickListener { dialog, which ->
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            })

        // On pressing the cancel button
        alertDialog.setNegativeButton("Cancel",
            DialogInterface.OnClickListener { dialog, which -> dialog.cancel() })

        // Showing Alert Message
        alertDialog.show()
    }

    inner class LocationBinder : Binder() {
        val service: LocationService
            get() = this@LocationService
    }

    companion object {
        //region Variable Declaration
        private val TAG = LocationService::class.java.simpleName

        // minimum distance for updates in meters
        private const val MIN_DISTANCE_CHANGE_FOR_UPDATES: Long = 10

        // update interval in milliseconds
        private const val MIN_TIME_BW_UPDATES: Long = 60000
    }
}

