package com.commercecx.lmx.service

import android.app.job.JobParameters
import android.app.job.JobService
import android.os.AsyncTask
import android.util.Log
import com.commercecx.lmx.model.Note
import io.realm.Realm
import java.util.*


class NoteJobService : JobService() {
    private var noteList: List<Note?>? = null
    private var jobParameters: JobParameters? = null
    var realm: Realm? = null
    override fun onStartJob(jobParameters: JobParameters): Boolean {
        this.jobParameters = jobParameters
        realm = Realm.getDefaultInstance()
        noteList = realm!!.where(Note::class.java).equalTo("isServerCommitted", false).findAll()


        //  new FetchNotesAsync().execute();

//        NetworkUtil networkUtil = new NetworkUtil(getApplicationContext());
//        NetworkOperations operations = new NetworkOperations(getApplicationContext());
        for (note in noteList!!) {

//            if(networkUtil.isConnected()){
//                operations.sendNoteToServer(note,note.getId());
////                if(note.isAttachment()){
////                    operations.sendFileToServer(note.getAttachName(),note.getAttachPath());
////                }
//            }
        }
        jobFinished(jobParameters, false)
        return true
    }

    override fun onStopJob(jobParameters: JobParameters): Boolean {
        return false
    }

    internal inner class FetchNotesAsync :
        AsyncTask<Void?, Void?, String>() {
        override fun onPreExecute() {
            super.onPreExecute()
        }

        protected override fun doInBackground(vararg params: Void?): String? {
            var result = "FAILED"
            try {
                realm = Realm.getDefaultInstance()
                noteList = realm!!.where(Note::class.java).equalTo("isServerCommitted", false).findAll()

                result = "SUCCESS"
            } catch (e: Exception) {
                Log.e(TAG, "doInBackground: ", e)
                jobFinished(jobParameters, false)
            }
            return result
        }

        override fun onPostExecute(result: String) {
            when (result) {
                "SUCCESS" -> {
                    //                    NetworkUtil networkUtil = new NetworkUtil(getApplicationContext());
//                    NetworkOperations operations = new NetworkOperations(getApplicationContext());
                    for (note in noteList!!) {

//                        if(networkUtil.isConnected()){
//                            operations.sendNoteToServer(note,note.getId());
//                            if(note.isAttachment()){
//                                operations.sendFileToServer(note.getAttachName(),note.getAttachPath());
//                            }
//                        }
                    }
                    jobFinished(jobParameters, false)
                }
            }
        }
    }

    companion object {
        private val TAG = NoteJobService::class.java.simpleName
    }
}

