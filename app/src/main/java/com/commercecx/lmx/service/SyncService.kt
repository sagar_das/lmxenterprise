package com.commercecx.lmx.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.commercecx.lmx.adapter.SyncAdapter

class SyncService : Service() {

    private var syncAdapter: SyncAdapter? = null
    private val syncAdapterLock = Any()


    fun SyncService() {}

    override fun onCreate() {
        synchronized(syncAdapterLock) {
            if (syncAdapter == null) {
                syncAdapter = SyncAdapter(applicationContext, true)
            }
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return syncAdapter!!.getSyncAdapterBinder()
    }
}
