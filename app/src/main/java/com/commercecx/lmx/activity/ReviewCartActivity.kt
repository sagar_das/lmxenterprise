package com.commercecx.lmx.activity


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.Nullable
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.lmx.R
import com.commercecx.lmx.adapter.ReviewCartAdapter
import com.commercecx.lmx.base.BaseActivity
import com.commercecx.lmx.base.BaseSalesForceActivity
import com.commercecx.lmx.interactors.OrderInteractor
import com.commercecx.lmx.model.CartOld
import com.commercecx.lmx.model.OrderOld
import com.commercecx.lmx.model.ProductOld
import com.commercecx.lmx.model.RecentActivityDash
import com.commercecx.lmx.model.RouteOld
import com.commercecx.lmx.presenter.CheckoutPresenter
import com.commercecx.lmx.presenter.CheckoutViewTranslator
import com.commercecx.lmx.util.Logger
import com.commercecx.lmx.util.Utils
import com.commercecx.lmx.view.JDECustomTextView
import com.commercecx.lmx.view.JDEInkView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.salesforce.androidsdk.rest.RestClient
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_review_cart.*
import java.io.ByteArrayOutputStream
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class ReviewCartActivity : BaseSalesForceActivity<CheckoutPresenter>(), CheckoutViewTranslator, SwipeRefreshLayout.OnRefreshListener {

    companion object {
        private const val TAG = "ReviewCartActivity"
        const val ARG_HIDE_EXTRA_INFO = "arg_hide_extra_info"

        fun startActivity(context: Context, hideExtraInfo: Boolean = false) {
            val intent = Intent(context, ReviewCartActivity::class.java).putExtra(ARG_HIDE_EXTRA_INFO, hideExtraInfo)
            context.startActivity(intent)
        }
    }
    private val DEFAULT_PAGE_NUMBER = 1


    private var linearLayoutManager: LinearLayoutManager? = null
    private var mCurrentPage = DEFAULT_PAGE_NUMBER

    // region Constants
    private val isLoading = false
    private var dialog: BottomSheetDialog? = null
    private var sectionAdapter: SectionedRecyclerViewAdapter? = null
    private var cart_id = 0
    private var cart: CartOld? = null

    private var bundle: Bundle? = null
    private var ltax: Long = 0
    private var lsubtotal: Long = 0
    private var ltotal: Long = 0
    var realm: Realm? = null
    var client: RestClient? = null
    var orderInteractor: OrderInteractor? = null

    /**
     * onScrollListner for pagination of Videos and Photos
     */
    private val onScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (linearLayoutManager != null) {
                try {
                    if (!isLoading) {
                        val visibleItemCount: Int = linearLayoutManager!!.getChildCount()
                        val totalItemCount: Int = linearLayoutManager!!.getItemCount()
                        val firstVisibleItemPosition: Int =
                            linearLayoutManager!!.findLastVisibleItemPosition()
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                        }
                    }
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    protected override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review_cart)
        ButterKnife.bind(this)
        bundle = getIntent().getBundleExtra("ROUTE_BUNDLE")
        realm = Realm.getDefaultInstance()
        setTitle("Review Cart")
        val toolbar: Toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)
        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        //  linearLayoutManager = new WrapContentLinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        mCurrentPage = DEFAULT_PAGE_NUMBER
        homeRV!!.setLayoutManager(linearLayoutManager)
        // homeRV!!.setLayoutManager(linearLayoutManager);
        homeRV!!.setHasFixedSize(true)
        homeRV!!.setItemViewCacheSize(20)
        homeRV!!.setDrawingCacheEnabled(true)
        homeRV!!.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH)
        //calling onScrollListner for Pagination
        homeRV!!.addOnScrollListener(onScrollListener)
        swipeRefreshLayout!!.setOnRefreshListener(this)
        swipeRefreshLayout!!.setColorSchemeResources(R.color.header_theme)
        val extras: Bundle = intent.extras!!
        if (extras != null) {
            cart_id = extras.getInt("cart_id")
            cart = Realm.getDefaultInstance().where(CartOld::class.java).equalTo("id", cart_id)
                .findFirst()
            calculateTotalAmount()
        }
        startShimmerAnimation()
    }

    override fun onResume(client: RestClient?) {
        super.onResume(client)
        this.client = client
        Log.i(TAG, "Setting screen name: DashboardFragment")
        val handler = Handler()
        handler.postDelayed({ //Do something after 100ms
            setHomeFeedAdapter()
        }, 500)
    }



    private fun startShimmerAnimation() {
        shimmerEffectLayout!!.baseAlpha = 0.7f
        shimmerDefaultLL!!.visibility = View.VISIBLE
        homeRV!!.setVisibility(View.GONE)
        shimmerEffectLayout!!.startShimmerAnimation()
    }

    private fun stopShimmerAnimation() {
        shimmerEffectLayout!!.stopShimmerAnimation()
        homeRV!!.setVisibility(View.VISIBLE)
        shimmerDefaultLL!!.visibility = View.GONE
    }

    fun setHomeFeedAdapter() {
        if (mCurrentPage == DEFAULT_PAGE_NUMBER) if (true) {
            noDataTextTV!!.visibility = View.GONE
            homeRV!!.setVisibility(View.VISIBLE)
            homeRV!!.setLayoutManager(LinearLayoutManager(this))
            cart = Realm.getDefaultInstance().where(CartOld::class.java).equalTo("id", cart_id)
                .findFirst()
            sectionAdapter = SectionedRecyclerViewAdapter()
            if (cart!!.getOldProducts() != null && cart!!.getOldProducts()!!.size > 0) {
                sectionAdapter!!.addSection(
                    ReviewCartAdapter(
                        this,
                        ReviewCartAdapter.NEW_ORDER,
                        cart!!
                    )
                )
            }
            if (cart!!.getPreviousProducts() != null && cart!!.getPreviousProducts()!!.size > 0) {
                sectionAdapter!!.addSection(
                    ReviewCartAdapter(
                        this,
                        ReviewCartAdapter.OLD_ORDER,
                        cart!!
                    )
                )
            }
            homeRV!!.setAdapter(sectionAdapter)
        } else {
            noDataTextTV!!.visibility = View.VISIBLE
        }
        stopShimmerAnimation()
    }

    override fun onRefresh() {}
    @OnClick(R.id.SignOrderBtn)
    fun onClickSignaturesBtn() {
        val custom_view: View =
            LayoutInflater.from(this).inflate(R.layout.bottom_sheet_signture_dialog, null)
        dialog = BottomSheetDialog(this)
        dialog!!.setContentView(custom_view)
        val inkView: JDEInkView = dialog!!.findViewById(R.id.ink)!!
        dialog!!.findViewById<JDECustomTextView>(R.id.cancelBtn)!!
            .setOnClickListener(View.OnClickListener { dialog!!.dismiss() })
        dialog!!.findViewById<JDECustomTextView>(R.id.submitBtn)!!.setOnClickListener(View.OnClickListener {
            realm!!.beginTransaction() //open the database
            //val route: RouteOld? = bundle!!.getSerializable("ROUTE_DATA") as RouteOld?
            val order = OrderOld()
            order.setId(cart!!.getId())
            //order.setDate(route!!.getDate())
            order.setName(cart!!.getName())
            order.setRoute_id("0")
            order.setTax(ltax)
            order.setSub_total(lsubtotal)
            order.setTotal(ltotal)
            val drawing: Bitmap = inkView.getBitmap()
            val byteArrayOutputStream = ByteArrayOutputStream()
            drawing.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
            val byteArray = byteArrayOutputStream.toByteArray()
            val encoded = Base64.encodeToString(byteArray, Base64.DEFAULT)
            order.setImage(encoded)
            if (cart!!.getOldProducts() == null) {
                val products: RealmList<ProductOld> = RealmList<ProductOld>()
                cart!!.setOldProducts(products)
            }
            if (cart!!.getPreviousProducts() != null && cart!!.getPreviousProducts()!!.size > 0) {
                cart!!.getOldProducts()!!.addAll(cart!!.getPreviousProducts()!!)
            }
            order.setProducts(cart!!.getOldProducts())

            //Sending Order at ato Salesforce
            //presenter().placeOrder("", "")
            orderInteractor = OrderInteractor(client!!)
            orderInteractor!!.syncCall(Utils.fetchUnixEpoch())

            realm!!.insertOrUpdate(order)
            val d = Date()
            val dateFormat: DateFormat = SimpleDateFormat("MM-dd-yyyy")
            val strDate = dateFormat.format(d)
            val recentActivity = RecentActivityDash()
            val raResults: RealmResults<RecentActivityDash> =
                realm!!.where(RecentActivityDash::class.java).findAll()
            val raId = if (raResults.size > 0) raResults[0]!!.getId() + 1 else 1
            recentActivity.setId(raId)
            recentActivity.setActivity_name("Created an Order")
            recentActivity.setActivity_date(strDate)
            realm!!.insertOrUpdate(recentActivity)
            realm!!.commitTransaction()
            dialog!!.dismiss()
            val intent = Intent(getApplicationContext(), OccurrenceActivity::class.java)
            startActivity(intent)
        })
        dialog!!.findViewById<JDECustomTextView>(R.id.clearBtn)!!
            .setOnClickListener(View.OnClickListener { inkView.clear() })
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.setCancelable(false)
        dialog!!.show()
    }

    fun calculateTotalAmount() {
        var sub_total = 0.0
        var tax = 0.0
        cart = Realm.getDefaultInstance().where(CartOld::class.java).equalTo("id", cart_id).findFirst()
        if (cart!!.getOldProducts() != null) {
            for (product in cart!!.getOldProducts()!!) {
                val price: Double =
                    java.lang.Double.valueOf(product.getItem_price()!!) * java.lang.Double.valueOf(
                        product.getItem_qty()!!
                    )
                val product_tax: Double =
                    java.lang.Double.valueOf(product.getItem_tax()!!) * java.lang.Double.valueOf(
                        product.getItem_qty()!!
                    )
                sub_total = sub_total + price
                tax = tax + product_tax
            }
        }
        if (cart!!.getPreviousProducts() != null) {
            for (product in cart!!.getPreviousProducts()!!) {
                val price: Double =
                    java.lang.Double.valueOf(product.getItem_price()!!) * java.lang.Double.valueOf(
                        product.getItem_qty()!!
                    )
                val product_tax: Double =
                    java.lang.Double.valueOf(product.getItem_tax()!!) * java.lang.Double.valueOf(
                        product.getItem_qty()!!
                    )
                sub_total = sub_total + price
                tax = tax + product_tax
            }
        }
        sub_total_amount!!.text = sub_total.toString()
        tax_amount!!.text = tax.toString()
        val total = sub_total + tax
        total_amount!!.text = total.toString()
        ltax = tax.toLong()
        lsubtotal = sub_total.toLong()
        ltotal = total.toLong()
    }

    private fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun createPresenter(): CheckoutPresenter = CheckoutPresenter(this)

    override fun hideExtraInfo() {
        Logger.info("Implemented")
    }

    override fun showVanOrWarehouseDialog() {
        Logger.info("Implemented")
    }

    override fun disablePoNumber() {
        Logger.info("Implemented")
    }

    override fun hidePoNumber() {
        Logger.info("Implemented")
    }

    override fun setPoNumberText(poNumber: String) {
        Logger.info("Implemented")
    }

    override fun showPoNumberEmptyError() {
        Logger.info("Implemented")
    }

    override fun openSignatureScreen() {
        Logger.info("Implemented")
    }

    override fun updateCheckOutOperatorRow(workOrderType: String) {
        Logger.info("Implemented")
    }

    override fun showSignatureImage(signatureBitmap: Bitmap?) {
        Logger.info("Implemented")
    }

    override fun openRouteDetailsScreen() {
        Logger.info("Implemented")
    }

    override fun showLoader() {
        Logger.info("Implemented")
    }

    override fun hideLoader() {
        Logger.info("Implemented")
    }

    override fun showDatePicker() {
        Logger.info("Implemented")
    }

    override fun setDate(year: Int, month: Int, day: Int, date: Long) {
        Logger.info("Implemented")
    }


}


