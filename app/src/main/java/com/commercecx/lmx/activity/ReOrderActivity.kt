package com.commercecx.lmx.activity

import android.os.Bundle
import android.os.Handler

import androidx.annotation.Nullable
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.lmx.R
import com.commercecx.lmx.adapter.ReOrderAdapter
import com.commercecx.lmx.base.BaseActivity
import com.commercecx.lmx.model.CartOld
import com.commercecx.lmx.model.OrderOld
import com.commercecx.lmx.util.Logger
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_re_order.*
import kotlinx.android.synthetic.main.view_common_header_back.*


class ReOrderActivity : BaseActivity() {



    protected override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_re_order)
        ButterKnife.bind(this)
        headerTitleName!!.setText("Reorder")
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction { realm -> realm.delete(CartOld::class.java) }
        val handler = Handler()
        handler.postDelayed({
            val order_id: Int = getIntent().getIntExtra("order_id", 0)
            val order: OrderOld =
                Realm.getDefaultInstance().where(OrderOld::class.java).equalTo("id", order_id)
                    .findFirst()!!
            val manager = LinearLayoutManager(this@ReOrderActivity)
            recyclerView!!.layoutManager = manager
            recyclerView!!.adapter = ReOrderAdapter(this@ReOrderActivity, order)
        }, 500)
    }

    @OnClick(R.id.back_btn)
    fun onClickBackBtn() {
        Logger.debug("OnBackPressed...")
        finish()
    }

    fun updateCount(cart: CartOld) {
        var quantity = 0
        if (cart.getOldProducts() != null) {
            for (product in cart.getOldProducts()!!) {
                quantity = quantity + Integer.valueOf(product.getItem_qty()!!)
            }
        }
        for (product in cart.getPreviousProducts()!!) {
            quantity = quantity + Integer.valueOf(product.getItem_qty()!!)
        }
        cartCountTV!!.text = "$quantity items added"
    }
}
