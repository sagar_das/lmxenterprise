package com.commercecx.lmx.activity

import android.app.AlertDialog
import android.app.SearchManager
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Optional

import com.androidadvance.topsnackbar.TSnackbar
import com.commercecx.lmx.R
import com.commercecx.lmx.adapter.CatalogAdapter
import com.commercecx.lmx.adapter.CatalogGridLayoutManager
import com.commercecx.lmx.base.BaseSFAct
import com.commercecx.lmx.fragment.AccountFragment
import com.commercecx.lmx.fragment.CatalogFragment
import com.commercecx.lmx.model.CartOld
import com.commercecx.lmx.model.Product
import com.commercecx.lmx.presenter.ProductsPresenter
import com.commercecx.lmx.presenter.ProductsViewTranslator
import com.commercecx.lmx.util.Logger
import com.commercecx.lmx.util.Utils
import com.salesforce.androidsdk.rest.RestClient
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_catalog_new.*
import kotlinx.android.synthetic.main.fab_catalog_submenu.*

class CatalogActivity : BaseSFAct<ProductsPresenter>(), SwipeRefreshLayout.OnRefreshListener,
    ProductsViewTranslator, CatalogAdapter.IProductClickListener {
    private val DEFAULT_PAGE_NUMBER = 1
    private val quantity = 0



    //   @BindView(R.id.cartCountTV)
    //  TextView cartCountTV;

    private var linearLayoutManager: LinearLayoutManager? = null
    private var gridLayoutManager: CatalogGridLayoutManager? = null
    private var isFabCatalogClicked = false
    private var mCurrentPage = DEFAULT_PAGE_NUMBER

    // region Constants
    private val isLoading = false
    private var catalogAdapter: CatalogAdapter? = null
    private var name: String? = null
    private var cart_id = 0
    private var bundle: Bundle? = null
    private lateinit var productsPresenter: ProductsPresenter

    /**
     * onScrollListner for pagination of Videos and Photos
     */
    private val onScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (gridLayoutManager != null) {
                try {
                    if (!isLoading) {
                        val visibleItemCount: Int = gridLayoutManager!!.childCount
                        val totalItemCount: Int = gridLayoutManager!!.itemCount
                        val firstVisibleItemPosition: Int =
                            gridLayoutManager!!.findLastVisibleItemPosition()
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                        }
                    }
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    protected override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_catalog_new)
        ButterKnife.bind(this)
        setTitle("Catalog")
        val toolbar: Toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)
        bundle = getIntent().getBundleExtra("ROUTE_BUNDLE")
        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        gridLayoutManager = CatalogGridLayoutManager(this, Utils.CATALOG_GRID_COLUMN_SIZE)

        //  linearLayoutManager = new WrapContentLinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        mCurrentPage = DEFAULT_PAGE_NUMBER
        homeRV!!.setLayoutManager(gridLayoutManager)
        // homeRV!!.setLayoutManager(linearLayoutManager);
        homeRV!!.setHasFixedSize(true)
        homeRV!!.setItemViewCacheSize(20)
        homeRV!!.setDrawingCacheEnabled(true)
        homeRV!!.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH)
        //calling onScrollListner for Pagination
        homeRV!!.addOnScrollListener(onScrollListener)
        swipeRefreshLayout!!.setOnRefreshListener(this)
        swipeRefreshLayout!!.setColorSchemeResources(R.color.header_theme)
        name = getIntent().getStringExtra("name")

        //Delete Previous Cart History
        cart_id = intent.extras!!.getInt("cart_id", -1)
        if (cart_id == -1) {
            val realm = Realm.getDefaultInstance()
            realm.executeTransaction { realm -> realm.delete(CartOld::class.java) }
        }
        presenter().onViewCreated(null, null, null)
        startShimmerAnimation()
    }

    @Optional
    @OnClick(R.id.back_btn)
    fun onClickBackBtn() {
        Logger.debug("OnBackPressed...")
        finish()
    }

    //    @OnClick({R.id.reviewCartBtn})
    //    public void onClickReviewCartBtn() {
    //        Logger.debug("onClickReviewCartBtn...");
    //        Intent intent = new Intent(this,ReviewCartActivity.class);
    //        startActivity(intent);
    //    }
    override fun onResume(client: RestClient?) {
        super.onResume()
        Log.i(TAG, "Setting screen name: DashboardFragment")
        if (catalogAdapter != null) {
            catalogAdapter!!.notifyChanges()
            linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            gridLayoutManager = CatalogGridLayoutManager(this, 500)
            homeRV!!.setLayoutManager(gridLayoutManager)
        }
        isFabCatalogClicked = false
    }


    private fun startShimmerAnimation() {
        shimmerEffectLayout!!.baseAlpha = 0.7f
        shimmerDefaultLL!!.visibility = View.VISIBLE
        homeRV!!.setVisibility(View.GONE)
        shimmerEffectLayout!!.startShimmerAnimation()
//        val handler = Handler()
//        handler.postDelayed({ //Do something after 100ms
//            setHomeFeedAdapter()
//        }, 500)
    }

    private fun stopShimmerAnimation() {
        shimmerEffectLayout!!.stopShimmerAnimation()
        homeRV!!.setVisibility(View.VISIBLE)
        shimmerDefaultLL!!.visibility = View.GONE
    }

    fun scrollToBeginning() {
        gridLayoutManager!!.scrollToPositionWithOffset(0, 0)
    }

    private fun setHomeFeedAdapter(data: MutableList<Product>) {
        if (mCurrentPage == DEFAULT_PAGE_NUMBER) if (true) {
            noDataTextTV!!.visibility = View.GONE
            catalogAdapter = CatalogAdapter(this, name, cart_id, bundle, data,this)
            homeRV!!.setAdapter(catalogAdapter)
        } else {
            noDataTextTV!!.visibility = View.VISIBLE
        }
        stopShimmerAnimation()
    }

    override fun onRefresh() {}
    fun updateCount(cart: CartOld?) {
        var quantity = 0
        if (cart != null) {
            for (product in cart.getOldProducts()!!) {
                quantity = quantity + Integer.valueOf(product.getItem_qty()!!)
            }
            if (cart.getPreviousProducts() != null) {
                for (product in cart.getPreviousProducts()!!) {
                    quantity = quantity + Integer.valueOf(product.getItem_qty()!!)
                }
            }
        }

        //   cartCountTV.setText(quantity + " items added");
        if (quantity > 0) {
            tvCartCount!!.visibility = View.VISIBLE
            tvCartCount!!.text = if (quantity < 100) quantity.toString() else "99+"
        } else {
            tvCartCount!!.visibility = View.GONE
        }
        // reviewCartBtn.setText("Review Cart (" + quantity + ")");
    }

    fun updateMessage(quantity: String, message: String) {
        val snackbar = TSnackbar.make(
            findViewById(R.id.llUpdatedMessage),
            "$quantity $message", TSnackbar.LENGTH_LONG
        )
        snackbar.setActionTextColor(Color.WHITE)
        val snackbarView = snackbar.view

        // snackbarView.setBackgroundColor(Color.parseColor("#CC00CC"));
        val textView = snackbarView.findViewById<View>(R.id.snackbar_text) as TextView
        textView.setTextColor(Color.WHITE)
        if (message.contains("add")) {
            snackbar.setAction("OK") { snackbar.dismiss() }
        } else {
            snackbar.setAction("UNDO") { snackbar.dismiss() }
        }
        snackbar.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    @OnClick(R.id.ibFilter)
    fun onImportClick() {
        //startActivity(Intent(getContext(), ImportActivity::class.java))
        showFilterDialog()
    }

    @OnClick(R.id.ibSearch)
    fun onSearchClick(){
        onSearchRequested()
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            intent.getStringExtra(SearchManager.QUERY)?.also { query ->

              receiveSearchQuery(query)


            }
        }
    }

    override fun onSearchRequested(): Boolean {
        return super.onSearchRequested()

    }

    fun receiveSearchQuery(query: String?){
        Toast.makeText(this,query, Toast.LENGTH_SHORT).show()
        productsPresenter.searchProducts(query)


    }

    private fun showFilterDialog() {
        //Show confirmation dialog
        val dialogBuilder = AlertDialog.Builder(this)
        val tagsDialog: View = layoutInflater.inflate(R.layout.product_options, null)
        dialogBuilder.setView(tagsDialog)
        val dialog = dialogBuilder.create()
        val ok = tagsDialog.findViewById<View>(R.id.btnOK) as Button
        ok.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    override fun onLogoutComplete() {
        TODO("Not yet implemented")
    }

    override fun onUserSwitched() {
        TODO("Not yet implemented")
    }

    companion object {
        private const val TAG = "CatalogActivity"
    }

    override fun createPresenter(): ProductsPresenter =
        ProductsPresenter(this)

    override fun initViews() {
        TODO("Not yet implemented")
    }

    override fun showProductDetailsContent(product: Product) {
        TODO("Not yet implemented")
    }

    override fun displayProducts(data: List<Product>) {
        setHomeFeedAdapter(data.toMutableList())
    }

    override fun showLoader() {
        Logger.info("Show loader")
    }

    override fun hideLoader() {
        Logger.info("Hide loader")
    }

    override fun getInstance(productsPresenter: ProductsPresenter) {
        this.productsPresenter = productsPresenter
    }

    override fun onProductClick(product: Product?) {
        Logger.info("Clicked")
    }
}
