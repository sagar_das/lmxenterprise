package com.commercecx.lmx.activity

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.lmx.R
import com.commercecx.lmx.model.ProductOld
import com.commercecx.lmx.util.CSVUtil
import com.commercecx.lmx.util.NoteAttachUtils
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_import.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class ImportActivity : AppCompatActivity() {

    private val TAG: String = this::class.java.simpleName
    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_import)
        ButterKnife.bind(this)
    }

    @OnClick(R.id.btnImport)
    fun onImportClick() {
        //Toast.makeText(this, "Import is clicked", Toast.LENGTH_SHORT).show();
        val mimeTypes = arrayOf("*/*")
        val docIntent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        docIntent.addCategory(Intent.CATEGORY_OPENABLE)
        docIntent.type = mimeTypes[0]
        startActivityForResult(docIntent, RESULT_LOAD_DOC)
    }

    private fun get_context(): Context {
        return this
    }

    internal inner class CSVRecordsTask :
        AsyncTask<Uri?, Void?, List<*>>() {
        override fun onPreExecute() {
            super.onPreExecute()
            pbImport!!.visibility = View.VISIBLE
            tvFilepath!!.visibility = View.GONE
            btnImport!!.visibility = View.GONE
        }

        protected override fun doInBackground(vararg uris: Uri?): List<*>? {
            return CSVUtil.readFromCSV(uris[0], get_context())
        }

        override fun onPostExecute(list: List<*>) {
            super.onPostExecute(list)
            pbImport!!.visibility = View.GONE
            tvFilepath!!.visibility = View.VISIBLE
            btnImport!!.visibility = View.VISIBLE
            val dialogBuilder = AlertDialog.Builder(get_context())
            val completeDialog: View =
                LayoutInflater.from(get_context()).inflate(R.layout.import_products_dialog, null)
            dialogBuilder.setView(completeDialog)
            val dialog = dialogBuilder.create()
            val title = completeDialog.findViewById<TextView>(R.id.title)
            val btnYes = completeDialog.findViewById<Button>(R.id.btnYes)
            val btnNo = completeDialog.findViewById<Button>(R.id.btnNo)
            title.text = "Proceed to import " + Integer.toString(list.size) + " items?"
            btnYes.setOnClickListener {
                try {
                    pbImport!!.visibility = View.VISIBLE
                    tvFilepath!!.visibility = View.GONE
                    btnImport!!.visibility = View.GONE
                    ProductRecordsTask().execute(list)
                    dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                    Log.e(TAG, "Unable to select image from gallery", e)
                    dialog.dismiss()
                }
            }
            btnNo.setOnClickListener { dialog.dismiss() }
            dialog.show()
        }
    }

    internal inner class ProductRecordsTask :
        AsyncTask<List<*>?, Void?, Void?>() {
        protected override fun doInBackground(vararg lists: List<*>?): Void? {
            val realm = Realm.getDefaultInstance()
            val results: RealmResults<ProductOld> = realm.where(ProductOld::class.java).findAll()
            var id = if (results.size > 0) results[0]!!.getId()!!.toInt() else 1
            val d = Date()
            val dateFormat: DateFormat = SimpleDateFormat("MM-dd-yyyy")
            val strDate = dateFormat.format(d)
            val productList: List<Array<String?>> = lists[0] as List<Array<String?>>
            for (item in productList) {
                val product = ProductOld()
                product.setId(Integer.toString(++id))
                product.setIndex(id)
                product.setName(item[0])
                product.setItem_price(item[1])
                product.setItem_tax(item[2])
                product.setDate(strDate)
                realm.executeTransactionAsync { realm -> realm.insertOrUpdate(product) }
            }
            return null
        }

        override fun onPostExecute(aVoid: Void?) {
            super.onPostExecute(aVoid)
            finish()
        }
    }

    protected override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RESULT_LOAD_DOC && resultCode == RESULT_OK) {
            if (data != null && data.data != null) {
                val uriData = data.data
                var fileBytes: ByteArray
                var fileType: String
                try {
                    contentResolver.openInputStream(uriData!!).use { ipStream ->
                        fileBytes = NoteAttachUtils.getBytes(ipStream!!)
                        fileType = getContentResolver().getType(uriData)!!
                        if (!fileType.contains("comma-separated-values")) {
                            Toast.makeText(
                                this,
                                "Please select a valid .CSV file",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        tvFilepath!!.text = uriData!!.path
                        CSVRecordsTask().execute(uriData)
                    }
                } catch (e: Exception) {
                    Log.e(TAG, "Unable to get input stream", e)
                }
            }
        }
    }

    companion object {
        private const val RESULT_LOAD_DOC = 301
    }
}
