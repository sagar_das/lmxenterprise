package com.commercecx.lmx.activity

import android.Manifest
import android.app.SearchManager
import android.content.Context

import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.os.Bundle

import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.core.view.MenuItemCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.preference.PreferenceManager
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Optional
import com.commercecx.lmx.R
import com.commercecx.lmx.adapter.AccountsAdapter
import com.commercecx.lmx.adapter.CatalogAdapter
import com.commercecx.lmx.base.BaseSalesForceActivity
import com.commercecx.lmx.fragment.*
import com.commercecx.lmx.model.Account
import com.commercecx.lmx.model.Product
import com.commercecx.lmx.model.RouteOld
import com.commercecx.lmx.presenter.HomePresenter
import com.commercecx.lmx.presenter.HomeViewTranslator
import com.commercecx.lmx.util.Logger
import com.commercecx.lmx.util.NotePhoto
import com.google.android.material.navigation.NavigationView
import io.realm.Realm
import io.realm.RealmResults
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class MainNavActivity : BaseSalesForceActivity<HomePresenter>(), HomeViewTranslator, NavigationView.OnNavigationItemSelectedListener,
    RoutesFragment.IMapClickListener, RoutesFragment.INotificationListener,
    DashStatsFragment.INotificationUpdateListener, SettingsFragment.ISettingsPrefListener,
    ProfilePicFragment.IProfilePicListener, CatalogAdapter.IProductClickListener,
    AccountsAdapter.IAccountClickListener, CatalogFragment.OnSearchSelectedListener, AccountFragment.OnSearchSelectedListener {

    private val sharedPrefs: SharedPreferences by lazy { PreferenceManager.getDefaultSharedPreferences(this) }

    protected var fragment: Fragment? = null
    private var ft: FragmentTransaction? = null
    private var routeList: List<RouteOld>? = null
    private var realm: Realm? = null
    var preferences: SharedPreferences? = null
    private var tvUsername: TextView? = null
    private var tvEmailid: TextView? = null
    private var ivProfilePic: ImageView? = null


    var nav_dashboard: TextView? = null




    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_nav)

        if (!checkLocationPermission()) {
            requestLocationPermission()
        }
        ButterKnife.bind(this)


        TAG = getString(R.string.tag)
        val toolbar: Toolbar? = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
//        setSupportActionBar(toolbar)
        realm = Realm.getDefaultInstance()
        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val drawer: DrawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()
        val navigationView: NavigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)
        val header: View = navigationView.getHeaderView(0)
        nav_dashboard =
            MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_dashboard)) as TextView

//        if (Intent.ACTION_SEARCH == intent.action) {
//            intent.getStringExtra(SearchManager.QUERY)?.also { query ->
//                Toast.makeText(this,"Search is not accessible at this time", Toast.LENGTH_SHORT)
//
//            }
//        }
         if (intent.action != null) {
            if (intent.action.equals(ACTION_ROUTES)) {
                onRoutesClickEvent()
            } else {
                onNotesClickEvent()
            }
        } else {
            onDashboardClickEvent()
        }
        tvUsername = header.findViewById(R.id.tvUsername)
        tvEmailid = header.findViewById(R.id.tvEmailid)
        ivProfilePic = header.findViewById(R.id.ivProfilePic)
        tvUsername!!.text = preferences!!.getString(
            "prefFirstName",
            "Guest"
        ) + " " + preferences!!.getString("prefLastName", "")
        tvEmailid!!.setText(preferences!!.getString("prefEmailId", "dummy@xyz.com"))
        val profilePicPath = preferences!!.getString("prefProfilePic", "no_pic")
        if (profilePicPath.equals("no_pic", ignoreCase = true)) {
            val resourceId: Int = getResources().getIdentifier(
                "ccx_logo", "drawable", getPackageName()
            )
            ivProfilePic!!.setImageResource(resourceId)
        } else {
            val photo = NotePhoto(profilePicPath, this@MainNavActivity)
            ivProfilePic!!.setImageDrawable(photo.bitmapDrawable)
        }
    }

    protected override fun onResume() {
        super.onResume()
        updateNotificationCounter()

    }

    override fun onAttachFragment(fragment: Fragment) {
        if (fragment is CatalogFragment) {
            fragment.setOnSearchSelectedListener(this)
        }
        else if(fragment is AccountFragment){
            fragment.setOnSearchSelectedListener(this)
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            intent.getStringExtra(SearchManager.QUERY)?.also { query ->

                var currentFragment = supportFragmentManager.fragments.last()
                if (currentFragment is CatalogFragment){
                    var catalogFragment : CatalogFragment = currentFragment as CatalogFragment
                    catalogFragment.receiveSearchQuery(query)
                }
                else if (currentFragment is AccountFragment){
                    var accountFragment : AccountFragment = currentFragment as AccountFragment
                    accountFragment.receiveSearchQuery(query)
                }
                else{
                    Toast.makeText(this,"Search is not accessible at this time", Toast.LENGTH_SHORT)
                }


            }
        }
    }

    override fun onSearchRequested(): Boolean {
        return super.onSearchRequested()

    }

    private fun updateNotificationCounter() {
        nav_dashboard!!.gravity = Gravity.CENTER_VERTICAL
        nav_dashboard!!.setTypeface(null, Typeface.BOLD)
        nav_dashboard!!.setTextColor(getResources().getColor(R.color.orange102))
        val preferences: SharedPreferences = getSharedPreferences(PREFS_NAME, 0)
        val count = preferences!!.getInt("count", 0)
        if (count == 0) {
            nav_dashboard!!.text = ""
        } else {
            nav_dashboard!!.text = if (count > 99) "99+" else count.toString()
            Log.i(TAG, "updateNotificationCounter: " + nav_dashboard!!.text.toString())
        }
    }

    override fun onBackPressed() {
        val drawer: DrawerLayout = findViewById(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
            return
        }
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack();
            return
        }


            super.onBackPressed()

    }

    @Optional
    @OnClick(R.id.notesClickRV)
    fun onNotesClickEvent() {
//        truckClickRV.setBackgroundColor(Color.TRANSPARENT);
//        routesClickRV.setBackgroundColor(Color.TRANSPARENT);
//        notesClickRV.setBackgroundColor(getResources().getColor(R.color.color_selected_footer));
        setTitle(getString(R.string.notes))
        fragment = NotesFragment()
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.addToBackStack(null)
        ft!!.commit()
    }

    @Optional
    @OnClick(R.id.routesClickRV)
    fun onRoutesClickEvent() {
//        truckClickRV.setBackgroundColor(Color.TRANSPARENT);
        //      routesClickRV.setBackgroundColor(getResources().getColor(R.color.color_selected_footer));
        //     notesClickRV.setBackgroundColor(Color.TRANSPARENT);
        val date = Date()
        val dateFormat: DateFormat = SimpleDateFormat("dd-MMM-yyyy")
        val strDate = dateFormat.format(date)
        //setTitle(getString(R.string.routes).toString() + ": " + strDate)
        title = "Orders"
        //fragment = RoutesFragment()
        fragment = OrderFragment()
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.addToBackStack(null)
        ft!!.commit()
    }

    fun onSettingsClickEvent() {
        val fragment: Fragment = supportFragmentManager.findFragmentByTag(TAG)!!
        if (fragment != null) supportFragmentManager.beginTransaction().remove(fragment).commit()
        setTitle("Settings")
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame_layout_fl, SettingsFragment(), TAG)
            .commit()
    }

    fun onChatbotClickEvent() {
        startActivity(Intent(this, ChatBotActivity::class.java))
    }

    fun onCatalogClickEvent() {


        setTitle("Product Catalog")
        if (supportFragmentManager.findFragmentByTag(TAG) != null) supportFragmentManager.beginTransaction()
            .remove(
                supportFragmentManager.findFragmentByTag(
                    TAG
                )!!
            ).commit()
        fragment = CatalogFragment()
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.addToBackStack(null)
        ft!!.commit()
    }

    fun onAccountClickEvent() {


        //title = "Accounts"
        if (supportFragmentManager.findFragmentByTag(TAG) != null) supportFragmentManager.beginTransaction()
            .remove(
                supportFragmentManager.findFragmentByTag(
                    TAG
                )!!
            ).commit()
        fragment = AccountFragment()
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.addToBackStack(null)
        ft!!.commit()
    }

    fun onProductItemClickEvent(product: Product){
        setTitle("Product Detail")
        if (supportFragmentManager.findFragmentByTag(TAG) != null) supportFragmentManager.beginTransaction()
            .remove(
                supportFragmentManager.findFragmentByTag(
                    TAG
                )!!
            ).commit()

        val bundle = Bundle()
        bundle.putSerializable("PRODUCT", product)
        fragment = ProductDetailFragment()
        fragment!!.arguments = bundle
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.addToBackStack(null)
        ft!!.commit()
    }

    fun onAccountItemClickEvent(account: Account){

        setTitle("Account Detail")
        if (supportFragmentManager.findFragmentByTag(TAG) != null) supportFragmentManager.beginTransaction()
            .remove(
                supportFragmentManager.findFragmentByTag(
                    TAG
                )!!
            ).commit()

        val bundle = Bundle()
        bundle.putSerializable("ACCOUNT", account)
        fragment = AccountDetailFragment()
        fragment!!.arguments = bundle
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.addToBackStack(null)
        ft!!.commit()
    }

    fun onDashboardClickEvent() {
//        truckClickRV.setBackgroundColor(Color.TRANSPARENT);
        //      routesClickRV.setBackgroundColor(getResources().getColor(R.color.color_selected_footer));
        //     notesClickRV.setBackgroundColor(Color.TRANSPARENT);
        setTitle(getString(R.string.dashboard))
        fragment = DashStatsFragment()
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.addToBackStack(null)
        ft!!.commit()
    }

    fun onProfilePicClickEvent() {
        setTitle("Profile photo ")
        if (supportFragmentManager.findFragmentByTag(TAG) != null) supportFragmentManager.beginTransaction()
            .remove(
                supportFragmentManager.findFragmentByTag(
                    TAG
                )!!
            ).commit()
        fragment = ProfilePicFragment()
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.commit()
    }

    fun onCompanyLogoClickEvent() {
        setTitle("Company Logo ")
        if (supportFragmentManager.findFragmentByTag(TAG) != null) supportFragmentManager.beginTransaction()
            .remove(
                supportFragmentManager.findFragmentByTag(
                    TAG
                )!!
            ).commit()
        fragment = CompanyLogoFragment()
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.commit()
    }

    fun onContactUsClickEVent() {
        setTitle("Contact Us")
        if (supportFragmentManager.findFragmentByTag(TAG) != null) supportFragmentManager.beginTransaction()
            .remove(
                supportFragmentManager.findFragmentByTag(
                    TAG
                )!!
            ).commit()
        fragment = ContactUsFragment()
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.commit()
    }

    @Optional
    @OnClick(R.id.notifyContainerRL)
    fun onNotifyClickEvent() {
//        Builder(this)
//            .anchorView(notifyIV)
//            .contentView(R.layout.notify_pop_up_view)
//            .gravity(Gravity.BOTTOM)
//            .dismissOnOutsideTouch(true)
//            .dismissOnInsideTouch(true)
//            .arrowColor(Color.WHITE)
//            .modal(true)
//            .animated(false)
//            .maxWidth(R.dimen.max_width)
//            .build()
//            .show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            LOCATION_PERM ->                 // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                  //  initiateMapFragment()
                } else {
                    Toast.makeText(this, "Location permission was denied", Toast.LENGTH_SHORT)
                        .show()
                }
            else -> {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
                return
            }
        }
    }

    fun startScan() {
        /**
         * Build a new MaterialBarcodeScanner
         */
//        final BarcodeScanner materialBarcodeScanner = new BarcodeScannerBuilder()
//                .withActivity(MainNavActivity.this)
//                .withEnableAutoFocus(true)
//                .withBleepEnabled(true)
//                .withBackfacingCamera()
//                .withCenterTracker()
//                .withText("Scanning...")
//                .withResultListener(new BarcodeScanner.OnResultListener() {
//                    @Override
//                    public void onResult(Barcode barcode, Bitmap bitmap) {
////                        barcodeResult = barcode;
////                        result.setText(barcode.rawValue);
//                        Toast.makeText(MainNavActivity.this, barcode.rawValue,Toast.LENGTH_LONG).show();
////                        Toast.makeText(MainNavActivity.this, bitmap.getByteCount(),Toast.LENGTH_LONG).show();
//                        Bundle bundle = new Bundle();
//                        bundle.putString(Constant.BundleKeys.BARCODE_KEY, barcode.rawValue);
//                        launchActivity(BarcodeActivity.class, bundle);
//                    }
//                })
//                .build();
//        materialBarcodeScanner.startScan();
    }

    fun onListViewClicked() {
        fragment = Fragment()
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.commit()
    }

    fun onFilterViewClicked() {
        fragment = Fragment()
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.commit()
    }

    override fun onMapClick() {
        if (!checkLocationPermission()) {
            requestLocationPermission()
        } else {
            //Displaying map fragment
            initiateMapFragment()
        }
    }

    //region Permissions
    private fun checkLocationPermission(): Boolean {
        return if ((ContextCompat.checkSelfPermission(
                this@MainNavActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
                    !== PackageManager.PERMISSION_GRANTED) || (ContextCompat.checkSelfPermission(
                this@MainNavActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
                    !== PackageManager.PERMISSION_GRANTED) || (ContextCompat.checkSelfPermission(
                this@MainNavActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
                    !== PackageManager.PERMISSION_GRANTED)
        ) {
            // Permission is not granted
            false
        } else {
            true
        }
    }

    private fun requestLocationPermission() {
        if (!ActivityCompat.shouldShowRequestPermissionRationale(
                this@MainNavActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        ) {
            ActivityCompat.requestPermissions(
                this@MainNavActivity, arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                LOCATION_PERM
            )
        }
    }

    //endregion
    private fun initiateMapFragment() {
        val date = Date()
        val dateFormat: DateFormat = SimpleDateFormat("MM-dd-yyyy")
        val strDate = dateFormat.format(date)
        routeList = ArrayList<RouteOld>()
        val route_results: RealmResults<RouteOld> =
            realm!!.where(RouteOld::class.java).equalTo("routeDate", strDate).findAll()
        routeList = realm!!.copyFromRealm(route_results)
        val address_list = arrayOfNulls<String>(routeList!!.size)
        val b = Bundle()
        b.putStringArray("ADDRESS", address_list)
        for (i in routeList!!.indices) {
            address_list[i] = routeList!!.get(i).getAddress()
        }
        fragment = GoogleMapFragment()
        fragment!!.arguments = b
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.commit()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_dashboard ->                // startActivity(new Intent(this, DashboardActivity.class));
                onDashboardClickEvent()
            R.id.nav_routes -> onRoutesClickEvent()
            R.id.nav_notes -> onNotesClickEvent()
            R.id.nav_settings -> onSettingsClickEvent()
            R.id.nav_product -> onCatalogClickEvent()
            R.id.nav_account -> onAccountClickEvent()
            R.id.nav_chatbot -> onChatbotClickEvent()
        }
        val drawer: DrawerLayout = findViewById(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onNotificationCreated() {
        val preferences: SharedPreferences = getSharedPreferences(PREFS_NAME, 0)
        val count = preferences!!.getInt("count", 0)
        val editor = preferences!!.edit()
        editor.putInt("count", count + 1)
        editor.commit()
        updateNotificationCounter()
    }

    override fun reduceNotificationCount() {
        val preferences: SharedPreferences = getSharedPreferences(PREFS_NAME, 0)
        val count = preferences!!.getInt("count", 0)
        val editor = preferences!!.edit()
        editor.putInt("count", count - 1)
        editor.commit()
        updateNotificationCounter()
    }

    override fun onPrefChanged() {
        tvUsername!!.text = preferences!!.getString(
            "prefFirstName",
            "Guest"
        ) + preferences!!.getString("prefLastName", "")
        tvEmailid!!.text = preferences!!.getString("prefEmailId", "No email id found")
    }

    override fun onClickProfilePhoto() {
        onProfilePicClickEvent()
    }

    override fun onClickCompanyLogo() {
        onCompanyLogoClickEvent()
    }

    override fun onCLickContactUs() {
        onContactUsClickEVent()
    }

    override fun onClickLogout() {
        getSmartStoreManager().logout(this, false)

        val sp = PreferenceManager.getDefaultSharedPreferences(this)
        val spe = sp.edit()
        spe.putBoolean("prefLogin", false)
        spe.commit()
        spe.clear()
        //           android.preference.PreferenceManager.getDefaultSharedPreferences(this).edit().remove(LAST_CALL_PREF).apply()
//            finish()
    }

    override fun onProfilePicChanged(notePhoto: NotePhoto?) {
        ivProfilePic!!.setImageDrawable(notePhoto!!.bitmapDrawable)
    }

    companion object {
        var TAG: String? = null
        private const val LOCATION_PERM = 101
        private const val MAP_FRAG_TAG = "MAP_FRAG_TAG"
        private const val ACTION_ROUTES = "com.org.jde.ACTION_ROUTES"
        private const val ACTION_NOTES = "com.org.jde.ACTION_NOTES"
        private const val PREFS_NAME = "notificationcount"

        fun startActivity(context: Context) {
            context.startActivity(Intent(context, MainNavActivity::class.java))
        }
    }

    override fun createPresenter(): HomePresenter = HomePresenter(
        getSmartStore(),
        sharedPrefs,
        this
    )

    override fun showRoutesContent() {
        TODO("Not yet implemented")
    }

    override fun showCatalogContent() {
        TODO("Not yet implemented")
    }

    override fun showOrdersContent() {
        TODO("Not yet implemented")
    }

    override fun showProfileContent(name: String, email: String) {
        TODO("Not yet implemented")
    }

    override fun selectFragment() {
        Logger.info("select fragment")
    }

    override fun showLoader(windowsFlagsEnabled: Boolean) {
        Logger.info("show loader")
    }

    override fun hideLoader(windowsFlagsEnabled: Boolean) {
        Logger.info("hide loader")
    }

    override fun showUnknownError() {
        TODO("Not yet implemented")
    }

    override fun onProductClick(product: Product?) {
        //Toast.makeText(this,"Clicked", Toast.LENGTH_SHORT).show()
        onProductItemClickEvent(product!!)

    }

    override fun onAccountClick(account: Account?) {
        //Toast.makeText(this,"Clicked", Toast.LENGTH_SHORT).show()
        onAccountItemClickEvent(account!!)
    }

    override fun onSearchClicked() {
        onSearchRequested()
    }

    override fun onAccountSearchClicked() {
        onSearchRequested()
    }




}
