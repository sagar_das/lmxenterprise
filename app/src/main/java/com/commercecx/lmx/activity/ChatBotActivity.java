package com.commercecx.lmx.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.commercecx.lmx.R;
import com.commercecx.lmx.adapter.ChatMessageAdapter;
import com.commercecx.lmx.util.AnimatedDotsView;
import com.commercecx.lmx.util.ChatMessage;
import com.commercecx.lmx.util.DialogflowOperations;
import com.commercecx.lmx.util.SpeechUtil;
import com.google.gson.JsonElement;




import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ai.api.AIDataService;
import ai.api.AIServiceException;
import ai.api.android.AIConfiguration;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Metadata;
import ai.api.model.Result;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatBotActivity extends AppCompatActivity
implements SpeechUtil.speakingCompleteListener{

    //region Variable decalartion
    static final String TAG = ChatBotActivity.class.getSimpleName();

    private static final int SPEECH_REQUEST_CODE = 101;
    private AIDataService aiDataService;
    private AIRequest aiRequest;
    private AIConfiguration config;
    private DialogflowOperations dialogflowOperations;
    @BindView(R.id.btn_send)
    ImageButton btn_send;
    @BindView(R.id.btn_mic)
    ImageButton btn_mic;
    @BindView(R.id.et_message)
    EditText et_message;
    @BindView(R.id.lvChatMessages)
    ListView lvChatMessages;
    @BindView(R.id.adv_1)
    AnimatedDotsView adv_1;
    ChatMessageAdapter mAdapter;

    private String currentMessage;
    private boolean isVoice = false;
    private SpeechUtil speechUtil;
    private HashMap<String, String> values;


    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_bot);
        ButterKnife.bind(this);

        setTitle("Smart Assistant");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dialogflowOperations = new DialogflowOperations(this);

        mAdapter = new ChatMessageAdapter(this, new ArrayList<ChatMessage>());

        lvChatMessages.setAdapter(mAdapter);

        speechUtil = new SpeechUtil(this,this);
        values = new HashMap<String, String>();

        config = new AIConfiguration(getResources().getString(R.string.DIALOGFLOW_CLIENT_ACCESS_TOKEN),
        AIConfiguration.SupportedLanguages.English,
        AIConfiguration.RecognitionEngine.System);

        aiDataService = new AIDataService(config);

        aiRequest = new AIRequest();




    }

    //region Events

    @OnClick(R.id.btn_send)
    public void onClickBtnSend(){

        isVoice = false;
        currentMessage = et_message.getText().toString();
        sendMessage(currentMessage,true);

        sendToDialogFlow();

    }

    private void sendToDialogFlow(){
        aiRequest.setQuery(currentMessage);
        et_message.setText("");
        adv_1.setVisibility(View.VISIBLE);
        new DialogResponseAsync().execute(aiRequest);
    }

    @OnClick(R.id.btn_mic)
    public void onClickBtnMic() {
        isVoice = true;
        initiateVoice();
    }

    public void initiateVoice() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        startActivityForResult(intent, SPEECH_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
        Intent data) {
        if (requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> results = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
            String spokenText = results.get(0);

            // Toast.makeText(this, spokenText, Toast.LENGTH_SHORT).show();
            currentMessage = spokenText;
            sendMessage(spokenText,true);
            sendToDialogFlow();
            Log.i(TAG, "onActivityResult: "+spokenText);
            // Do something with spokenText


        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSpeakingComplete() {
        onClickBtnMic();
    }


    //endreigon

    //region Dialogflow and chat message processing
    public class DialogResponseAsync extends AsyncTask<AIRequest, Void,AIResponse> {
        @Override
        protected AIResponse doInBackground(AIRequest... requests) {

        final AIRequest request = requests[0];
        try {
            final AIResponse response = aiDataService.request(aiRequest);
            return response;
        } catch (AIServiceException e) {
        }
        return null;
    }

        @Override
        protected void onPostExecute(AIResponse response) {
            if (response != null) {

                final Result result = response.getResult();

                final String speech = result.getFulfillment().getSpeech();
                Log.i(TAG, "Speech: " + speech);

                final Metadata metadata = result.getMetadata();
                if (metadata != null) {
                    Log.i(TAG, "Intent id: " + metadata.getIntentId());
                    Log.i(TAG, "Intent name: " + metadata.getIntentName());
                }

                addParamters(result);

                adv_1.setVisibility(View.GONE);

                sendMessage(speech,false);

                if(metadata.getIntentName()!=null) {
                    dialogflowOperations.processResult(metadata.getIntentName(), values);
                }

                if(isVoice){
                    speechUtil.initiateSpeech(speech);
                    isVoice = false;
                }

            }
            else{
                sendMessage("Unable to reach server",false);
            }
        }
    }

    private void sendMessage(String message, boolean mine) {
        ChatMessage chatMessage = new ChatMessage(message, mine, false);
        mAdapter.add(chatMessage);

        if(!mine){
            lvChatMessages.setSelection(mAdapter.getCount() - 1);
        }

        //mimicOtherMessage(message);
    }

    private void addParamters(Result result){
        final HashMap<String, JsonElement> params = result.getParameters();
        if (params != null && !params.isEmpty()) {
            Log.i(TAG, "Parameters: ");
            for (final Map.Entry<String, JsonElement> entry : params.entrySet()) {
                Log.i(TAG, String.format("%s: %s", entry.getKey(), entry.getValue().toString()));
                values.put(entry.getKey(),entry.getValue().toString());
            }
        }
    }

    //endregion



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

