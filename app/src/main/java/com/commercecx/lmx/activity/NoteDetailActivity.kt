package com.commercecx.lmx.activity

import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.annotation.Nullable
import butterknife.BindView
import butterknife.ButterKnife
import com.commercecx.lmx.R
import com.commercecx.lmx.base.BaseActivity
import com.commercecx.lmx.constant.Constant
import com.commercecx.lmx.model.Note
import com.veinhorn.scrollgalleryview.MediaInfo
import com.veinhorn.scrollgalleryview.ScrollGalleryView
import com.veinhorn.scrollgalleryview.loader.DefaultImageLoader
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_note_detail.*
import kotlinx.android.synthetic.main.view_common_header_back.*
import java.util.*


class NoteDetailActivity : BaseActivity() {



    protected override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_detail)
        ButterKnife.bind(this)
        headerTitleName!!.setText("Note Details")
        val intent: Intent = getIntent()
        val note_id = intent.getStringExtra(Constant.BundleKeys.NOTE_KEY)
        val realm = Realm.getDefaultInstance()
        val note: Note = realm.where(Note::class.java).contains("id", note_id).findFirst()!!
        val infos: MutableList<MediaInfo> = ArrayList()
        infos.add(
            MediaInfo.mediaLoader(
                DefaultImageLoader(
                    BitmapFactory.decodeByteArray(
                        note.getAttachments(),
                        0,
                        note.getAttachments().size
                    )
                )
            )
        )
        scroll_gallery_view!!
            .setThumbnailSize(100)
            .setZoom(true)
            .setFragmentManager(getSupportFragmentManager())
            .addMedia(infos)
        //                .addMedia(MediaInfo.mediaLoader(new DefaultImageLoader(R.drawable.wallpaper1)))
//                .addMedia(MediaInfo.mediaLoader(new DefaultImageLoader(toBitmap(R.drawable.wallpaper7))))
//                .addMedia(MediaInfo.mediaLoader(new MediaLoader() {
//                    @Override public boolean isImage() {
//                        return true;
//                    }
//
//                    @Override public void loadMedia(Context context, ImageView imageView,
//                                                    MediaLoader.SuccessCallback callback) {
//                        imageView.setImageBitmap(toBitmap(R.drawable.wallpaper3));
//                        callback.onSuccess();
//                    }
//
//                    @Override public void loadThumbnail(Context context, ImageView thumbnailView,
//                                                        MediaLoader.SuccessCallback callback) {
//                        thumbnailView.setImageBitmap(toBitmap(R.drawable.wallpaper3));
//                        callback.onSuccess();
//                    }
//                }))
//                .addMedia(MediaInfo.mediaLoader(new DefaultVideoLoader(movieUrl, R.mipmap.default_video)))
//                .addMedia(infos);
    }
}
