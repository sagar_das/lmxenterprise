package com.commercecx.lmx.activity

import android.Manifest

import android.app.AlertDialog

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.annotation.Nullable
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Optional
import com.commercecx.lmx.R
import com.commercecx.lmx.adapter.NotePhotoAdapter
import com.commercecx.lmx.base.BaseActivity
import com.commercecx.lmx.constant.Constant
import com.commercecx.lmx.fragment.AttachFragment
import com.commercecx.lmx.fragment.CameraFragment
import com.commercecx.lmx.model.Note
import com.commercecx.lmx.service.NoteJobService
import com.commercecx.lmx.util.Logger
import com.commercecx.lmx.util.NoteAttachUtils
import com.commercecx.lmx.util.NotePhoto
import com.google.android.material.bottomsheet.BottomSheetDialog
import io.realm.Realm
import io.realm.RealmResults
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_add_note.*


class AddNoteActivity : BaseActivity(), CameraFragment.CameraFragCallbacks,
    NotePhotoAdapter.NotePhotoAdapterClickListener, TextWatcher {
    private val dialog: BottomSheetDialog? = null
    private var fragmentTransaction: FragmentTransaction? = null
    private var file: File? = null
    private val sourceFile: File? = null
    private val destFile: File? = null
    private val dateFormatter: SimpleDateFormat? = null
    private var realm: Realm? = null


    var btnLL: LinearLayout? = null
    var mPaths: List<String>? = null
    private var note: Note? = null
    private var note_id: String? = null
    private var attachPAth: String? = null
    private var attachName: String? = null
    private var fragCamera: CameraFragment? = null
    private var fragAttach: AttachFragment? = null
    private var notePhotoAdapter: NotePhotoAdapter? = null
    private var photoList: MutableList<NotePhoto>? = null
    private val filesList: MutableList<ByteArray> = ArrayList()
    private val fileTypeList: MutableList<String> = ArrayList()
    private var linearLayoutManager: LinearLayoutManager? = null
    private var isContentChanged = false

    //endregion
    protected override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_note)
        ButterKnife.bind(this)
        TAG = "FRAGMENT"
        isContentChanged = false
        nameNoteTV!!.addTextChangedListener(this)
        tvNotes!!.addTextChangedListener(this)
        photoList = ArrayList<NotePhoto>()
        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        setTitle(getResources().getString(R.string.new_note))
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.inflateMenu(R.menu.add_note_menu)
        setSupportActionBar(toolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)
        mPaths = ArrayList()
        val intent: Intent = getIntent()
        note_id = intent.getStringExtra(Constant.BundleKeys.NOTE_KEY)
        realm = Realm.getDefaultInstance()
        note = realm!!.where(Note::class.java).contains("id", note_id).findFirst()
        if (note_id != null) {
            nameNoteTV!!.setText(note!!.getName())
            tvNotes!!.setText(note!!.getRemark())
            isContentChanged = false
            if (note!!.isAttachment()) {
                val bitmapDrawable = loadAttachment(note!!.getAttachPath()!!, note!!.getAttachName()!!)
                if (llImages!!.visibility == View.GONE) {
                    llImages!!.visibility = View.VISIBLE
                }
                // photoList.add(new NotePhoto(this, note.getAttachments()));
                photoList!!.add(NotePhoto(bitmapDrawable))
                rvAttachedImages!!.setLayoutManager(linearLayoutManager)
                notePhotoAdapter = NotePhotoAdapter(photoList!!, this)
                rvAttachedImages!!.setAdapter(notePhotoAdapter)
            }
        }
    }

    //region Permissions
    private fun checkCameraPerission(): Boolean {
        return if (ContextCompat.checkSelfPermission(
                this@AddNoteActivity,
                Manifest.permission.WRITE_CALENDAR
            )
            !== PackageManager.PERMISSION_GRANTED
        ) {
            // Permission is not granted
            false
        } else {
            true
        }
    }

    private fun requestCameraPermission() {
        if (!ActivityCompat.shouldShowRequestPermissionRationale(
                this@AddNoteActivity,
                Manifest.permission.CAMERA
            )
        ) {
            ActivityCompat.requestPermissions(
                this@AddNoteActivity, arrayOf(Manifest.permission.CAMERA),
                CAMERA_PERM
            )
        }
    }

    //endregion
    //region Results
    protected override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK) {
            if (data != null && data.data != null) {
                val uriData = data.data
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uriData)
                    saveAttachmentToLocalDb(bitmap)
                    val notePhoto = NotePhoto(this, bitmap)
                    setAttachmentAdapter(notePhoto)
                } catch (e: IOException) {
                    e.printStackTrace()
                    Log.e(TAG, "Unable to select image from gallery", e)
                }
            }
        } else if (requestCode == RESULT_LOAD_DOC && resultCode == RESULT_OK) {
            if (data != null && data.data != null) {
                val uriData = data.data
                var fileBytes: ByteArray
                var fileType: String
                try {
                    getContentResolver().openInputStream(uriData!!).use { ipStream ->
                        fileBytes = NoteAttachUtils.getBytes(ipStream!!)
                        fileType = getContentResolver().getType(uriData)!!
                        filesList.add(fileBytes)
                        fileTypeList.add(fileType)
                        val notePhoto = NotePhoto(true)
                    }
                } catch (e: Exception) {
                    Log.e(TAG, "Unable to get input stream", e)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            CAMERA_PERM ->                 // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    initiateCameraFragment()
                } else {
                    Toast.makeText(this, "Camera permission was denied", Toast.LENGTH_SHORT).show()
                }
        }
    }

    //endregion
    //region Fragment control
    private fun initiateCameraFragment() {
        flCamera!!.visibility = View.VISIBLE
        view_note_sub_header!!.visibility = View.GONE
        tvNotes!!.setVisibility(View.GONE)
        if (llImages!!.visibility == View.VISIBLE) {
            llImages!!.visibility = View.GONE
        }
        if (fragCamera == null) {
            fragCamera = CameraFragment()
            fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction!!.add(R.id.flCamera, fragCamera!!, CAM_FRAG_TAG)
            fragmentTransaction!!.commit()
        }
    }

    override fun onPhotoClick(photo: NotePhoto?, position: Int) {
        flCamera!!.visibility = View.VISIBLE
        view_note_sub_header!!.visibility = View.GONE
        tvNotes!!.setVisibility(View.GONE)
        if (llImages!!.visibility == View.VISIBLE) {
            llImages!!.visibility = View.GONE
        }
        if (fragAttach == null) {
            fragAttach = AttachFragment()
            val b = Bundle()
            b.putSerializable("NOTE_PHOTO", photo)
            fragAttach!!.setArguments(b)
            fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction!!.add(R.id.flCamera, fragAttach!!, PHOTO_FRAG_TAG)
            fragmentTransaction!!.commit()
        }
    }

    override fun onPhotoDelete() {
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setTitle("Are you sure of deleting?")
            .setMessage("Deleted contents cannot be recovered.")
            .setPositiveButton(
                "OK"
            ) { dialogInterface, i ->
                photoList!!.removeAt(0)
                llImages!!.visibility = View.GONE
                isContentChanged = true
            }
            .setNegativeButton(
                "Cancel"
            ) { dialogInterface, i -> }
            .create()
        val dialog = dialogBuilder.create()
        dialog.show()
    }

    //endregion
    override fun onBackPressed() {
        var frag: Fragment? = null
        if (supportFragmentManager.findFragmentByTag(CAM_FRAG_TAG).also({ frag = it }) != null) {
            supportFragmentManager.beginTransaction().remove(frag!!).commit()
            fragCamera = null
            flCamera!!.visibility = View.GONE
            view_note_sub_header!!.visibility = View.VISIBLE
            tvNotes!!.visibility = View.VISIBLE
            if (photoList != null && photoList!!.size > 0) {
                llImages!!.visibility = View.VISIBLE
            }
        } else if (supportFragmentManager.findFragmentByTag(PHOTO_FRAG_TAG)
                .also({ frag = it }) != null
        ) {
            supportFragmentManager.beginTransaction().remove(frag!!).commit()
            fragAttach = null
            flCamera!!.visibility = View.GONE
            view_note_sub_header!!.visibility = View.VISIBLE
            tvNotes!!.setVisibility(View.VISIBLE)
            llImages!!.visibility = View.VISIBLE
        } else if (isContentChanged) {

            //Show confirmation dialog
            val dialogBuilder = AlertDialog.Builder(this)
            val discardDialog: View = getLayoutInflater().inflate(R.layout.delete_dialog, null)
            dialogBuilder.setView(discardDialog)
            val dialog = dialogBuilder.create()
            val discard = discardDialog.findViewById<View>(R.id.btnDiscard) as Button
            val cancel = discardDialog.findViewById<View>(R.id.btnCancel) as Button
            discard.setOnClickListener { //                    AddNoteActivity.super.onBackPressed();
                finish()
            }
            cancel.setOnClickListener { dialog.dismiss() }
            dialog.show()


            //  super.onBackPressed();
        } else {
            finish()
        }
        Logger.debug("OnBackPressed...")
    }

    @Optional
    @OnClick(R.id.back_btn)
    fun onClickBackBtn() {
        //Logger.debug("OnBackPressed...");

        //Show confirmation dialog
        val dialogBuilder = AlertDialog.Builder(this)
        val discardDialog: View = getLayoutInflater().inflate(R.layout.delete_dialog, null)
        dialogBuilder.setView(discardDialog)
        val dialog = dialogBuilder.create()
        val discard = discardDialog.findViewById<View>(R.id.btnDiscard) as Button
        val cancel = discardDialog.findViewById<View>(R.id.btnCancel) as Button
        discard.setOnClickListener { finish() }
        cancel.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    @Optional
    @OnClick(R.id.saveBtn)
    fun onClickSaveBtn() {
        Logger.debug("onSavePressed...")
        val d = Date()
        val updatedTSmp = d.time
        val files: MutableList<ByteArray> = ArrayList()

        //TODO: Show progress bar while saving
        if (tvNotes!!.text.toString().isEmpty()) {
            Toast.makeText(
                this@AddNoteActivity,
                getResources().getString(R.string.note_save),
                Toast.LENGTH_LONG
            ).show()
        } else {
            val realm = Realm.getDefaultInstance()
            if (note_id == null) {
                val notes: List<Note> = realm.where(Note::class.java).findAll()
                val note = Note()
                note.setId(saltString)
                note.setIndex(notes.size)
                note.setName(
                    if (nameNoteTV!!.getText().toString()
                            .equals("")
                    ) "No Title" else nameNoteTV!!.getText().toString()
                )
                note.setRemark(tvNotes!!.getText().toString())
                note.setServerCommitted(false)
                note.setPendingOperation("insert")
                note.setUpdatedTSMP(updatedTSmp)


                //Added by Sagar Das
                if (filesList.size > 0) {
                    note.setAttachment(true)
                    for (attachFiles in filesList) {
                        files.add(attachFiles)
                    }
                    note.setAttachments(files[0])
                }

                //TODO: Perform this bitmap operation on new thread
                if (photoList!!.size > 0) {
                    note.setAttachment(true)
                    note.setAttachPath(attachPAth)
                    note.setAttachName(attachName)
                } else {
                    note.setAttachment(false)
                }
                note.setActive(false)
                val df = android.text.format.DateFormat()
                note.setDate(android.text.format.DateFormat.format("MM/dd/yy", Date()).toString())
                realm.executeTransactionAsync { realm -> realm.insertOrUpdate(note) }
                finish()
            } else {
                if (filesList.size > 0) {
                    note!!.setAttachment(true)
                    for (attachFiles in filesList) {
                        files.add(attachFiles)
                    }
                    note!!.setAttachments(files[0])
                }
                realm.executeTransaction {
                    note!!.setName(
                        if (nameNoteTV!!.getText().toString()
                                .equals("")
                        ) "No Title" else nameNoteTV!!.getText().toString()
                    )
                    note!!.setRemark(tvNotes!!.getText().toString())
                    note!!.setServerCommitted(false)
                    note!!.setPendingOperation("update")
                    note!!.setUpdatedTSMP(updatedTSmp)
                    //                        if (mPaths.size() > 0) {
                    //                            note.setAttachment(true);
                    //                            List<byte[]> files = new ArrayList<>();
                    //                            for (String path : mPaths) {
                    //                                files.add(readBytesFromFile(path));
                    //                            }
                    //                            note.setAttachments(files.get(0));
                    //                        }
                    //Added by Sagar Das
                    if (files.size > 0) {
                        note!!.setAttachment(true)
                        note!!.setAttachments(files[0])
                    } else if (photoList!!.size > 0) {
                        note!!.setAttachment(true)
                        note!!.setAttachPath(attachPAth)
                        note!!.setAttachName(attachName)
                    } else {
                        note!!.setAttachment(false)
                    }
                    val df = android.text.format.DateFormat()
                    note!!.setDate(
                        android.text.format.DateFormat.format("MM/dd/yy", Date())
                            .toString()
                    )
                }
                finish()
            }
        }
    }

    // length of the random string.
    private val saltString: String
        private get() {
            val SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
            val salt = StringBuilder()
            val rnd = Random()
            while (salt.length < 18) { // length of the random string.
                val index = (rnd.nextFloat() * SALTCHARS.length).toInt()
                salt.append(SALTCHARS[index])
            }
            return salt.toString()
        }

    private fun scheduleNoteUpdateJob() {
        val jobScheduler = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        val jobService = ComponentName(this, NoteJobService::class.java)
        val jobInfo = JobInfo.Builder(NOTE_UPDATE_JOB_ID, jobService)
            .setPersisted(true)
            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
            .build()
        if (jobScheduler.schedule(jobInfo) != JobScheduler.RESULT_SUCCESS) {
            Log.d(TAG, "job scheduling failed ")
        }
    }

    //region Attachment Functionality
    private fun attachDocument() {
        dialog!!.cancel()
        val mimeTypes = arrayOf("application/pdf")

        //Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.type = if (mimeTypes.size == 1) mimeTypes[0] else "*/*"
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)

        //startActivityForResult(Intent.createChooser(intent,"ChooseFile"),RESULT_LOAD_DOC);
        startActivityForResult(intent, RESULT_LOAD_DOC)
    }

    private fun attachCamera() {
        //dialog.cancel();

        //Ensuring only one attachment is added
        if (photoList!!.size > 0) {
            Toast.makeText(this, getResources().getString(R.string.note_attach), Toast.LENGTH_SHORT)
                .show()
            return
        }
        if (!checkCameraPerission()) {
            requestCameraPermission()
        } else {
            //Displaying camera fragment
            initiateCameraFragment()
        }
    }

    private fun attachPhotoLibrary() {
        // dialog.cancel();

        //Ensuring only one attachment is added
        if (photoList!!.size > 0) {
            Toast.makeText(this, getResources().getString(R.string.note_attach), Toast.LENGTH_SHORT)
                .show()
            return
        }
        val mimeTypes = arrayOf("image/*")
        val galleryIntent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        galleryIntent.addCategory(Intent.CATEGORY_OPENABLE)
        galleryIntent.type = mimeTypes[0]
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG)
    }

    private fun saveAttachmentToLocalDb(bitmapImage: Bitmap) {
        val contextWrapper = ContextWrapper(getApplicationContext())
        val directory = contextWrapper.getDir("NoteAttachments", Context.MODE_PRIVATE)

        //Forming name
        val date = Date()
        val dateFormat: DateFormat = SimpleDateFormat("MM-dd-yyyy")
        attachName = dateFormat.format(date) + "_" + java.lang.Long.toString(date.time) + ".jpg"
        file = File(directory, attachName)
        attachPAth = directory.absolutePath
        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(file)
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos)
        } catch (e: Exception) {
            Log.i(TAG, "saveAttachmentToLocalDb: Error in saving attachments", e)
        } finally {
            try {
                fos!!.close()
            } catch (e: Exception) {
                Log.e(TAG, "Error closing file output stream ", e)
            }
        }
    }

    private fun loadAttachment(path: String, name: String): BitmapDrawable? {
        var bitmapDrawable: BitmapDrawable? = null
        try {
            val f = File(path, name)
            val b = BitmapFactory.decodeStream(FileInputStream(f))
            bitmapDrawable = BitmapDrawable(getResources(), b)
            Log.i(TAG, "loadAttachment: ")
        } catch (e: Exception) {
            Log.e(TAG, "loadAttachment: ", e)
        }
        return bitmapDrawable
    }

    override fun onPhotoCaptured(filename: String?) {
        val oldFrag: Fragment = supportFragmentManager.findFragmentByTag(CAM_FRAG_TAG)!!
        supportFragmentManager.beginTransaction().remove(oldFrag).commit()
        fragCamera = null
        flCamera!!.visibility = View.GONE
        val absolutePath: String = getFileStreamPath(filename).getAbsolutePath()
        val notePhoto = NotePhoto(absolutePath, this)
        setAttachmentAdapter(notePhoto)
        saveAttachmentToLocalDb(notePhoto.bitmap!!)
    }

    //endregion
    private fun setAttachmentAdapter(notePhoto: NotePhoto) {
        view_note_sub_header!!.visibility = View.VISIBLE
        tvNotes!!.setVisibility(View.VISIBLE)
        if (llImages!!.visibility == View.GONE) {
            llImages!!.visibility = View.VISIBLE
        }
        photoList!!.add(notePhoto)
        if (notePhotoAdapter != null) {
            notePhotoAdapter!!.swapDataSet(photoList!!)
        } else {
            notePhotoAdapter = NotePhotoAdapter(photoList!!, this)
            rvAttachedImages!!.setLayoutManager(linearLayoutManager)
            rvAttachedImages!!.setAdapter(notePhotoAdapter)
        }
    }

    private fun showTagsDialog() {
        //Show confirmation dialog
        val dialogBuilder = AlertDialog.Builder(this)
        val tagsDialog: View = getLayoutInflater().inflate(R.layout.note_tag_dialog, null)
        dialogBuilder.setView(tagsDialog)
        val dialog = dialogBuilder.create()
        val ok = tagsDialog.findViewById<View>(R.id.btnOK) as Button
        val cancel = tagsDialog.findViewById<View>(R.id.btnCancel) as Button
        ok.setOnClickListener { dialog.dismiss() }
        cancel.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val deleteItem = menu.findItem(R.id.action_delete)
        deleteItem.isVisible = note_id != null
        return true
    }

    //region Menu Functionality
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.add_note_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save -> onClickSaveBtn()
            R.id.action_camera -> attachCamera()
            R.id.action_attach -> attachPhotoLibrary()
            R.id.action_tags -> showTagsDialog()
            R.id.action_delete -> deleteNote()
            R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    //endregion
    //region Text change listener
    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
    override fun afterTextChanged(editable: Editable) {
        isContentChanged = true
    }

    //endregion
    private fun deleteNote() {


        //Show confirmation dialog
        val dialogBuilder = AlertDialog.Builder(this)
        val discardDialog: View = getLayoutInflater().inflate(R.layout.note_delete_dialog, null)
        dialogBuilder.setView(discardDialog)
        val dialog = dialogBuilder.create()
        val delete = discardDialog.findViewById<View>(R.id.btnDelete) as Button
        val cancel = discardDialog.findViewById<View>(R.id.btnCancel) as Button
        delete.setOnClickListener {
            realm!!.executeTransaction { realm ->
                val result: RealmResults<Note> =
                    realm.where(Note::class.java).equalTo("id", note_id).findAll()
                note_id = result[0]!!.getId()
                result.deleteAllFromRealm()
            }
            finish()
        }
        cancel.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    companion object {
        //region Variable declaration
        var TAG = AddNoteActivity::class.java.simpleName
        private const val NOTE_UPDATE_JOB_ID = 201
        private const val CAM_FRAG_TAG = "CAMERA_TAG"
        private const val PHOTO_FRAG_TAG = "PHOTO_TAG"
        private const val CAMERA_PERM = 101
        private const val RESULT_LOAD_IMG = 102
        private const val RESULT_LOAD_DOC = 103
    }
}
