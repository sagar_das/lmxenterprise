package com.commercecx.lmx.activity

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.lmx.R
import com.commercecx.lmx.model.RouteOld
import com.commercecx.lmx.view.JDECustomEditText
import com.commercecx.lmx.view.JDECustomTextView
import kotlinx.android.synthetic.main.activity_add_route.*
import java.text.SimpleDateFormat
import java.util.*

class AddRouteActivity : AppCompatActivity() {

    var bundle: Bundle? = null
    val finalCalendar = Calendar.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_route)
        bundle = intent.getBundleExtra("ROUTE_BUNDLE")
        ButterKnife.bind(this)
    }

    override fun onResume() {
        super.onResume()
        var route: RouteOld? = null
        if (bundle != null) route = bundle!!.getSerializable("ROUTE_DATA") as RouteOld?
        if (route != null) {
            nameRouteTV!!.setText(route.getName())
            addressRouteTV!!.setText(route.getAddress())
            customerNameTV!!.setText(route.getCustomerName())
            customerPhoneTV!!.setText(route.getCustomerPhoneNo())
        }
    }

    @OnClick(R.id.createRouteBtn)
    fun onCreateOrderBtn() {
        if (tvDateText!!.text.toString()
                .equals("No date selected",true) || nameRouteTV!!.getText().toString()
                .equals("",true)
            || addressRouteTV!!.text.toString().equals("",true)
        ) {
            Toast.makeText(this, "Please enter all details", Toast.LENGTH_SHORT).show()
            return
        }
        val dateformat = SimpleDateFormat("MM-dd-yyyy")
        //        Calendar calendar = Calendar.getInstance();
//        calendar.set(dpRouteDate.getYear(), dpRouteDate.getMonth(), dpRouteDate.getDayOfMonth());
        val intent = Intent(this@AddRouteActivity, ReviewRouteActivity::class.java)
        val b = Bundle()
        val route = RouteOld()
        route.setName(nameRouteTV!!.text.toString())
        route.setAddress(addressRouteTV!!.text.toString())
        route.setrouteDatee(dateformat.format(finalCalendar.time))
        route.setCustomerName(customerNameTV!!.text.toString())
        route.setCustomerPhoneNo(customerPhoneTV!!.text.toString())
        b.putSerializable("ROUTE_BUNDLE", route)
        intent.putExtra("ROUTE_DATA", b)
        startActivity(intent)
    }

    @OnClick(R.id.datePickerBtn)
    fun onSelectDate() {
        val calendar = Calendar.getInstance()
        val day = calendar[Calendar.DAY_OF_MONTH]
        val month = calendar[Calendar.MONTH]
        val year = calendar[Calendar.YEAR]
        val picker = DatePickerDialog(
            this,
            { view, year, monthOfYear, dayOfMonth ->
                tvDateText!!.text = "Route date : " + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year
                finalCalendar[year, monthOfYear] = dayOfMonth
            }, year, month, day
        )
        picker.show()
    }
}
