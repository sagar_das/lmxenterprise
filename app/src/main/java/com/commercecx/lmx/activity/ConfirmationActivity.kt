package com.commercecx.lmx.activity

import android.content.Intent
import android.os.Bundle
import androidx.annotation.Nullable
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.lmx.R
import com.commercecx.lmx.base.BaseActivity
import com.commercecx.lmx.util.Logger
import com.commercecx.lmx.view.JDECustomTextView
import kotlinx.android.synthetic.main.activity_confirmation.*


class ConfirmationActivity : BaseActivity() {

    protected override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)
        ButterKnife.bind(this)
        val intent: Intent = intent
        if (intent != null) {
            when (intent.getStringExtra("TYPE_OF_ACTION")) {
                "ROUTE" -> {
                    createOrder!!.text = "The route is created successfully"
                    createOrder!!.text = "The order is created successfully"
                }
                "ORDER" -> createOrder!!.text = "The order is created successfully"
            }
        }
    }

    @OnClick(R.id.OkBtn)
    fun submitOrder() {
        Logger.debug("onCreateOrderBtn...")
        val intent = Intent(applicationContext, MainNavActivity::class.java)
        intent.action = ACTION_ROUTES
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    companion object {
        private const val ACTION_ROUTES = "com.org.jde.ACTION_ROUTES"
    }
}
