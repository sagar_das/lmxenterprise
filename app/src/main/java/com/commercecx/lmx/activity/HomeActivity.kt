package com.commercecx.lmx.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.commercecx.lmx.R
import com.salesforce.androidsdk.rest.RestClient
import com.salesforce.androidsdk.ui.SalesforceActivity

class HomeActivity : SalesforceActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }

    override fun onResume(client: RestClient?) {
        TODO("Not yet implemented")
    }
}