package com.commercecx.lmx.activity


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.speech.RecognizerIntent
import android.speech.tts.TextToSpeech
import android.speech.tts.TextToSpeech.OnInitListener

import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.commercecx.lmx.R
import com.commercecx.lmx.adapter.NotificationAdapter
import com.commercecx.lmx.base.BaseActivity
import com.commercecx.lmx.model.Note
import com.commercecx.lmx.model.Notification
import com.google.android.material.floatingactionbutton.FloatingActionButton
import io.realm.Realm
import kotlinx.android.synthetic.main.dashboard_stats_main.*
import java.util.*



class DashboardActivity : BaseActivity(), OnInitListener {
    private var tts: TextToSpeech? = null
    private var isNote = false
    private val fabVoice: FloatingActionButton? = null


    var notificationAdapter: NotificationAdapter? = null
    var linearLayoutManager: LinearLayoutManager? = null
    private var text: String? = null
    private var notificationList: List<Notification>? = null
    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        ButterKnife.bind(this)

        //  rvNotifications!!.addItemDecoration(new DividerItemDecoration(DashboardActivity.this, LinearLayoutManager.HORIZONTAL));
        createNotifications()
        notificationAdapter =
            NotificationAdapter(Fragment(), applicationContext, notificationList!!)
        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvNotifications!!.setLayoutManager(linearLayoutManager)
        rvNotifications!!.setAdapter(notificationAdapter)
        setTitle("Dashboard")
        text = "Sorry, didn't understand you"

        // fabVoice = (FloatingActionButton)findViewById(R.id.fabVoice);
        fabVoice!!.setOnClickListener(View.OnClickListener { initiateVoice() })

        // tts = new TextToSpeech(this, this);
    }

    private fun createNotifications() {
        notificationList = ArrayList<Notification>()
        //        Notification n1 = new Notification("Customer", "Assigned with Harry's customer","customer");
//        Notification n2 = new Notification("Truck", "Item loaded in truck","truck");
//        Notification n3 = new Notification("Customer", "Assigned with Josh's customer","customer");
//        Notification n4 = new Notification("Truck", "Item loaded in truck","truck");
//        Notification n5 = new Notification("See More", " ","more");
//        notificationList.add(n1);
//        notificationList.add(n2);
//        notificationList.add(n3);
//        notificationList.add(n4);
//        notificationList.add(n5);
    }

    private fun initiateVoice() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        startActivityForResult(intent, SPEECH_REQUEST_CODE)
    }

    protected override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
            val results: List<String> = data!!.getStringArrayListExtra(
                RecognizerIntent.EXTRA_RESULTS
            )
            val spokenText = results[0]
            Toast.makeText(this, spokenText, Toast.LENGTH_SHORT).show()
            parseVoiceResults(spokenText)
            Log.i(TAG, "onActivityResult: $spokenText")
            // Do something with spokenText
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun parseVoiceResults(spokenText: String) {
        if (spokenText.contains("note")) {
            val index = spokenText.indexOf("note", 0)
            //speakOut("note");
            isNote = true
            val contents = spokenText.substring(index + 4, spokenText.length)
            saveNote(contents)
        } else {
            tts = TextToSpeech(this, this)
        }
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            val result = tts!!.setLanguage(Locale.US)
            if (result == TextToSpeech.LANG_MISSING_DATA
                || result == TextToSpeech.LANG_NOT_SUPPORTED
            ) {
                Log.e("TTS", "This Language is not supported")
            } else {
                Toast.makeText(this, "Initilization Success!", Toast.LENGTH_SHORT).show()
                speakOut(text)
            }
        } else {
            Log.e("TTS", "Initilization Failed!")
            Toast.makeText(this, "Initilization Failed!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun speakOut(type: String?) {
        tts!!.speak(text, TextToSpeech.QUEUE_FLUSH, null)
    }

    private fun saveNote(contents: String) {
        val realm = Realm.getDefaultInstance()
        val notes: List<Note> = realm.where(Note::class.java).findAll()
        val note = Note()
        note.setId(saltString)
        note.setIndex(notes.size)
        note.setName("No Title")
        note.setRemark(contents)
        note.setAttachment(false)
        note.setActive(false)
        val df = DateFormat()
        note.setDate(DateFormat.format("MM/dd/yy", Date()).toString())
        realm.executeTransactionAsync { realm -> realm.insertOrUpdate(note) }
        val handler = Handler()
        handler.postDelayed({ //Do something after 100ms
            Toast.makeText(this@DashboardActivity, "Note is saved", Toast.LENGTH_SHORT).show()
            text = "Alright! Note has been created."
            tts = TextToSpeech(this@DashboardActivity, this@DashboardActivity)
        }, 4000)
    }

    // length of the random string.
    private val saltString: String
        private get() {
            val SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
            val salt = StringBuilder()
            val rnd = Random()
            while (salt.length < 18) { // length of the random string.
                val index = (rnd.nextFloat() * SALTCHARS.length).toInt()
                salt.append(SALTCHARS[index])
            }
            return salt.toString()
        }

    companion object {
        private const val SPEECH_REQUEST_CODE = 101
        private val TAG = DashboardActivity::class.java.simpleName
    }
}
