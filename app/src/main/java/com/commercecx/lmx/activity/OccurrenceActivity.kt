package com.commercecx.lmx.activity

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import androidx.annotation.Nullable
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.lmx.R
import com.commercecx.lmx.base.BaseActivity
import com.commercecx.lmx.service.OrderNotificationService

import com.commercecx.lmx.spinner.MaterialSpinner

import com.commercecx.lmx.util.Logger
import com.commercecx.lmx.view.JDECustomTextView
import kotlinx.android.synthetic.main.activity_occurrence.*
import kotlinx.android.synthetic.main.view_common_header_back.*
import java.util.*


class OccurrenceActivity : BaseActivity() {

    protected override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_occurrence)
        ButterKnife.bind(this)
        headerTitleName!!.setText("Review Cart")
        spinner!!.setHint("Choose...")
        spinner!!.setItems("Do not reoccur", "Weekly", "Monthly", "Yearly")
        spinner!!.setOnItemSelectedListener(object : MaterialSpinner.OnItemSelectedListener<String?> {
            override fun onItemSelected(view: MaterialSpinner?, position: Int, id: Long, item: String?) {
//                Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                if (position > 0) {
                    val reminderIntent = Intent(
                        getApplicationContext(),
                        OrderNotificationService::class.java
                    )
                    val alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager
                    val pendingIntent =
                        PendingIntent.getService(this@OccurrenceActivity, 0, reminderIntent, 0)
                    val calendar = Calendar.getInstance()
                    calendar[Calendar.SECOND] = 0
                    calendar[Calendar.MINUTE] = 0
                    calendar[Calendar.HOUR] = 8
                    calendar[Calendar.AM_PM] = Calendar.AM
                    calendar.add(Calendar.DAY_OF_MONTH, 1)
                    alarmManager.setRepeating(
                        AlarmManager.RTC_WAKEUP,
                        calendar.timeInMillis,
                        1000 * 60 * 60 * 24.toLong(),
                        pendingIntent
                    )
                }
            }
        })
    }

    @OnClick(R.id.back_btn)
    fun onClickBackBtn() {
        Logger.debug("OnBackPressed...")
        finish()
    }

    @OnClick(R.id.submitOrder)
    fun submitOrder() {
        Logger.debug("onCreateOrderBtn...")
        val intent = Intent(getApplicationContext(), ConfirmationActivity::class.java)
        intent.putExtra("TYPE_OF_ACTION", "ORDER")
        startActivity(intent)
    }

    @OnClick(R.id.skipButton)
    fun skipOrder() {
        Logger.debug("onCreateOrderBtn...")
        val intent = Intent(getApplicationContext(), ConfirmationActivity::class.java)
        startActivity(intent)
    }
}
