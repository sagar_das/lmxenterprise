package com.commercecx.lmx.activity

import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle

import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.lmx.R
import com.commercecx.lmx.model.ProductOld
import com.commercecx.lmx.view.JDECustomEditText
import com.commercecx.lmx.view.JDECustomTextView
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_new_catalog_item.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class NewCatalogItemActivity : AppCompatActivity() {

    var activity: Activity? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_catalog_item)
        activity = this
        ButterKnife.bind(this)
    }

    @OnClick(R.id.createItemBtn)
    fun OnClickCreateItem() {
        val dialogBuilder = AlertDialog.Builder(this)
        val completeDialog: View = layoutInflater.inflate(R.layout.complete_dialog, null)
        dialogBuilder.setView(completeDialog)
        val dialog = dialogBuilder.create()
        val title = completeDialog.findViewById<View>(R.id.title) as TextView
        val btnYes = completeDialog.findViewById<View>(R.id.btnYes) as Button
        val btnNo = completeDialog.findViewById<View>(R.id.btnNo) as Button
        title.text = "Proceed to create this product?"
        btnYes.setOnClickListener {
            val realm = Realm.getDefaultInstance()
            val results: RealmResults<ProductOld> = realm.where(ProductOld::class.java).findAll()
            val id = if (results.size > 0) Integer.toString(
                results[0]!!.getId()!!.toInt() + 1
            ) else "1"
            val d = Date()
            val dateFormat: DateFormat = SimpleDateFormat("MM-dd-yyyy")
            val strDate = dateFormat.format(d)
            val product = ProductOld()
            product.setId(id)
            product.setIndex(id.toInt())
            product.setName(tvItemName!!.text.toString())
            product.setItem_price(tvItemPrice!!.text.toString())
            product.setItem_tax(tvItemTax!!.text.toString())
            product.setDate(strDate)
            realm.executeTransactionAsync { realm -> realm.insertOrUpdate(product) }
            dialog.dismiss()
            finish()
        }
        btnNo.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }
}
