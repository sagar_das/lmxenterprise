package com.commercecx.lmx.activity

import android.content.Intent
import android.os.Bundle

import android.text.Html
import androidx.appcompat.app.AppCompatActivity
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.lmx.R
import com.commercecx.lmx.model.RecentActivityDash
import com.commercecx.lmx.model.RouteOld
import com.commercecx.lmx.view.JDECustomTextView
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_review_route.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class ReviewRouteActivity : AppCompatActivity() {

    var route: RouteOld? = null
    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review_route)
        ButterKnife.bind(this)
    }

    protected override  fun onResume() {
        super.onResume()
        val intent: Intent = getIntent()
        route = intent.getBundleExtra("ROUTE_DATA").getSerializable("ROUTE_BUNDLE") as RouteOld?
        tvRouteName!!.setText(Html.fromHtml("<b>Route name: </b>" + route!!.getName()))
        tvRouteAddress!!.setText(Html.fromHtml("<b>Route address: </b>" + route!!.getAddress()))
        tvRoutedate!!.setText(Html.fromHtml("<b>Date: </b>" + route!!.getrouteDate()))
        tvCustomerName!!.setText(Html.fromHtml("<b>Customer name: </b>" + route!!.getCustomerName()))
        tvCustomerPhoneNo!!.setText(Html.fromHtml("<b>Customer Phone No: </b>" + route!!.getCustomerPhoneNo()))
    }

    @OnClick(R.id.confirmRouteBtn)
    fun onConfirmRouteClick() {
        val realm = Realm.getDefaultInstance()
        val results: RealmResults<RouteOld> = realm.where(RouteOld::class.java).findAll()
        val id = if (results.size > 0) Integer.toString(
            results[results.size - 1]!!.getId()!!.toInt() + 1
        ) else "1"
        val d = Date()
        val dateFormat: DateFormat = SimpleDateFormat("MM-dd-yyyy")
        val strDate = dateFormat.format(d)
        val updatedTSmp = d.time

        //Toast.makeText(this, "id "+id, Toast.LENGTH_SHORT).show();
        route!!.setId(id)
        route!!.setActive(false)
        route!!.setIndex(id.toInt())
        route!!.setDate(strDate)
        val recentActivity = RecentActivityDash()
        val raResults: RealmResults<RecentActivityDash> =
            realm.where(RecentActivityDash::class.java).findAll()
        val raId = if (raResults.size > 0) raResults[0]!!.getId() + 1 else 1
        recentActivity.setId(raId)
        recentActivity.setActivity_name("Created a Route")
        recentActivity.setActivity_date(strDate)
        realm.executeTransactionAsync { realm ->
            realm.insertOrUpdate(route)
            realm.insertOrUpdate(recentActivity)
        }
        val intent = Intent(this, ConfirmationActivity::class.java)
        intent.putExtra("TYPE_OF_ACTION", "ROUTE")
        startActivity(intent)
    }
}
