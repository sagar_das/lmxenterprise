package com.commercecx.lmx.activity


import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle

import android.view.MenuItem
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.widget.Toolbar
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.lmx.R
import com.commercecx.lmx.base.BaseActivity
import com.commercecx.lmx.util.Logger
import com.commercecx.lmx.view.JDECustomEditText
import kotlinx.android.synthetic.main.activity_create_new_order.*
import kotlinx.android.synthetic.main.activity_create_new_order.tvDateText
import java.util.*


class CreateNewOrderActivity : BaseActivity() {

    var bundle: Bundle? = null
    val finalCalendar = Calendar.getInstance()
    protected override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_new_order)
        ButterKnife.bind(this)
        setTitle("New Order")
        bundle = getIntent().getBundleExtra("ROUTE_BUNDLE")
        val toolbar: Toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)
    }

    @OnClick(R.id.createOrderBtn)
    fun onCreateOrderBtn() {
        Logger.debug("onCreateOrderBtn...")
        if (nameNoteTV!!.text.toString().isNotEmpty()) {
            val intent = Intent(this, CatalogActivity::class.java)
            intent.putExtra("name", nameNoteTV!!.text.toString())
            intent.putExtra("ROUTE_BUNDLE", bundle)
            startActivity(intent)
        } else {
            Toast.makeText(
                this,
                "Please Enter Name of the Order which will be used for future reference",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    @OnClick(R.id.datePickerBtn)
    fun onSelectDate() {
        val calendar = Calendar.getInstance()
        val day = calendar[Calendar.DAY_OF_MONTH]
        val month = calendar[Calendar.MONTH]
        val year = calendar[Calendar.YEAR]
        val picker = DatePickerDialog(
            this,
            { view, year, monthOfYear, dayOfMonth ->
                tvDateText!!.text = "Order date : " + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year
                finalCalendar[year, monthOfYear] = dayOfMonth
            }, year, month, day
        )
        picker.show()
    }
}
