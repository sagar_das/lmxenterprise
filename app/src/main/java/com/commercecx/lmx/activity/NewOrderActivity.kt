package com.commercecx.lmx.activity


import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Optional
import com.commercecx.lmx.R
import com.commercecx.lmx.adapter.NewOrderAdapter
import com.commercecx.lmx.base.BaseActivity
import com.commercecx.lmx.model.OrderOld
import com.commercecx.lmx.model.RouteOld
import com.commercecx.lmx.util.Logger
import com.commercecx.lmx.view.JDEExpandableRecyclerView
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_new_order.*
import kotlinx.android.synthetic.main.fab_order_submenu.*
import java.util.*


class NewOrderActivity : BaseActivity() {





    private var fabExpanded = false
    private var bundle: Bundle? = null
    private var route: RouteOld? = null
    private var orderList: List<OrderOld>? = null
    private var realm: Realm? = null
    protected override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_order)
        ButterKnife.bind(this)
        setTitle("Orders")
        if (bundle == null) {
            bundle = getIntent().getBundleExtra("ROUTE_BUNDLE")
            route = bundle!!.getSerializable("ROUTE_DATA") as RouteOld?
        }
        realm = Realm.getDefaultInstance()
        val toolbar: Toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)

        // headerTitleTV.setText("New Order");
        val orders: RealmResults<OrderOld>? =
            Realm.getDefaultInstance().where(
                OrderOld::class.java
            ).findAll()

        assert(recyclerView != null)
        recyclerView!!.setLayoutManager(
            LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false
            )
        )
        val testAdapter = NewOrderAdapter(this, realm!!.copyFromRealm(orders) as ArrayList<OrderOld>, route)
        recyclerView!!.setAdapter(testAdapter)
    }

    protected override fun onResume() {
        super.onResume()
        fabExpanded = false
        cvClone!!.setVisibility(View.GONE)
        cvNewOrder!!.setVisibility(View.GONE)
        fabClone!!.setVisibility(View.GONE)
        fabNewOrder!!.setVisibility(View.GONE)
        fabOrder!!.setImageResource(R.drawable.ic_add_no_circle)
    }

    @Optional
    @OnClick(R.id.back_btn)
    fun onClickBackBtn() {
        Logger.debug("OnBackPressed...")
        finish()
    }

    @Optional
    @OnClick(R.id.createOrderBtn)
    fun onCreateOrderBtn() {
        Logger.debug("onCreateOrderBtn...")
        orderList =
            realm!!.where(OrderOld::class.java).equalTo("route_id", route!!.getId()).findAll()

        if (orderList!!.size == 10) {
            val dialogBuilder = AlertDialog.Builder(this)
            val premiumDialog: View =
                LayoutInflater.from(this).inflate(R.layout.premium_dialog, null)
            dialogBuilder.setView(premiumDialog)
            val dialog = dialogBuilder.create()
            val btnOK = premiumDialog.findViewById<View>(R.id.btnOk) as Button
            btnOK.setOnClickListener { dialog.dismiss() }
            dialog.show()
            return
        }
        val intent = Intent(this, CreateNewOrderActivity::class.java)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    //TODO: Change visibility modfifiers to Observable pattern
    @OnClick(R.id.fabOrder)
    fun onClickOrder() {
        if (fabExpanded) {
            cvClone!!.setVisibility(View.GONE)
            cvNewOrder!!.setVisibility(View.GONE)
            fabClone!!.setVisibility(View.GONE)
            fabNewOrder!!.setVisibility(View.GONE)
            fabOrder!!.setImageResource(R.drawable.ic_add_no_circle)
            fabExpanded = false
            viewFade!!.visibility = View.GONE
        } else {
            cvClone!!.setVisibility(View.VISIBLE)
            cvNewOrder!!.setVisibility(View.VISIBLE)
            fabClone!!.setVisibility(View.VISIBLE)
            fabNewOrder!!.setVisibility(View.VISIBLE)
            fabOrder!!.setImageResource(R.drawable.ic_close_white)
            fabExpanded = true
            viewFade!!.visibility = View.VISIBLE
        }
    }

    @OnClick(R.id.fabNewOrder)
    fun onClickNewOrder() {
        val intent = Intent(this, CreateNewOrderActivity::class.java)
        intent.putExtra("ROUTE_BUNDLE", bundle)
        startActivity(intent)
    }

    @OnClick(R.id.fabClone)
    fun onClickClickOrder() {
        Toast.makeText(
            this,
            "Click on the existing orders displayed on the screen.",
            Toast.LENGTH_SHORT
        ).show()
        onClickOrder()
    }
}
