package com.commercecx.lmx.fragment

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.speech.tts.TextToSpeech

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.lmx.R
import com.commercecx.lmx.activity.DashboardActivity
import com.commercecx.lmx.activity.DashboardPref
import com.commercecx.lmx.adapter.NotificationAdapter
import com.commercecx.lmx.adapter.RecentActivityAdapter
import com.commercecx.lmx.base.BaseFragment
import com.commercecx.lmx.model.Notification
import com.commercecx.lmx.model.OrderOld
import com.commercecx.lmx.model.RecentActivityDash
import com.commercecx.lmx.model.RouteOld
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import io.realm.Realm
import io.realm.RealmResults
import io.realm.Sort
import kotlinx.android.synthetic.main.dashboard_stats_main.*
import java.util.*


class DashStatsFragment : BaseFragment(), NotificationAdapter.INotificationClickListener {
    private val tts: TextToSpeech? = null
    private val isNote = false
    private var realm: Realm? = null
    var notificationAdapter: NotificationAdapter? = null
    var recentActivityAdapter: RecentActivityAdapter? = null
    var linearLayoutManager: LinearLayoutManager? = null
    var llmanagerRA: LinearLayoutManager? = null
    private val text: String? = null
    private var notificationList: List<Notification>? = null
    private var recentActivityList: List<RecentActivityDash>? = null
    private var orderList: List<OrderOld>? = null
    private var routeList: List<RouteOld>? = null

    interface INotificationUpdateListener {
        fun reduceNotificationCount()
    }

    var mNotifyUpdate: INotificationUpdateListener? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.activity_dashboard, container, false)
        ButterKnife.bind(this, view)
        realm = Realm.getDefaultInstance()
        val graph = view.findViewById<View>(R.id.graph) as GraphView
        val machineSeries = LineGraphSeries(
            arrayOf<DataPoint>(
                DataPoint(0.toDouble(), 1.toDouble()),
                DataPoint(1.toDouble(), 5.toDouble()),
                DataPoint(2.toDouble(), 3.toDouble())
            )
        )
        machineSeries.color = Color.BLUE
        val productSeries = LineGraphSeries(
            arrayOf<DataPoint>(
                DataPoint(0.toDouble(), 2.toDouble()),
                DataPoint(2.toDouble(), 6.toDouble()),
                DataPoint(3.toDouble(), 4.toDouble())
            )
        )
        productSeries.color = Color.GREEN
        val serviceSeries = LineGraphSeries(
            arrayOf<DataPoint>(
                DataPoint(0.toDouble(), 3.toDouble()),
                DataPoint(3.toDouble(), 7.toDouble()),
                DataPoint(4.toDouble(), 5.toDouble())
            )
        )
        serviceSeries.color = Color.RED
        graph.addSeries(machineSeries)
        graph.addSeries(productSeries)
        graph.addSeries(serviceSeries)
        return view
    }

    override fun onResume() {
        super.onResume()
        val settings: SharedPreferences = requireActivity().getSharedPreferences(PREFS_NAME, 0)
        if (settings.getBoolean("recentactivity", true)) {
            cvRecentActivity!!.setVisibility(View.VISIBLE)
        } else {
            cvRecentActivity!!.setVisibility(View.GONE)
        }
        setOrderAndRouteCount()
        fetchNotifications()
        fetchRecentActivityList()
        setNotificationAdapter()
        setRecentActivityAdapter()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mNotifyUpdate = activity as INotificationUpdateListener?
    }

    private fun setNotificationAdapter() {
        if (notificationList!!.size > 0) {
            tvNoNotifications!!.visibility = View.GONE
            notificationAdapter = NotificationAdapter(this, requireContext(), notificationList!!)
            linearLayoutManager =
                LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false)
            rvNotifications!!.setLayoutManager(linearLayoutManager)
            rvNotifications!!.setAdapter(notificationAdapter)
        } else {
            //tvNoNotifications.setVisibility(View.VISIBLE);
        }
    }

    private fun setRecentActivityAdapter() {
        if (recentActivityList!!.size > 0) {
            //tvNoNotifications.setVisibility(View.GONE);
            recentActivityAdapter = RecentActivityAdapter(recentActivityList!!, requireContext())
            llmanagerRA = LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false)
            rvRecentActivity!!.setLayoutManager(llmanagerRA)
            rvRecentActivity!!.setAdapter(recentActivityAdapter)
        } else {
            //tvNoNotifications.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.fabMore)
    fun onPrefClick() {
        startActivity(Intent(getContext(), DashboardPref::class.java))
    }

    private fun fetchNotifications() {
        notificationList = ArrayList<Notification>()
        var results: RealmResults<Notification> = realm!!.where(Notification::class.java).findAll()
        results = results.sort("updatedTSMP", Sort.DESCENDING)
        notificationList = realm!!.copyFromRealm(results)
    }

    private fun fetchRecentActivityList() {
        recentActivityList = ArrayList<RecentActivityDash>()
        val results: RealmResults<RecentActivityDash> =
            realm!!.where(RecentActivityDash::class.java).findAll()
        recentActivityList = realm!!.copyFromRealm(results)
    }

    private fun setOrderAndRouteCount() {
        orderList = ArrayList<OrderOld>()
        routeList = ArrayList<RouteOld>()
        val order_results: RealmResults<OrderOld> = realm!!.where(OrderOld::class.java).findAll()
        val route_results: RealmResults<RouteOld> = realm!!.where(RouteOld::class.java).findAll()
        orderList = realm!!.copyFromRealm(order_results)
        routeList = realm!!.copyFromRealm(route_results)
        tvOrderCount!!.text = if (orderList!!.size > 0) Integer.toString(orderList!!.size) else "0"
        tvRouteCount!!.text = if (routeList!!.size > 0) Integer.toString(routeList!!.size) else "0"
    }

    override fun OnNotificationClick() {
        mNotifyUpdate!!.reduceNotificationCount()
    }

    companion object {
        private const val SPEECH_REQUEST_CODE = 101
        private val TAG: String = DashboardActivity::class.java.getSimpleName()
        private const val PREFS_NAME = "dashboardpref"
    }
}
