package com.commercecx.lmx.fragment

/**
 * Created by Sagar Das on 8/19/20.
 */

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Optional
import com.commercecx.lmx.R
import com.commercecx.lmx.activity.AddNoteActivity
import com.commercecx.lmx.adapter.NotesAdapter
import com.commercecx.lmx.base.BaseFragment
import com.commercecx.lmx.model.Note
import com.commercecx.lmx.view.JDECustomTextView
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.realm.Realm
import io.realm.RealmList
import io.realm.Sort
import kotlinx.android.synthetic.main.content_note.*
import kotlinx.android.synthetic.main.fab_note_submenu.*
import kotlinx.android.synthetic.main.fragment_notes.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*


class NotesFragment : BaseFragment(), SwipeRefreshLayout.OnRefreshListener {
    private val DEFAULT_PAGE_NUMBER = 1
    private var jsonArray: JSONArray? = null


    private var gson: Gson? = null
    private var notes: List<Note>? = null


    private var fabExpanded = false
    private var fileList: MutableList<String?>? = null

    //    private WrapContentLinearLayoutManager linearLayoutManager;
    private var linearLayoutManager: LinearLayoutManager? = null
    private var mCurrentPage = DEFAULT_PAGE_NUMBER

    //endregion
    // region Constants
    private val isLoading = false
    private var notesAdapter: NotesAdapter? = null
    //endregion
    /**
     * onScrollListner for pagination of Videos and Photos
     */
    private val onScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (linearLayoutManager != null) {
                try {
                    if (!isLoading) {
                        val visibleItemCount: Int = linearLayoutManager!!.childCount
                        val totalItemCount: Int = linearLayoutManager!!.itemCount
                        val firstVisibleItemPosition: Int =
                            linearLayoutManager!!.findLastVisibleItemPosition()
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                        }
                    }
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        removeListeners()
        mCurrentPage = 1
    }

    private fun removeListeners() {
        homeRV!!.removeOnScrollListener(onScrollListener)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fabExpanded = false
        val view = inflater.inflate(R.layout.fragment_notes, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        //  linearLayoutManager = new WrapContentLinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        mCurrentPage = DEFAULT_PAGE_NUMBER
        homeRV!!.setLayoutManager(linearLayoutManager)
        // homeRV.setLayoutManager(linearLayoutManager);
        homeRV!!.setHasFixedSize(true)
        homeRV!!.setItemViewCacheSize(20)
        homeRV!!.setDrawingCacheEnabled(true)
        homeRV!!.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH)
        //calling onScrollListner for Pagination
        homeRV!!.addOnScrollListener(onScrollListener)
        swipeRefreshLayout!!.setOnRefreshListener(this)
        swipeRefreshLayout!!.setColorSchemeResources(R.color.header_theme)
        viewFade!!.setOnClickListener { onAddNote() }
        startShimmerAnimation()
    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, "Setting screen name: DashboardFragment")
        val realm = Realm.getDefaultInstance()
        //notes = realm.where(Note.class).findAll();
        var results = realm.where(Note::class.java).findAll()
        results = results.sort("updatedTSMP", Sort.DESCENDING)
        notes = ArrayList()
        notes = realm.copyFromRealm(results)

//        List<Note> tempNotes = new ArrayList<>();
//        tempNotes = realm.copyFromRealm(notes);
        if (notes!!.size != 0) {

            //    notes = sortNotes(tempNotes);
            setHomeFeedAdapter()
        } else {
            setHomeFeedAdapter()
        }
    }

    //    private List<Note> sortNotes(List<Note> tempNotes) {
    //
    //        Collections.sort(tempNotes,new Comparator<Note>() {
    //            @Override
    //            public int compare(Note note, Note t1) {
    //
    //                return note.getUpdatedTSMP() > t1.getUpdatedTSMP() ? 1 : note.getUpdatedTSMP() < t1.getUpdatedTSMP() ? -1 : 0;
    //
    //            }
    //
    //
    //        });
    //
    //        return tempNotes;
    //    }
    private fun startShimmerAnimation() {
        shimmerEffectLayout!!.baseAlpha = 0.7f
        shimmerDefaultLL!!.visibility = View.VISIBLE
        homeRV!!.setVisibility(View.GONE)
        notes_count!!.visibility = View.GONE
        fabNotes!!.setVisibility(View.GONE)
        cvTextNote!!.setVisibility(View.GONE)
        cvHandwriting!!.setVisibility(View.GONE)
        cvAudio!!.setVisibility(View.GONE)
        fabTextNote!!.setVisibility(View.GONE)
        //fabhandwriting!!.setVisibility(View.GONE)
        fabAudio!!.setVisibility(View.GONE)
        shimmerEffectLayout!!.startShimmerAnimation()
    }

    private fun stopShimmerAnimation() {
        shimmerEffectLayout!!.stopShimmerAnimation()
        homeRV!!.setVisibility(View.VISIBLE)
        notes_count!!.visibility = View.VISIBLE
        fabNotes!!.setVisibility(View.VISIBLE)
        shimmerDefaultLL!!.visibility = View.GONE
    }

    private fun setHomeFeedAdapter() {
        if (mCurrentPage == DEFAULT_PAGE_NUMBER) if (true) {
            noDataTextTV!!.visibility = View.GONE
            notesAdapter = NotesAdapter(requireActivity(), notes!!)
            homeRV!!.setAdapter(notesAdapter)
        } else {
            noDataTextTV!!.visibility = View.VISIBLE
        }
        if (notes!!.size == 0) {
            noDataTextTV!!.visibility = View.VISIBLE
        }
        stopShimmerAnimation()
    }

    override fun onRefresh() {
        swipeRefreshLayout!!.setRefreshing(false)
    }

    @Optional
    @OnClick(R.id.add_note)
    fun onClickAddNote() {
        val intent = Intent(getActivity(), AddNoteActivity::class.java)
        startActivity(intent)
    }

    @OnClick(R.id.fabNotes)
    fun onAddNote() {
        if (fabExpanded) {
            cvTextNote!!.visibility = View.GONE
            cvHandwriting!!.visibility = View.GONE
            cvAudio!!.visibility = View.GONE
            fabTextNote!!.visibility = View.GONE
            //fabhandwriting!!.visibility = View.GONE
            fabAudio!!.visibility = View.GONE
            fabNotes!!.setImageResource(R.drawable.ic_create_white)
            rlNotesContent!!.isEnabled = true
            fabExpanded = false
            viewFade!!.visibility = View.GONE
        } else {
            cvTextNote!!.visibility = View.VISIBLE
            cvHandwriting!!.visibility = View.GONE
            cvAudio!!.visibility = View.GONE
            fabTextNote!!.visibility = View.VISIBLE
            //fabhandwriting!!.visibility = View.GONE
            fabAudio!!.visibility = View.GONE
            fabNotes!!.setImageResource(R.drawable.ic_close_white)
            rlNotesContent!!.isEnabled = false
            fabExpanded = true
            viewFade!!.visibility = View.VISIBLE
        }
    }

    @OnClick(R.id.fabTextNote)
    fun onTextNote() {
        val intent = Intent(getActivity(), AddNoteActivity::class.java)
        startActivity(intent)
        onAddNote()
    }

    fun notesFetchCompleted(response: String?) {
        fileList = ArrayList()
        val gsonBuilder = GsonBuilder()
        gson = gsonBuilder.create()
        try {
            val jsonObject = JSONObject(response)
            jsonArray = jsonObject.getJSONArray("Items")
            notes =

                    gson!!.fromJson<Array<Note>>(
                        jsonArray.toString(),
                        Array<Note>::class.java
                    ) as List<Note>


            for (n in notes!!) {
                if (n.isAttachment()) {
                    fileList!!.add(n.getAttachName())
                }
                //   n.setUpdatedTSMP(date.getTime());
            }
            Log.i(TAG, "notesFetchCompleted: ")
            Realm.getDefaultInstance().use { realm ->
                realm.executeTransaction { realm ->
                    val _notesList = RealmList<Note>()
                    _notesList.addAll(notes!!)
                    realm.insertOrUpdate(_notesList) // <-- insert unmanaged to Realm
                }
            }
            //            Realm realm = Realm.getDefaultInstance();
//            for(final Note note:notes){
//                realm.executeTransactionAsync(new Realm.Transaction() {
//                    @Override
//                    public void execute(Realm realm) {
//                        realm.insertOrUpdate(note);
//                    }
//
//                });
//            }
        } catch (e: Exception) {
            Log.e(TAG, "notesFetchCompleted: ", e)
        }
        if (fileList!!.size == 0) {
            setHomeFeedAdapter()
        } else {
            setHomeFeedAdapter()
        }
    }

    fun filesFetchCompleted() {
        setHomeFeedAdapter()
    }

    companion object {
        //region Variable declaration
        private val TAG = NotesFragment::class.java.simpleName
    }
}
