package com.commercecx.lmx.fragment

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import com.commercecx.lmx.R
import com.commercecx.lmx.base.BaseFragment
import com.commercecx.lmx.util.NotePhoto
import kotlinx.android.synthetic.main.fragment_attach.*


/**
 * A simple [Fragment] subclass.
 */
class AttachFragment  //endregion
    : BaseFragment() {
    //region Variable Declaraton

    private var notePhoto: NotePhoto? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_attach, container, false)
        ButterKnife.bind(this, view)
        val b: Bundle = requireArguments()
        notePhoto = b.getSerializable("NOTE_PHOTO") as NotePhoto?
        ivImageFullView!!.setImageDrawable(notePhoto!!.bitmapDrawable)
        return view
    }
}
