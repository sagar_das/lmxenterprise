package com.commercecx.lmx.fragment

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.location.*
import android.os.Bundle

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.location.LocationManagerCompat
import com.commercecx.lmx.R
import com.commercecx.lmx.base.BaseFragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapFragment
import com.google.android.gms.maps.model.*
import org.osmdroid.util.GeoPoint
import java.io.IOException


/**
 * A simple [Fragment] subclass.
 */
class GoogleMapFragment : BaseFragment(), LocationListener {
    private var locationManager: LocationManager? = null
    private var gMap: GoogleMap? = null
    private var gActivity: Activity? = null
    private var mLocationPermissionGranted = false
    var currentLocation: Location? = null
    var fusedLocationProviderClient: FusedLocationProviderClient? = null
    private val mDefaultLocation = LatLng(-33.8523341, 151.2106085)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val rootView: View = inflater.inflate(R.layout.fragment_google_map, container, false)
        val address_array: Array<String> = requireArguments().getStringArray("ADDRESS")!!
        val mapFragment =
            getChildFragmentManager().findFragmentById(R.id.frgGoogleMaps) as MapFragment
        locationManager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            requestLocationPermission()
        } else {
            mLocationPermissionGranted = true
        }
        locationManager!!.requestLocationUpdates(
            LocationManager.NETWORK_PROVIDER,
            MIN_TIME,
            MIN_DISTANCE,
            this
        ) //You can also use LocationManager.GPS_PROVIDER and LocationManager.PASSIVE_PROVIDER
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireContext())
        deviceLocation
        mapFragment.getMapAsync { mMap ->
            gMap = mMap
            mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
            mMap.clear()
            for (address in address_array) {
                val gp = getLocationFromAddress(address)
                if (gp != null) {
                    mMap.addMarker(
                        MarkerOptions()
                            .position(LatLng(gp.latitude, gp.longitude))
                            .title(address)
                    )
                } else {
                    Toast.makeText(
                        gActivity,
                        "Please enter valid address for your routes.",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }


            //                mMap.addMarker(new MarkerOptions()
            //                        .position(new LatLng(37.4629101, -122.2449094))
            //                        .title("Route 1")
            //                        .snippet("Route 1 desc"));
        }
        return rootView
    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable: Drawable = ContextCompat.getDrawable(context, vectorResId)!!
        vectorDrawable.setBounds(
            0,
            0,
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight
        )
        val bitmap = Bitmap.createBitmap(
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    fun getLocationFromAddress(strAddress: String?): GeoPoint? {
        val coder = Geocoder(getContext())
        val address: List<Address>?
        var p1: GeoPoint? = null
        return try {
            address = coder.getFromLocationName(strAddress, 5)
            if (address == null || address.size == 0) {
                return null
            }
            val location = address[0]
            location.latitude
            location.longitude
            p1 = GeoPoint(
                location.latitude,
                location.longitude
            )
            p1
        } catch (e: IOException) {
            null
        }
    }

    override fun onLocationChanged(location: Location) {
        val latLng = LatLng(location.latitude, location.longitude)
        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10f)
        gMap!!.animateCamera(cameraUpdate)
        locationManager!!.removeUpdates(this)
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
    override fun onProviderEnabled(provider: String) {}
    override fun onProviderDisabled(provider: String) {}
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        this.gActivity = activity
    }

    private fun requestLocationPermission() {
        val mPermissions = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        if (!ActivityCompat.shouldShowRequestPermissionRationale(
                gActivity!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            ActivityCompat.requestPermissions(gActivity!!, mPermissions, RC_HANDLE_LOCATION_PERM)
            return
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        mLocationPermissionGranted = false
        when (requestCode) {
            RC_HANDLE_LOCATION_PERM -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    mLocationPermissionGranted = true
                }
            }
        }
        // updateLocationUI();
    }//Toast.makeText(getContext(), currentLocation.getLatitude() + "" + currentLocation.getLongitude(), Toast.LENGTH_LONG).show();
    //Log.i(TAG, "CX_MAPS "+currentLocation.getLatitude());

    //
    /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
    private val deviceLocation: Unit
        private get() {
            /*
             * Get the best and most recent location of the device, which may be null in rare
             * cases when a location is not available.
             */
            try {
                if (mLocationPermissionGranted) {
                    val task = fusedLocationProviderClient!!.lastLocation
                    task.addOnSuccessListener { location ->
                        Log.i(TAG, "CX_MAPS inside")
                        if (location != null) {
                            currentLocation = location
                            //Toast.makeText(getContext(), currentLocation.getLatitude() + "" + currentLocation.getLongitude(), Toast.LENGTH_LONG).show();
                            //Log.i(TAG, "CX_MAPS "+currentLocation.getLatitude());
                            val latLng =
                                LatLng(currentLocation!!.latitude, currentLocation!!.longitude)
                            val markerOptions =
                                MarkerOptions().position(latLng).title("You are here!").icon(
                                    BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)
                                )
                            val googlePlex = CameraPosition.builder()
                                .target(latLng)
                                .zoom(10f)
                                .bearing(0f)
                                .tilt(45f)
                                .build()
                            gMap!!.addMarker(markerOptions)
                            gMap!!.animateCamera(
                                CameraUpdateFactory.newCameraPosition(googlePlex),
                                10000,
                                null
                            )

                            //
                        }
                    }
                }
            } catch (e: SecurityException) {
                Log.e("Exception: %s", e.message)
            }
        }

    companion object {
        private const val DEFAULT_ZOOM = 10f
        private const val MIN_TIME: Long = 400
        private const val MIN_DISTANCE = 1000f
        const val RC_HANDLE_LOCATION_PERM = 5
        private val TAG = GoogleMapFragment::class.java.simpleName
    }
}
