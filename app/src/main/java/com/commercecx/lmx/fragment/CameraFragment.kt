package com.commercecx.lmx.fragment

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.app.Activity
import android.content.Context
import android.hardware.Camera
import android.os.Bundle

import android.util.Log
import android.view.*
import android.widget.Button
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.lmx.R
import com.commercecx.lmx.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_camera.*
import java.io.IOException
import java.text.DateFormat


/**
 * A simple [Fragment] subclass.
 */
class CameraFragment  //endregion
    : BaseFragment() {
    private var mCamera: Camera? = null



    //endregion
    //region Camera Callbacks
    private val mSHutterCallback =
        Camera.ShutterCallback { pbCamera!!.visibility = View.VISIBLE }
    private val mJPEGCallBack =
        Camera.PictureCallback { bytes, camera ->
            val fileName = DateFormat.getDateTimeInstance().toString() + ".jpg"
            var success = true
            try {
                requireActivity().openFileOutput(fileName, Context.MODE_PRIVATE)
                    .use({ os -> os.write(bytes) })
            } catch (e: Exception) {
                Log.e(TAG, "onPictureTaken: FIle not found", e)
                success = false
            }
            if (success) {
                mCallback!!.onPhotoCaptured(fileName)
            } else {
                mCallback!!.onPhotoCaptured("NONE")
            }
        }

    //endregion
    //region Fragment callback
    interface CameraFragCallbacks {
        fun onPhotoCaptured(filename: String?)
    }

    private var mCallback: CameraFragCallbacks? = null
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mCallback = activity as CameraFragCallbacks?
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val v: View = inflater.inflate(R.layout.fragment_camera, container, false)
        ButterKnife.bind(this, v)
        //region Generating surface view for Camera
        val holder = svCamera!!.holder
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)
        holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(surfaceHolder: SurfaceHolder) {
                try {
                    if (mCamera != null) {
                        mCamera!!.setPreviewDisplay(holder)
                        mCamera!!.setDisplayOrientation(90)
                    }
                } catch (e: IOException) {
                    Log.e(TAG, "surfaceCreated: Error setting up preview display", e)
                }
            }

            override fun surfaceChanged(surfaceHolder: SurfaceHolder, i: Int, i1: Int, i2: Int) {
                if (mCamera != null) {
                    val params = mCamera!!.parameters
                    var size = getOptimalPreviewSize(
                        params.supportedPreviewSizes,
                        i1, i2
                    )
                    params.setPreviewSize(size!!.width, size.height)
                    size = getOptimalPreviewSize(
                        params.supportedPreviewSizes,
                        i1, i2
                    )
                    params.setPictureSize(size!!.width, size.height)
                    val focusModes = params.supportedFocusModes
                    if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
                        params.focusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO
                    }

                    //mCamera.setParameters(params);
                    try {
                        mCamera!!.startPreview()
                    } catch (e: Exception) {
                        Log.e(TAG, "surfaceChanged: Could not start preview", e)
                        mCamera!!.release()
                        mCamera = null
                    }
                }
            }

            override fun surfaceDestroyed(surfaceHolder: SurfaceHolder) {
                if (mCamera != null) {
                    mCamera!!.stopPreview()
                }
            }
        })
        pbCamera!!.visibility = View.INVISIBLE

        //endregion
        return v
    }

    override fun onResume() {
        super.onResume()
        mCamera = Camera.open(0)
    }

    override fun onPause() {
        super.onPause()
        if (mCamera != null) {
            mCamera!!.release()
            mCamera = null
        }
    }

    @OnClick(R.id.btnCapture)
    fun captureClick() {
        if (mCamera != null) {
            mCamera!!.takePicture(mSHutterCallback, null, mJPEGCallBack)
        }
    }

    private fun getOptimalPreviewSize(
        sizes: List<Camera.Size>?,
        w: Int, h: Int
    ): Camera.Size? {
        val ASPECT_TOLERANCE = 0.1
        val targetRatio = w.toDouble() / h
        if (sizes == null) {
            return null
        }
        var optimalSize: Camera.Size? = null
        var minDiff: Double = Double.MAX_VALUE

        //Matching size and aspect ratio
        for (size in sizes) {
            val ratio = size.width.toDouble() / size.height
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) {
                continue
            }
            if (Math.abs(size.height - h) < minDiff) {
                optimalSize = size
                minDiff = Math.abs(size.height - h).toDouble()
            }
        }

        // If no match found for the aspect ratio
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE
            for (size in sizes) {
                if (Math.abs(size.height - h) < minDiff) {
                    optimalSize = size
                    minDiff = Math.abs(size.height - h).toDouble()
                }
            }
        }
        return optimalSize
    }

    companion object {
        //region Variables declaration
        private val TAG = CameraFragment::class.java.simpleName
        const val EXTRA_PHOTO_FILENAME = "com.org.jde.fragment.photo_filename"
    }
}
