package com.commercecx.lmx.fragment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.lmx.R
import com.commercecx.lmx.adapter.AccountsAdapter
import com.commercecx.lmx.adapter.CatalogGridLayoutManager
import com.commercecx.lmx.base.BaseSalesForceFragment
import com.commercecx.lmx.model.Account
import com.commercecx.lmx.model.OrderOld
import com.commercecx.lmx.model.RouteOld
import com.commercecx.lmx.presenter.AccountsPresenter
import com.commercecx.lmx.presenter.AccountsViewTranslator
import com.commercecx.lmx.util.Utils
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_account.*

/**
 * Created by Sagar Das on 8/25/20.
 */
class AccountFragment : BaseSalesForceFragment<AccountsPresenter>(), SwipeRefreshLayout.OnRefreshListener,
    AccountsViewTranslator {
    private val DEFAULT_PAGE_NUMBER = 1
    private val quantity = 0
    private var linearLayoutManager: LinearLayoutManager? = null
    private var gridLayoutManager: CatalogGridLayoutManager? = null

    private var mCurrentPage = DEFAULT_PAGE_NUMBER

    // region Constants
    private val isLoading = false
    private var accountAdapter: AccountsAdapter? = null
    private val name: String? = null
    private var cActivity: Activity? = null
    private lateinit var accountsPresenter: AccountsPresenter

    private var bundle: Bundle? = null
    private var route: RouteOld? = null
    private var orderList: List<OrderOld>? = null
    private var realm: Realm? = null

    internal lateinit var callback: OnSearchSelectedListener

    fun setOnSearchSelectedListener(callback: OnSearchSelectedListener) {
        this.callback = callback
    }

    interface OnSearchSelectedListener {
        fun onAccountSearchClicked()
    }





    private val onScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (gridLayoutManager != null) {
                try {
                    if (!isLoading) {
                        val visibleItemCount: Int = gridLayoutManager!!.getChildCount()
                        val totalItemCount: Int = gridLayoutManager!!.getItemCount()
                        val firstVisibleItemPosition: Int =
                            gridLayoutManager!!.findLastVisibleItemPosition()
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                        }
                    }
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        requireActivity().title = "Accounts"
        val view: View = inflater.inflate(R.layout.fragment_account, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter().onViewCreated()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        linearLayoutManager = LinearLayoutManager(cActivity, LinearLayoutManager.VERTICAL, false)
        gridLayoutManager = CatalogGridLayoutManager(requireContext(), Utils.CATALOG_GRID_COLUMN_SIZE)
        //  linearLayoutManager = new WrapContentLinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        mCurrentPage = DEFAULT_PAGE_NUMBER
        homeRV!!.setLayoutManager(linearLayoutManager)
        // homeRV.setLayoutManager(linearLayoutManager);
        homeRV!!.setHasFixedSize(true)
        homeRV!!.setItemViewCacheSize(20)
        homeRV!!.setDrawingCacheEnabled(true)
        homeRV!!.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH)
        //calling onScrollListner for Pagination
        homeRV!!.addOnScrollListener(onScrollListener)
        swipeRefreshLayout!!.setOnRefreshListener(this)
        swipeRefreshLayout!!.setColorSchemeResources(R.color.header_theme)
        startShimmerAnimation()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        this.cActivity = activity

    }

    private fun startShimmerAnimation() {
        shimmerEffectLayout!!.baseAlpha = 0.7f
        shimmerDefaultLL!!.visibility = View.VISIBLE
        homeRV!!.setVisibility(View.GONE)
        shimmerEffectLayout!!.startShimmerAnimation()
//        val handler = Handler()
//        handler.postDelayed({ //Do something after 100ms
//            setHomeFeedAdapter()
//        }, 500)
    }



    private fun stopShimmerAnimation() {
        shimmerEffectLayout!!.stopShimmerAnimation()
        homeRV!!.setVisibility(View.VISIBLE)
        shimmerDefaultLL!!.visibility = View.GONE
    }

    fun scrollToBeginning() {
        gridLayoutManager!!.scrollToPositionWithOffset(0, 0)
    }

    private fun setHomeFeedAdapter(data: MutableList<Account>) {
        if (mCurrentPage == DEFAULT_PAGE_NUMBER) if (true) {
            noDataTextTV!!.visibility = View.GONE
            accountAdapter = AccountsAdapter(requireActivity(), data, requireContext())
            homeRV!!.adapter = accountAdapter
        } else {
            noDataTextTV!!.visibility = View.VISIBLE
        }
        stopShimmerAnimation()
    }





    override fun onRefresh() {}

    companion object {
        private val TAG = CatalogFragment::class.java.simpleName
    }



    override fun createPresenter(): AccountsPresenter =
        AccountsPresenter(this)

    override fun displayAccounts(data: List<Account>) {
        setHomeFeedAdapter(data.toMutableList())
    }

    override fun getInstance(accountsPresenter: AccountsPresenter) {
        this.accountsPresenter = accountsPresenter
    }

    @OnClick(R.id.ibSearch)
    fun onSearchClick() {
        //startActivity(Intent(getContext(), ImportActivity::class.java))
        callback.onAccountSearchClicked()
    }

    fun receiveSearchQuery(query: String?){
        Toast.makeText(requireContext(),query, Toast.LENGTH_SHORT).show()
       accountsPresenter.searchAccounts(query)


    }



}
