package com.commercecx.lmx.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.lmx.R
import com.commercecx.lmx.activity.CreateNewOrderActivity
import com.commercecx.lmx.base.BaseFragment
import com.commercecx.lmx.model.Account
import com.commercecx.lmx.model.Product
import com.commercecx.lmx.util.NoteAttachUtils
import kotlinx.android.synthetic.main.account_detail_screen.*
import kotlinx.android.synthetic.main.account_detail_screen.view.*
import kotlinx.android.synthetic.main.fab_order_submenu.*


/**
 * Created by Sagar Das on 8/25/20.
 */
class AccountDetailFragment: BaseFragment() {

    private var fabExpanded = false
    private var bundle: Bundle? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.account_detail_screen, container, false)
        ButterKnife.bind(this, view)


        ButterKnife.bind(this, view)
        val b: Bundle = requireArguments()
        val account = b.getSerializable("ACCOUNT") as Account?

        view.tvName.text = account!!.name
        view.tvDesc.text = account!!.description
        view.tvBillingAddress.text = account!!.billingStreet + "," + account!!.billingCity + "," +account!!.billingState + "," +account!!.billingCountry + "," +account!!.billingPostalCode
        view.tvShippingAddress.text = account!!.shippingStreet + "," + account!!.shippingCity + "," +account!!.shippingState + "," +account!!.shippingCountry + "," +account!!.shippingPostalCode


        return view
    }

    override fun onResume() {
        super.onResume()
        fabExpanded = false
        cvClone!!.setVisibility(View.GONE)
        cvNewOrder!!.setVisibility(View.GONE)
        fabClone!!.setVisibility(View.GONE)
        fabNewOrder!!.setVisibility(View.GONE)
        fabOrder!!.setImageResource(R.drawable.ic_add_no_circle)

    }

    @OnClick(R.id.fabOrder)
    fun onClickOrder() {
        if (fabExpanded) {
            cvClone!!.setVisibility(View.GONE)
            cvNewOrder!!.setVisibility(View.GONE)
            fabClone!!.setVisibility(View.GONE)
            fabNewOrder!!.setVisibility(View.GONE)
            fabOrder!!.setImageResource(R.drawable.ic_add_no_circle)
            fabExpanded = false
          //  viewFade!!.visibility = View.GONE
        } else {
            cvClone!!.setVisibility(View.VISIBLE)
            cvNewOrder!!.setVisibility(View.VISIBLE)
            fabClone!!.setVisibility(View.VISIBLE)
            fabNewOrder!!.setVisibility(View.VISIBLE)
            fabOrder!!.setImageResource(R.drawable.ic_close_white)
            fabExpanded = true
        //    viewFade!!.visibility = View.VISIBLE
        }
    }


    @OnClick(R.id.fabNewOrder)
    fun onClickNewOrder() {
        val intent = Intent(requireContext(), CreateNewOrderActivity::class.java)
        intent.putExtra("ROUTE_BUNDLE", bundle)
        startActivity(intent)
    }

    @OnClick(R.id.fabClone)
    fun onClickClickOrder() {
        Toast.makeText(
            requireContext(),
            "Click on the existing orders displayed on the screen.",
            Toast.LENGTH_SHORT
        ).show()
        onClickOrder()
    }
}