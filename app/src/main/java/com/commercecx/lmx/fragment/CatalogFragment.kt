package com.commercecx.lmx.fragment

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.lmx.R
import com.commercecx.lmx.activity.NewCatalogItemActivity
import com.commercecx.lmx.adapter.CatalogAdapter
import com.commercecx.lmx.adapter.CatalogGridLayoutManager
import com.commercecx.lmx.base.BaseSalesForceFragment
import com.commercecx.lmx.model.Product
import com.commercecx.lmx.presenter.ProductsPresenter
import com.commercecx.lmx.presenter.ProductsViewTranslator
import com.commercecx.lmx.util.Logger
import com.commercecx.lmx.util.Utils
import kotlinx.android.synthetic.main.fragment_catalog.*



/**
 * A simple [Fragment] subclass.
 */
class CatalogFragment : BaseSalesForceFragment<ProductsPresenter>(), SwipeRefreshLayout.OnRefreshListener,
    ProductsViewTranslator {
    private val DEFAULT_PAGE_NUMBER = 1
    private val quantity = 0
    private var linearLayoutManager: LinearLayoutManager? = null
    private var gridLayoutManager: CatalogGridLayoutManager? = null
    private val isFabCatalogClicked = false
    private var mCurrentPage = DEFAULT_PAGE_NUMBER
    private lateinit var productsPresenter: ProductsPresenter
    // region Constants
    private val isLoading = false
    private var catalogAdapter: CatalogAdapter? = null
    private val name: String? = null
    private var cActivity: Activity? = null

    internal lateinit var callback: OnSearchSelectedListener

    fun setOnSearchSelectedListener(callback: OnSearchSelectedListener) {
        this.callback = callback
    }

    interface OnSearchSelectedListener {
        fun onSearchClicked()
    }






    private val onScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (gridLayoutManager != null) {
                try {
                    if (!isLoading) {
                        val visibleItemCount: Int = gridLayoutManager!!.getChildCount()
                        val totalItemCount: Int = gridLayoutManager!!.getItemCount()
                        val firstVisibleItemPosition: Int =
                            gridLayoutManager!!.findLastVisibleItemPosition()
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                        }
                    }
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_catalog, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter().onViewCreated(null, null, null)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        linearLayoutManager = LinearLayoutManager(cActivity, LinearLayoutManager.VERTICAL, false)
        gridLayoutManager = CatalogGridLayoutManager(requireContext(), Utils.CATALOG_GRID_COLUMN_SIZE)
        //  linearLayoutManager = new WrapContentLinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        mCurrentPage = DEFAULT_PAGE_NUMBER
        homeRV!!.setLayoutManager(gridLayoutManager)
        // homeRV.setLayoutManager(linearLayoutManager);
        homeRV!!.setHasFixedSize(true)
        homeRV!!.setItemViewCacheSize(20)
        homeRV!!.setDrawingCacheEnabled(true)
        homeRV!!.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH)
        //calling onScrollListner for Pagination
        homeRV!!.addOnScrollListener(onScrollListener)
        swipeRefreshLayout!!.setOnRefreshListener(this)
        swipeRefreshLayout!!.setColorSchemeResources(R.color.header_theme)
        startShimmerAnimation()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        this.cActivity = activity

    }

    private fun startShimmerAnimation() {
        shimmerEffectLayout!!.baseAlpha = 0.7f
        shimmerDefaultLL!!.visibility = View.VISIBLE
        homeRV!!.setVisibility(View.GONE)
        shimmerEffectLayout!!.startShimmerAnimation()
//        val handler = Handler()
//        handler.postDelayed({ //Do something after 100ms
//            setHomeFeedAdapter()
//        }, 500)
    }

    override fun onResume() {
        super.onResume()

    }

    private fun stopShimmerAnimation() {
        shimmerEffectLayout!!.stopShimmerAnimation()
        homeRV!!.setVisibility(View.VISIBLE)
        shimmerDefaultLL!!.visibility = View.GONE
    }

    fun scrollToBeginning() {
        gridLayoutManager!!.scrollToPositionWithOffset(0, 0)
    }

    private fun setHomeFeedAdapter(data: MutableList<Product>) {
        if (mCurrentPage == DEFAULT_PAGE_NUMBER) if (true) {
            noDataTextTV!!.visibility = View.GONE

            if(catalogAdapter == null) {
                catalogAdapter = CatalogAdapter(requireActivity(), name, 0, null, data, requireContext())
                homeRV!!.adapter = catalogAdapter
            }
            else{
                catalogAdapter!!.changeProductListData(data)

            }
        } else {
            noDataTextTV!!.visibility = View.VISIBLE
        }
        stopShimmerAnimation()
    }





    override fun onRefresh() {}
    @OnClick(R.id.fabCatalog)
    fun OnCatalogClick() {
        startActivity(Intent(getContext(), NewCatalogItemActivity::class.java))
    }

    @OnClick(R.id.ibImport)
    fun onImportClick() {
        //startActivity(Intent(getContext(), ImportActivity::class.java))
        showFilterDialog()
    }

    @OnClick(R.id.ibSearch)
    fun onSearchClick() {
        //startActivity(Intent(getContext(), ImportActivity::class.java))
        callback.onSearchClicked()
    }

    private fun showFilterDialog() {
        //Show confirmation dialog
        val dialogBuilder = AlertDialog.Builder(requireActivity())
        val tagsDialog: View = layoutInflater.inflate(R.layout.product_options, null)
        dialogBuilder.setView(tagsDialog)
        val dialog = dialogBuilder.create()
        val ok = tagsDialog.findViewById<View>(R.id.btnOK) as Button
        ok.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    companion object {
        private val TAG = CatalogFragment::class.java.simpleName
    }

    override fun initViews() {
        TODO("Not yet implemented")
    }

    override fun showProductDetailsContent(product: Product) {
        TODO("Not yet implemented")
    }

    override fun displayProducts(data: List<Product>) {
        setHomeFeedAdapter(data.toMutableList())
    }

    override fun showLoader() {
        Logger.info("Show loader")
    }

    override fun hideLoader() {
        Logger.info("Hide loader")
    }

    override fun context(): Context? {
        TODO("Not yet implemented")
    }

    override fun resources(): Resources {
        TODO("Not yet implemented")
    }

    override fun lifecycle(): Lifecycle? {
        TODO("Not yet implemented")
    }

    override fun getSupportFragmentManager(): FragmentManager? {
        TODO("Not yet implemented")
    }

    override fun showMessage(text: String) {
        TODO("Not yet implemented")
    }

    override fun createPresenter(): ProductsPresenter =
        ProductsPresenter(this)

    override fun getInstance(productsPresenter: ProductsPresenter) {
        this.productsPresenter = productsPresenter
    }

    fun receiveSearchQuery(query: String?){
        Toast.makeText(requireContext(),query, Toast.LENGTH_SHORT).show()
        productsPresenter.searchProducts(query)


    }
}
