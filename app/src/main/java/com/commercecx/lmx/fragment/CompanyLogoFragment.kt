package com.commercecx.lmx.fragment

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.Manifest
import android.app.Activity
import android.app.AlertDialog

import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.MediaStore

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.lmx.R
import com.commercecx.lmx.base.BaseFragment
import com.commercecx.lmx.util.NotePhoto
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fab_profilepic_submenu.*
import kotlinx.android.synthetic.main.fragment_company_logo.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class CompanyLogoFragment : BaseFragment(), CameraFragment.CameraFragCallbacks {



    private var isFabExpanded = false
    private var cActivity: Activity? = null
    private var fragmentTransaction: FragmentTransaction? = null
    private var fragCamera: CameraFragment? = null
    var sp: SharedPreferences? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val v: View = inflater.inflate(R.layout.fragment_company_logo, container, false)
        ButterKnife.bind(this, v)
        sp = PreferenceManager.getDefaultSharedPreferences(getContext())
        val companyLogoPath = sp!!.getString("prefCompanyLogo", "no_pic")
        if (companyLogoPath.equals("no_pic", ignoreCase = true)) {
            val res = requireActivity().resources
            val resourceId = res.getIdentifier(
                "tom", "drawable", cActivity!!.packageName
            )
            ivCompanyLogo!!.setImageResource(resourceId)
        } else {
            val photo = NotePhoto(companyLogoPath, cActivity)
            ivCompanyLogo!!.setImageDrawable(photo.bitmapDrawable)
        }
        return v
    }

    override fun onResume() {
        super.onResume()
        cvAttach!!.setVisibility(View.GONE)
        cvCamera!!.setVisibility(View.GONE)
        fabCamera!!.setVisibility(View.GONE)
        fabAttach!!.setVisibility(View.GONE)
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        this.cActivity = activity
    }

    @OnClick(R.id.fabEditProfilePic)
    fun onClickChangeProfilePic() {
        isFabExpanded = if (isFabExpanded) {
            cvAttach!!.setVisibility(View.GONE)
            cvCamera!!.setVisibility(View.GONE)
            fabCamera!!.setVisibility(View.GONE)
            fabAttach!!.setVisibility(View.GONE)
            true
        } else {
            cvAttach!!.setVisibility(View.VISIBLE)
            cvCamera!!.setVisibility(View.GONE)
            fabCamera!!.setVisibility(View.GONE)
            fabAttach!!.setVisibility(View.VISIBLE)
            false
        }
    }

    @OnClick(R.id.fabAttach)
    fun onClickAttach() {
        attachPhotoLibrary()
    }

    @OnClick(R.id.fabCamera)
    fun onClickCamera() {
        if (!checkCameraPerission()) {
            requestCameraPermission()
        } else {
            initiateCameraFragment()
        }
    }

    private fun attachPhotoLibrary() {
        val mimeTypes = arrayOf("image/*")
        val galleryIntent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        galleryIntent.addCategory(Intent.CATEGORY_OPENABLE)
        galleryIntent.type = mimeTypes[0]
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG)
    }

    private fun saveCompanyLogoToLocalDb(bitmapImage: Bitmap) {
        val contextWrapper = ContextWrapper(getContext())
        val directory = contextWrapper.getDir("CompanyLogo", Context.MODE_PRIVATE)

        //Forming name
        val date = Date()
        val dateFormat: DateFormat = SimpleDateFormat("MM-dd-yyyy")
        val fileName = dateFormat.format(date) + "_" + date.time + ".jpg"
        val file = File(directory, fileName)
        val spe = sp!!.edit()
        spe.putString("prefCompanyLogo", file.absolutePath)
        spe.commit()
        spe.clear()
        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(file)
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos)
        } catch (e: Exception) {
            Log.i(TAG, "saveAttachmentToLocalDb: Error in saving attachments", e)
        } finally {
            try {
                fos!!.close()
            } catch (e: Exception) {
                Log.e(TAG, "Error closing file output stream ", e)
            }
        }
    }

    override fun onPhotoCaptured(filename: String?) {
        val oldFrag: androidx.fragment.app.Fragment? = requireFragmentManager().findFragmentByTag(CAM_FRAG_TAG)
        requireFragmentManager().beginTransaction().remove(oldFrag!!).commit()
        fragCamera = null
        flCamera!!.visibility = View.GONE
        val absolutePath: String = requireContext().getFileStreamPath(filename).getAbsolutePath()
        val notePhoto = NotePhoto(absolutePath, cActivity)
        saveCompanyLogoToLocalDb(notePhoto.bitmap!!)
    }

    //region Permissions
    private fun checkCameraPerission(): Boolean {
        // Permission is not granted
        return ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.CAMERA
        ) === PackageManager.PERMISSION_GRANTED
    }

    private fun requestCameraPermission() {
        if (!ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                Manifest.permission.CAMERA
            )
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(), arrayOf(Manifest.permission.CAMERA),
                CAMERA_PERM
            )
        }
    }

    //endregion
    //region Results
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RESULT_LOAD_IMG && resultCode == Activity.RESULT_OK) {
            if (data != null && data.data != null) {
                val uriData = data.data
                val dialogBuilder = AlertDialog.Builder(getContext())
                val completeDialog: View =
                    LayoutInflater.from(getContext()).inflate(R.layout.complete_dialog, null)
                dialogBuilder.setView(completeDialog)
                val dialog = dialogBuilder.create()
                val title = completeDialog.findViewById<TextView>(R.id.title)
                val btnYes = completeDialog.findViewById<Button>(R.id.btnYes)
                val btnNo = completeDialog.findViewById<Button>(R.id.btnNo)
                title.text = "Proceed to change company logo?"
                btnYes.setOnClickListener {
                    try {
                        val bitmap = MediaStore.Images.Media.getBitmap(
                            requireContext().getContentResolver(),
                            uriData
                        )
                        saveCompanyLogoToLocalDb(bitmap)
                        val notePhoto = NotePhoto(cActivity, bitmap)
                        ivCompanyLogo!!.setImageDrawable(notePhoto.bitmapDrawable)
                        //mListener.onProfilePicChanged(notePhoto);
                        dialog.dismiss()
                    } catch (e: IOException) {
                        e.printStackTrace()
                        Log.e(
                            TAG,
                            "Unable to select image from gallery",
                            e
                        )
                        dialog.dismiss()
                    }
                }
                btnNo.setOnClickListener { dialog.dismiss() }
                dialog.show()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            CAMERA_PERM ->                 // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    initiateCameraFragment()
                } else {
                    Toast.makeText(getContext(), "Camera permission was denied", Toast.LENGTH_SHORT)
                        .show()
                }
        }
    }

    //endregion
    private fun initiateCameraFragment() {
        flCamera!!.visibility = View.VISIBLE
        if (fragCamera == null) {
            fragCamera = CameraFragment()
            fragmentTransaction = requireFragmentManager().beginTransaction()
            fragmentTransaction!!.add(R.id.flCamera, fragCamera!!, CAM_FRAG_TAG)
            fragmentTransaction!!.commit()
        }
    }

    companion object {
        //region Variable Declaration
        private val TAG = CompanyLogoFragment::class.java.simpleName
        private const val RESULT_LOAD_IMG = 102
        private const val CAMERA_PERM = 101
        private const val CAM_FRAG_TAG = "CAMERA_TAG"
    }
}
