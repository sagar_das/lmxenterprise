package com.commercecx.lmx.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import com.commercecx.lmx.R
import com.commercecx.lmx.adapter.NewOrderAdapter
import com.commercecx.lmx.base.BaseFragment
import com.commercecx.lmx.model.OrderOld
import com.commercecx.lmx.model.RouteOld
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.fragment_orders.view.*
import java.util.ArrayList

/**
 * Created by Sagar Das on 8/26/20.
 */
class OrderFragment: BaseFragment() {

    private var orderList: List<OrderOld>? = null
    private var realm: Realm? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_orders, container, false)
        ButterKnife.bind(this, view)

        realm = Realm.getDefaultInstance()
        val orders: RealmResults<OrderOld>? =
            Realm.getDefaultInstance().where(
                OrderOld::class.java
            ).findAll()

        assert(view.recyclerView != null)
        view.recyclerView!!.setLayoutManager(
            LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
        )
        var route = RouteOld()
        route.setCustomerName("Sagar")
        route.setAddress("Brazil")
        val testAdapter = NewOrderAdapter(requireContext(), realm!!.copyFromRealm(orders) as ArrayList<OrderOld>, route)
        view.recyclerView!!.setAdapter(testAdapter)
        return view
    }
}