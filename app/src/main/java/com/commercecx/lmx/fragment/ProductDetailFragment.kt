package com.commercecx.lmx.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import com.commercecx.lmx.R
import com.commercecx.lmx.base.BaseFragment
import com.commercecx.lmx.model.Product
import com.commercecx.lmx.util.NoteAttachUtils
import kotlinx.android.synthetic.main.product_detail_screen.view.*

/**
 * Created by Sagar Das on 8/25/20.
 */
class ProductDetailFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.product_detail_screen, container, false)
        ButterKnife.bind(this, view)


        ButterKnife.bind(this, view)
        val b: Bundle = requireArguments()
        val product = b.getSerializable("PRODUCT") as Product?

        view.tvNameValue.text = product!!.productname
        view.tvPriceVal.text = "$12"
        view.tvDescVal.text = product!!.productcode


        val imgArr = product!!.productimage.split(",")

        val imageBitmap = NoteAttachUtils.convertBytesToBitmap(imgArr[1])

        view.ivProduct.setImageBitmap(imageBitmap)



        return view
    }
}