package com.commercecx.lmx.fragment

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.app.*
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.Nullable
import androidx.cardview.widget.CardView
import androidx.core.app.NotificationCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Optional
import com.commercecx.lmx.R
import com.commercecx.lmx.activity.AddRouteActivity
import com.commercecx.lmx.activity.MainNavActivity
import com.commercecx.lmx.adapter.RoutesAdapter
import com.commercecx.lmx.base.BaseFragment
import com.commercecx.lmx.model.RouteOld
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.realm.Realm
import io.realm.Realm.Transaction.OnSuccess
import io.realm.RealmResults
import io.realm.Sort
import kotlinx.android.synthetic.main.fab_routes_submenu.*
import kotlinx.android.synthetic.main.fab_routes_submenu.mapLL
import kotlinx.android.synthetic.main.fragment_catalog.*
import kotlinx.android.synthetic.main.view_route_sub_header.*
import org.json.JSONArray
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class RoutesFragment : BaseFragment(), SwipeRefreshLayout.OnRefreshListener,
    RoutesAdapter.IRoutesCompletedListener {
    private val DEFAULT_PAGE_NUMBER = 1
    private var routes: MutableList<RouteOld>? = null
    private val jsonArray: JSONArray? = null
    private var gson: Gson? = null
    private val countOfRecordsServer = 0







    private var fabExpanded = false

    //    private WrapContentLinearLayoutManager linearLayoutManager;
    private var linearLayoutManager: LinearLayoutManager? = null
    private var mCurrentPage = DEFAULT_PAGE_NUMBER

    // region Constants
    private val isLoading = false
    private var routesAdapter: RoutesAdapter? = null
    private var dataPresent = true
    private var notificationManager: NotificationManager? = null

    interface IMapClickListener {
        fun onMapClick()
    }

    var mapClickListener: IMapClickListener? = null

    interface INotificationListener {
        fun onNotificationCreated()
    }

    var mNotification: INotificationListener? = null

    /**
     * onScrollListner for pagination of Videos and Photos
     */
    private val onScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (linearLayoutManager != null) {
                try {
                    if (!isLoading) {
                        val visibleItemCount: Int = linearLayoutManager!!.getChildCount()
                        val totalItemCount: Int = linearLayoutManager!!.getItemCount()
                        val firstVisibleItemPosition: Int =
                            linearLayoutManager!!.findLastVisibleItemPosition()
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                        }
                    }
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        removeListeners()
        mCurrentPage = 1
    }

    private fun removeListeners() {
        homeRV!!.removeOnScrollListener(onScrollListener)
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mapClickListener = activity as IMapClickListener?
        mNotification = activity as INotificationListener?
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_dashboard, container, false)
        ButterKnife.bind(this, view)


        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        //  linearLayoutManager = new WrapContentLinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        mCurrentPage = DEFAULT_PAGE_NUMBER
        homeRV!!.setLayoutManager(linearLayoutManager)
        // homeRV.setLayoutManager(linearLayoutManager);
        homeRV!!.setHasFixedSize(true)
        homeRV!!.setItemViewCacheSize(20)
        homeRV!!.setDrawingCacheEnabled(true)
        homeRV!!.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH)
        //calling onScrollListner for Pagination
        homeRV!!.addOnScrollListener(onScrollListener)
        swipeRefreshLayout!!.setOnRefreshListener(this)
        swipeRefreshLayout!!.setColorSchemeResources(R.color.header_theme)
        startShimmerAnimation()
    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, "Setting screen name: DashboardFragment")
        fabExpanded = false
        cvClone!!.visibility = View.GONE
        cvNewRoute!!.visibility = View.GONE
        fabClone!!.visibility = View.GONE
        fabNewRoute!!.visibility = View.GONE
        //fabCurrentNav.setImageResource(R.drawable.ic_add_no_circle);
        initializeData()
    }

    private fun initializeData() {
        val date = Date()
        val dateFormat: DateFormat = SimpleDateFormat("MM-dd-yyyy")
        val strDate = dateFormat.format(date)
        var eRoutes: RealmResults<RouteOld?> =
            Realm.getDefaultInstance().where(RouteOld::class.java).equalTo("routeDate", strDate)
                .findAll()
        eRoutes = eRoutes.sort("index", Sort.ASCENDING)
        routes = Realm.getDefaultInstance().copyFromRealm(eRoutes)
        if (routes!!.size == 0) {
            dataPresent = false
        }
        swipeRefreshLayout!!.setRefreshing(false)
        setHomeFeedAdapter()
    }

    private fun startShimmerAnimation() {
        shimmerEffectLayout!!.baseAlpha = 0.7f
        shimmerDefaultLL!!.visibility = View.VISIBLE
        homeRV!!.visibility = View.GONE
        shimmerEffectLayout!!.startShimmerAnimation()
    }

    private fun stopShimmerAnimation() {
        shimmerEffectLayout!!.stopShimmerAnimation()
        homeRV!!.visibility = View.VISIBLE
        shimmerDefaultLL!!.visibility = View.GONE
    }

    private fun setHomeFeedAdapter() {
        if (mCurrentPage == DEFAULT_PAGE_NUMBER) if (dataPresent) {
            noDataTextTV!!.visibility = View.GONE
            routesAdapter = RoutesAdapter(this, requireContext(), requireActivity().layoutInflater, routes!!)
            homeRV!!.adapter = routesAdapter
        } else {
            noDataTextTV!!.visibility = View.VISIBLE
        }
        stopShimmerAnimation()
    }

    override fun onRefresh() {
        initializeData()
    }

    @OnClick(R.id.fabCurrentNav)
    fun onNavClick() {
//        Intent intent = new Intent(getContext(), NavigationActivity.class);
//        startActivity(intent);
        fabExpanded = if (fabExpanded) {
            cvClone!!.visibility = View.GONE
            cvNewRoute!!.visibility = View.GONE
            fabClone!!.visibility = View.GONE
            fabNewRoute!!.visibility = View.GONE
            fabCurrentNav!!.setImageResource(R.drawable.ic_add_no_circle)
            false
            //viewFade.setVisibility(View.GONE);
        } else {
            cvClone!!.visibility = View.VISIBLE
            cvNewRoute!!.visibility = View.VISIBLE
            fabClone!!.visibility = View.VISIBLE
            fabNewRoute!!.visibility = View.VISIBLE
            fabCurrentNav!!.setImageResource(R.drawable.ic_close_white)
            true
            //viewFade.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.fabNewRoute)
    fun onClickNewRoute() {
        if (routes!!.size == 10) {
            val dialogBuilder = AlertDialog.Builder(getContext())
            val premiumDialog: View =
                LayoutInflater.from(getContext()).inflate(R.layout.premium_dialog, null)
            dialogBuilder.setView(premiumDialog)
            val dialog = dialogBuilder.create()
            val btnOK = premiumDialog.findViewById<View>(R.id.btnOk) as Button
            btnOK.setOnClickListener { dialog.dismiss() }
            dialog.show()
            return
        }
        val intent = Intent(getContext(), AddRouteActivity::class.java)
        startActivity(intent)
    }

    @OnClick(R.id.fabClone)
    fun onClickCloneRoute() {
        Toast.makeText(
            getContext(),
            "Click on the existing orders displayed on the screen.",
            Toast.LENGTH_SHORT
        ).show()
        //onClickOrder();
    }

    @Optional
    @OnClick(R.id.filterLL)
    fun onFilterClickEvent() {
        filterLL!!.setBackgroundColor(getResources().getColor(R.color.truck_header_selected_color))
        mapLL!!.setBackgroundColor(Color.TRANSPARENT)
        listLL!!.setBackgroundColor(Color.TRANSPARENT)
        (getActivity() as MainNavActivity).onFilterViewClicked()
    }

    @OnClick(R.id.mapLL)
    fun onMapClickEvent() {
        mapLL!!.setBackgroundColor(getResources().getColor(R.color.truck_header_selected_color))
        //  filterLL.setBackgroundColor(Color.TRANSPARENT);
        //  listLL.setBackgroundColor(Color.TRANSPARENT);
        (getActivity() as MainNavActivity).onMapClick()
    }

    @Optional
    @OnClick(R.id.listLL)
    fun onListClickEvent() {
        listLL!!.setBackgroundColor(getResources().getColor(R.color.truck_header_selected_color))
        mapLL!!.setBackgroundColor(Color.TRANSPARENT)
        filterLL!!.setBackgroundColor(Color.TRANSPARENT)
        (getActivity() as MainNavActivity).onListViewClicked()
    }

    @Optional
    @OnClick(R.id.map_type)
    fun onMapClick() {
        mapClickListener!!.onMapClick()
    }

    private fun saveToLocalDB() {
        swipeRefreshLayout!!.setRefreshing(false)
        val gsonBuilder = GsonBuilder()
        gson = gsonBuilder.create()
        val realm = Realm.getDefaultInstance()
        val date = Date()
        val dateFormat: DateFormat = SimpleDateFormat("MM-dd-yyyy")
        val strDate = dateFormat.format(date)
        val existingRoutes: List<RouteOld> = Realm.getDefaultInstance().where(
            RouteOld::class.java
        ).equalTo("routeDate", strDate).findAll()
        val existRoutes: List<RouteOld?> = realm.copyFromRealm(existingRoutes)

//        JSONArray existingJsonArray = new JSONArray(existingRoutes);
//        routes =new LinkedList<Route>(Arrays.asList(gson.fromJson(existingJsonArray.toString(), Route[].class)));
        if (existingRoutes.size == 0) {
            routes =

                    gson!!.fromJson<Array<RouteOld>>(
                        jsonArray.toString(),
                        Array<RouteOld>::class.java
                    ) as MutableList<RouteOld>


            sortRoutes()
            for (route in routes!!) {
                realm.executeTransactionAsync { realm -> realm.insertOrUpdate(route) }
            }
        } else if (existingRoutes.size < countOfRecordsServer) {
            routes =

                    gson!!.fromJson<Array<RouteOld>>(
                        jsonArray.toString(),
                        Array<RouteOld>::class.java
                    ) as MutableList<RouteOld>


            sortRoutes()
            var position = 0
            var max: Long = routes!![0].getUpdatedTSMP()
            for (i in routes!!.indices) {
                if (routes!![i].getUpdatedTSMP() > max) {
                    position = i
                    max = routes!![i].getUpdatedTSMP()
                }
            }
            val newRoute: RouteOld? = routes!![position]
            val mBuilder: NotificationCompat.Builder = NotificationCompat.Builder(requireContext(), CHANNEL_ID)
                .setSmallIcon(R.drawable.lmx_icon)
                .setContentTitle("Hi Tom")
                .setContentText(
                    "You are assigned with " + newRoute!!.getName().toString() + " customer"
                )
                .setStyle(
                    NotificationCompat.BigTextStyle()
                        .bigText(
                            "You are assigned with " + newRoute!!.getName().toString() + " customer"
                        )
                )
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
            notificationManager = requireActivity().getSystemService(NotificationManager::class.java)
            createNotificationChannel()
            val notification: Notification = mBuilder.build()
            notificationManager!!.notify(ROUTES_NOTIFICATION_ID, notification)
            realm.executeTransactionAsync { realm -> realm.insertOrUpdate(newRoute) }
            saveNotificationToDB(newRoute.getName()!!)
        } else if (existingRoutes.size == countOfRecordsServer) {
            routes = existRoutes as MutableList<RouteOld>
            sortRoutes()
        } else if (countOfRecordsServer == 0) {
            dataPresent = false
        }
        setHomeFeedAdapter()
    }

    private fun sortRoutes() {
        for (i in 0 until routes!!.size - 1) {
            for (j in i + 1 until routes!!.size) {
                if (routes!![i].getIndex() > routes!![j].getIndex()) {
                    Collections.swap(routes, i, j)
                }
            }
        }
    }

    private fun saveNotificationToDB(contents: String) {
        val notification: com.commercecx.lmx.model.Notification = com.commercecx.lmx.model.Notification()
        notification.setNotification_id(saltString)
        notification.setTitle("Customer")
        notification.setContents("You got assigned with $contents customer")
        notification.setType("customer")
        notification.setRead(false)
        notification.setUpdatedTSMP(Date().time)
        val realm = Realm.getDefaultInstance()
        realm.executeTransactionAsync(Realm.Transaction { realm -> realm.insertOrUpdate(notification) },
            OnSuccess { mNotification!!.onNotificationCreated() })
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = "Routes"
            val description = "Routes description"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            channel.description = description
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager!!.createNotificationChannel(channel)
        }
    }

    // length of the random string.
    private val saltString: String
        private get() {
            val SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
            val salt = StringBuilder()
            val rnd = Random()
            while (salt.length < 18) { // length of the random string.
                val index = (rnd.nextFloat() * SALTCHARS.length).toInt()
                salt.append(SALTCHARS[index])
            }
            return salt.toString()
        }

    override fun onRouteCompleted() {
        homeRV!!.setLayoutManager(linearLayoutManager)
        homeRV!!.setAdapter(routesAdapter)
        routesAdapter!!.notifyDataSetChanged()
    }

    companion object {
        private const val TAG = "DashboardFragment"
        private const val ROUTE_FETCH_JOB_ID = 202
        private const val CHANNEL_ID = "LMX_ROUTES_CHANNEL"
        private const val ACTION_ROUTES = "com.org.jde.ACTION_ROUTES"
        private const val ROUTES_NOTIFICATION_ID = 102
    }
}
